package com.gmi.slotscore.constante;

import com.gmi.slotscore.logica.configuracion.ConfiguracionGanaSiempre;
import com.gmi.slotscore.logica.configuracion.ConfiguracionSlots;
import com.gmi.slotscore.logica.configuracion.ConfiguracionSlotsPremiosChicos;

public class Sistema {

	/**
	 * Representa la configuracion de maquina a usarse para el juego, por ende
	 * para las apuestas reales
	 */
//	public static Class configuracionMaquina = ConfiguracionSlotsPremiosChicos.class;
    public static Class configuracionMaquina = ConfiguracionGanaSiempre.class;
	
	/**
	 * El monto de la apuesta admitida en el juego. Esto actualmente limita el
	 * sistema a que solo pueda haber un tipo de apuesta. Lo que realmente me
	 * importa un rabano porque si manana se puede apostar otro valor es que
	 * to_do anduvo bien y estamos haciendo guita!
	 */
	public static int APUESTA_ADMITIDA = 10;

	/**
	 * El incremento de la apuesta es de a esta cantidad
	 */
	public static float INCREMENTO_APUESTA = 0.2f;

	/**
	 * La cantidad maxima apostable por linea es el valor asignado aqui
	 */
	public static float APUESTA_MAXIMA_POR_LINEA = 1f;

	/**
	 * Factor multiplicador o divisor de moneda. Usado para modificar el tipo de
	 * moneda en juego y usar valores mas altos o mas bajos dependiendo de lo
	 * deseado
	 */
	public static int FACTOR_CONVERSION_MONEDA = 10;

	/**
	 * El monto apostado durante la ejecucion de una tirada gratuita generada
	 * por el bonus
	 */
	public static float MONTO_APUESTA_GRATUITA = 0.2f;

	/**
	 * Indica si el sistema hara uso o no
	 * de las jugadas ganadoras conformadas
	 * totalmente por figuras wild
	 * 
	 */
	public static byte APUESTA_WILD = 0;

	//las lineas apostadas durante la ejecucion de
	//una tirada gratuita generada por el bonus
	public static short CANTIDAD_LINEAS_APUESTA_GRATUITA = 10;

    //habilita la operacion que calcula las
    //probabilidades de la maquina dada la
    //configuracion elegida
    public static boolean CALCULAR_ESTADISTICA_MAQUINA = false;

    //habilitacion del audio
    public static boolean AUDIO_FX_ENABLED = true;

	/**
	 * El pozo puede tener un monto minimo al cual se precisa llegar antes de
	 * admitir la premiacion con el Jackpot
	 */
	public static int JACKPOT_MONTO_MINIMO = 1000;

	/**
	 * De cada apuesta que pueda participar del jackpot se retira un porcentaje
	 * para aportar al pozo
	 */
	public static float JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA = 2f;

}