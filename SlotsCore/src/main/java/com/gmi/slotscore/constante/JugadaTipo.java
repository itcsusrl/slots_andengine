package com.gmi.slotscore.constante;

public class JugadaTipo {

	public final static int NORMAL = 0; 
	public final static int SCATTER = 1;
	public final static int BONUS = 2;
	public final static int JACKPOT = 3;
	public final static int BONUS_GAME = 4;
	public final static int WILD = 5; 
	
	public static String obtenerDescripcion(int valor) {
		
		switch (valor) {
		case NORMAL:
			return "Normal";
		case SCATTER:
			return "Scatter";
		case BONUS:
			return "Bonus";
		case JACKPOT:
			return "Jackpot";
		case BONUS_GAME:
			return "Bonus Menor Mayor";
		case WILD:
			return "Wild";
		}	
		
		return "";
	}
}