package com.gmi.slotscore.constante;

public class FiguraTipo {

	public final static int NORMAL = 0; 
	public final static int SCATTER = 1;
	public final static int BONUS = 2;
	public final static int WILD = 3;
	public final static int KILLER = 4;
	public final static int JACKPOT = 5;
	public final static int BONUS_GAME = 6;
	
}
