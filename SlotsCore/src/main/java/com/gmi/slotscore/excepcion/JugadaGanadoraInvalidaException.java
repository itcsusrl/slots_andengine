package com.gmi.slotscore.excepcion;

public class JugadaGanadoraInvalidaException extends ConfiguracionInvalidaException {

	public JugadaGanadoraInvalidaException(String jugada) {
		super("La jugada ganadora '" + jugada + "' no es valida");
	}
}