package com.gmi.slotscore.excepcion;

public class ConfiguracionInvalidaException extends RuntimeException {

	public ConfiguracionInvalidaException(String message) {
		super(message);
	}
}
