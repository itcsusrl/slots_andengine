package com.gmi.slotscore.excepcion;

public class SystemException extends RuntimeException {
	
	private static final long serialVersionUID = 7989948099029271378L;

	public SystemException() {
		super();
	}

	public SystemException(int codigoError) {
		super(String.valueOf(codigoError));
	}

	public SystemException(String desc) {
		super(desc);
	}
}