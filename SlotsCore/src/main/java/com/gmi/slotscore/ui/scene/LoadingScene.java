package com.gmi.slotscore.ui.scene;

import com.gmi.slotscore.MainActivity;
import com.gmi.slotscore.ui.BaseScene;
import com.gmi.slotscore.ui.SceneManager;

import org.andengine.entity.scene.background.Background;
import org.andengine.entity.text.Text;
import org.andengine.util.color.Color;

/**
 * Created by pablo on 04/12/14.
 */
public class LoadingScene extends BaseScene {

    @Override
    public void createScene() {
        setBackground(new Background(Color.BLACK));
        Text loadingText = new Text(0, 0, resourcesManager.menuFont, "Loading...", vbom);
        loadingText.setX((MainActivity.CAMERA_WIDTH - loadingText.getWidth()) / 2f);
        loadingText.setY((MainActivity.CAMERA_HEIGHT - loadingText.getHeight()) / 2f);
        attachChild(loadingText);
    }

    @Override
    public void onBackKeyPressed() {
        return;
    }

    @Override
    public SceneManager.SceneType getSceneType() {
        return SceneManager.SceneType.SCENE_LOADING;
    }

    @Override
    public void disposeScene() {

    }
}