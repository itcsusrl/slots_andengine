package com.gmi.slotscore.ui;

import com.gmi.slotscore.dominio.Linea;

import org.andengine.entity.sprite.Sprite;

/**
 * Created by pablo on 26/11/14.
 */
public class PlayingLineCircle extends Sprite {
    public PlayingLineCircle(BaseScene scene, Linea line, float pX, float pY) {
        super(pX, pY, line.getCircleTextureRegion(), scene.vbom);
    }
}
