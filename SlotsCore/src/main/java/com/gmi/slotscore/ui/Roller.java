package com.gmi.slotscore.ui;

import com.gmi.slotscore.MainActivity;
import com.gmi.slotscore.dominio.Figura;
import com.gmi.slotscore.ui.scene.GameScene;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.ease.EaseLinear;
import org.andengine.util.modifier.ease.EaseStrongOut;
import org.andengine.util.modifier.ease.IEaseFunction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 18/09/14.
 */
public class Roller extends Rectangle implements IEntityModifier.IEntityModifierListener, IOnAreaTouchListener {

    public enum ROLLER_STATE {STOPPED, PRE_ROLLING, ROLLING, STOPPING}

    //number of figures on scene
    public int QNTY_FIGURES;

    //height of figures
    public float FIGURE_HEIGHT;

    //current rolling state
    public ROLLER_STATE state;

    //used to control the roller speed
    private IEaseFunction easeRollerRolling = EaseLinear.getInstance();
    private IEaseFunction easeRollerStopping = EaseStrongOut.getInstance();

    //roller events listener
    private RollerListener rollerListener;

    //delay in the stop process
    private float stopDelay;

    //flag to indicate the user
    //want to stop the roller
    private boolean forceStop;

    private Entity roller;
    private Entity rollerHelper;

    //position indexes
    private int currentIndex;
    private int lastIndex;

    private float currentRollingTime;
    private float maxRollingTime;

    private final float MAX_TIME_ROLLING = 3f;
    private final float MAX_TIME_STOPPING = 1f;
    private final float MAX_TIME_FORCE_STOPPING = 0.5f;

    private float lastMovingDistance;
    private float currentMovingDistance;
    private float totalMovingDistance;

    private int[] fictional;
    private List<RollFigure> fictionalRollFigures;

    private List<Figura> figures;

    private MainActivity activity;
    private GameScene gameScene;

    public Roller(BaseScene scene, List<Figura> figures, float x, RollerListener rollerListener, float stopDelay) {
        super(x, 185, 155, 768, scene.activity.getVertexBufferObjectManager());
        this.gameScene = (GameScene) scene;
        this.activity = (MainActivity) scene.activity;
        this.figures = figures;
        setAlpha(0f);
        this.stopDelay = stopDelay;
        this.rollerListener = rollerListener;
        state = ROLLER_STATE.STOPPED;

        //static values
        QNTY_FIGURES = figures.size();
        FIGURE_HEIGHT = figures.get(0).getTextureRegion().getHeight();

        //list of fictional roll figures
        fictionalRollFigures = new ArrayList<RollFigure>();

        roller = new Entity();
        rollerHelper = new Entity();

        for(int i=0; i<figures.size(); i++) {
            //set the initial position
            Figura figura = figures.get(i);
            roller.attachChild(new RollFigure(i, 0, i * FIGURE_HEIGHT, figura.getTextureRegion(), activity.getVertexBufferObjectManager()));
            rollerHelper.attachChild(new RollFigure(i, 0, i * FIGURE_HEIGHT, figura.getTextureRegion(), activity.getVertexBufferObjectManager()));
        }

        //set initial positions
        roller.setX(0);
        roller.setY(0);
        rollerHelper.setX(0);
        rollerHelper.setY(roller.getY() - QNTY_FIGURES * FIGURE_HEIGHT);

        //attach rollers
        attachChild(roller);
        attachChild(rollerHelper);

        //set the first position
        int firstRandomPosition = (int)((float)Math.random() * (float)QNTY_FIGURES);
        rollFast(firstRandomPosition);

    }

    public void shineAndRotate(int index, final long time, AnimatedSprite.IAnimationListener listener) {

        AnimatedSprite shineAnimation1 = new AnimatedSprite(0, 0, gameScene.resourcesManager.shineTextureRegion, activity.getVertexBufferObjectManager());
        AnimatedSprite shineAnimation2 = new AnimatedSprite(0, 0, gameScene.resourcesManager.shineTextureRegion, activity.getVertexBufferObjectManager());

        roller.getChildByIndex(index-1).detachChildren();
        rollerHelper.getChildByIndex(index-1).detachChildren();

        roller.getChildByIndex(index-1).attachChild(shineAnimation1);
        rollerHelper.getChildByIndex(index - 1).attachChild(shineAnimation2);

        RotationModifier rot1 = new RotationModifier(0.5f,0,360);
        RotationModifier rot2 = new RotationModifier(0.5f,0,360);
        rot1.setAutoUnregisterWhenFinished(true);
        rot2.setAutoUnregisterWhenFinished(true);

        roller.getChildByIndex(index-1).registerEntityModifier(rot1);
        rollerHelper.getChildByIndex(index-1).registerEntityModifier(rot2);

        int SHINE_REPETITIONS = 4;
        shineAnimation1.animate((time / SHINE_REPETITIONS), SHINE_REPETITIONS, listener);
        shineAnimation2.animate((time / SHINE_REPETITIONS), SHINE_REPETITIONS, listener);
    }

    public void rollFast(int index) {
        lastIndex = currentIndex;
        currentIndex = index;
        forcePosition(index);
    }

    public void roll(int index) {
        if(state == ROLLER_STATE.STOPPED) {
            //set indexes
            lastIndex = currentIndex;
            currentIndex = index;
            //set state
            state = ROLLER_STATE.PRE_ROLLING;
        }
    }

    public void rollFictional(int[] fictional) {
        if(state == ROLLER_STATE.STOPPED) {
            //set fictional figures
            this.fictional = fictional;
            //set indexes
            lastIndex = currentIndex;
            currentIndex = 0;
            //set state
            state = ROLLER_STATE.PRE_ROLLING;
        }
    }

    public void stop() {
        if(state == ROLLER_STATE.ROLLING && !forceStop) {
            forceStop = true;
        }
    }

    private void removeFictionalFigures() {

    }

    @Override
    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {

    }

    @Override
    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, ITouchArea pTouchArea, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        return true;
    }

    float pendingMovingDistance;

    @Override
    protected void onManagedUpdate(float pSecondsElapsed) {
        super.onManagedUpdate(pSecondsElapsed);

        if(state == ROLLER_STATE.PRE_ROLLING) {

            //total pxs to move
            totalMovingDistance = (QNTY_FIGURES * FIGURE_HEIGHT * 4);

            //reset flags
            currentRollingTime = 0f;
            currentMovingDistance = 0f;
            lastMovingDistance = 0f;
            currentMovingDistance = 0f;
            pendingMovingDistance = 0f;
            maxRollingTime = MAX_TIME_ROLLING;

            //remove possible old
            //fictional figures
            for(RollFigure fictionalFigure : fictionalRollFigures) {
                fictionalFigure.detachSelf();
            }

            state = ROLLER_STATE.ROLLING;
        }
        else if(state == ROLLER_STATE.ROLLING || state == ROLLER_STATE.STOPPING) {

            //increment time
            currentRollingTime += pSecondsElapsed;

            if(state == ROLLER_STATE.ROLLING) {

                float percentage = easeRollerRolling.getPercentage(currentRollingTime, maxRollingTime);
                currentMovingDistance = totalMovingDistance * percentage;

                //adjust
                if(currentMovingDistance > totalMovingDistance) {
                    currentMovingDistance = totalMovingDistance;
                }

                //calc movement
                float diffMovement = lastMovingDistance > 0 ? currentMovingDistance - lastMovingDistance : currentMovingDistance;

                //set movement
                roller.setY(roller.getY() + diffMovement);
                rollerHelper.setY(rollerHelper.getY() + diffMovement);

                //adjust
                Entity firstRoller = roller.getY() < rollerHelper.getY() ? roller : rollerHelper;
                Entity lastRoller = roller.getY() > rollerHelper.getY() ? roller : rollerHelper;
                if(lastRoller.getY() > MainActivity.CAMERA_HEIGHT) {
                    lastRoller.setY(firstRoller.getY() - QNTY_FIGURES * FIGURE_HEIGHT);
                }

                //save previous value
                lastMovingDistance = currentMovingDistance;

                boolean gotoStopping = false;

                if(forceStop) {
                    maxRollingTime = MAX_TIME_FORCE_STOPPING;
                    pendingMovingDistance = totalMovingDistance - currentMovingDistance;
                    forceStop = false;
                    gotoStopping = true;
                }

                //evaluate finish
                if(currentMovingDistance >= totalMovingDistance) {
                    maxRollingTime = MAX_TIME_STOPPING;
                    pendingMovingDistance = 0f;
                    gotoStopping = true;
                }

                if(gotoStopping) {
                    //reset flags
                    currentRollingTime = 0f;
                    currentMovingDistance = 0f;
                    lastMovingDistance = 0f;
                    currentMovingDistance = 0f;
                    state = ROLLER_STATE.STOPPING;
                }
            }
            else if(state == ROLLER_STATE.STOPPING) {

                int extraMoves = (lastIndex > currentIndex ? lastIndex-currentIndex : QNTY_FIGURES - currentIndex + lastIndex);

                //append the pending moving distance
                //from the rolling state
                totalMovingDistance = pendingMovingDistance + (extraMoves * FIGURE_HEIGHT);

                float percentage = easeRollerStopping.getPercentage(currentRollingTime, maxRollingTime + stopDelay);
                currentMovingDistance = totalMovingDistance * percentage;

                //adjust
                if(currentMovingDistance > totalMovingDistance) {
                    currentMovingDistance = totalMovingDistance;
                }

                if(fictional != null) {
                    //add fictional figures
                    for(int i=0; i<fictional.length; i++) {
                        //set the initial position
                        Figura figura = activity.getFigureById(fictional[i]);
                        if(figura != null) {
                            RollFigure rollerFigure1 = new RollFigure(0, i*FIGURE_HEIGHT, figura.getTextureRegion(), activity.getVertexBufferObjectManager());
                            RollFigure rollerFigure2 = new RollFigure(0, i*FIGURE_HEIGHT , figura.getTextureRegion(), activity.getVertexBufferObjectManager());
                            fictionalRollFigures.add(rollerFigure1);
                            fictionalRollFigures.add(rollerFigure2);
                            roller.attachChild(rollerFigure1);
                            rollerHelper.attachChild(rollerFigure2);
                        }
                    }
                    //reset fictional
                    fictional = null;
                }

                //calc movement
                float diffMovement = lastMovingDistance > 0 ? currentMovingDistance - lastMovingDistance : currentMovingDistance;

                //set movement
                roller.setY(roller.getY() + diffMovement);
                rollerHelper.setY(rollerHelper.getY() + diffMovement);

                //adjust
                Entity firstRoller = roller.getY() < rollerHelper.getY() ? roller : rollerHelper;
                Entity lastRoller = roller.getY() > rollerHelper.getY() ? roller : rollerHelper;
                if(lastRoller.getY() > MainActivity.CAMERA_HEIGHT) {
                    lastRoller.setY(firstRoller.getY() - QNTY_FIGURES * FIGURE_HEIGHT);
                }

                //save previous value
                lastMovingDistance = currentMovingDistance;

                //evaluate finish
                if(currentMovingDistance >= totalMovingDistance) {
                    state = ROLLER_STATE.STOPPED;
                    rollFast(currentIndex);
                    rollerListener.finishRolling();
                }
            }
        }
    }

    public void forcePosition(int index) {
        //calculate the necessary
        //movement to locate the
        //position
        float totalMovingPxs = index * FIGURE_HEIGHT;
        //move the first roller
        //to match the position
        roller.setY(-totalMovingPxs);
        //adjust the second to help
        //cover all the screen
        if(index > QNTY_FIGURES/2) {
            rollerHelper.setY(roller.getY() + (QNTY_FIGURES * FIGURE_HEIGHT));
        }
        else {
            rollerHelper.setY(roller.getY() - (QNTY_FIGURES * FIGURE_HEIGHT));
        }
    }

    private class RollFigure extends Sprite {
        public int index;
        public RollFigure(float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
            this(-1, pX, pY, pTextureRegion, pVertexBufferObjectManager);
        }
        public RollFigure(int index, float pX, float pY, ITextureRegion pTextureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
            super(pX, pY, pTextureRegion, pVertexBufferObjectManager);
            this.index = index;
        }
    }

    public interface RollerListener {
        void finishRolling();
    }
}