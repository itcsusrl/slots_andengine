package com.gmi.slotscore.ui;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

/**
 * Created by pablo on 07/10/14.
 */
public abstract class AbstractGame extends Rectangle {

    protected BaseScene scene;

    public AbstractGame(BaseScene scene, float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pWidth, pHeight, pVertexBufferObjectManager);
        this.scene = scene;
    }

    public abstract void loadResources();
    public abstract void createScene();

    public void load() {
        scene.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setColor(Color.BLACK);
                loadResources();
                createScene();
            }
        });
    }

    public abstract void enterGame();

    public abstract void exitGame();
}