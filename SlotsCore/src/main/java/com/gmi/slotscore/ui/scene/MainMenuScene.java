package com.gmi.slotscore.ui.scene;

import com.gmi.slotscore.MainActivity;
import com.gmi.slotscore.dominio.util.Pair;
import com.gmi.slotscore.ui.BaseScene;
import com.gmi.slotscore.ui.SceneManager;

import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.FadeInModifier;
import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.PathModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.util.GLState;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 01/12/14.
 */
public class MainMenuScene extends BaseScene {

    private final static float SPLASH_INITIAL_SCALE = 0.7f;
    private final static float SKY_MOVEMENT_TIME = 7f;
    private final static float SKY_WIDTH = MainActivity.CAMERA_WIDTH;

    private Sprite splash;

    private Sprite stadiumSprite;

    private Sprite skySprite1;
    private Sprite skySprite2;
    private boolean finishedMoveSky;

    private Sprite tribuna1Sprite;
    private Sprite tribuna2Sprite;

    public Sprite[] menuPlayersSprite;
    private List<Pair<Float, Float>> menuPlayersPositions;
    private List<Integer> menuPlayersZIndex;

    private IEntity touchToPlayEntity;

    public Sprite[] countersSprite;

    private boolean countDownStarted;

    @Override
    public void createScene() {

        splash = new Sprite(0, 0, resourcesManager.splashRegion, vbom);
        splash.setScale(SPLASH_INITIAL_SCALE);
        splash.setX((MainActivity.CAMERA_WIDTH - splash.getWidth()) / 2f);
        splash.setY(60);

        stadiumSprite = new Sprite(0, 0, resourcesManager.stadiumRegion, vbom) {
            @Override
            protected void preDraw(GLState pGLState, Camera pCamera) {
                super.preDraw(pGLState, pCamera);
                pGLState.enableDither();
            }
        };
        stadiumSprite.setWidth(MainActivity.CAMERA_WIDTH);
        stadiumSprite.setHeight(MainActivity.CAMERA_HEIGHT);

        skySprite1 = new Sprite(0, 0, resourcesManager.skyRegion, vbom) {
            @Override
            protected void preDraw(GLState pGLState, Camera pCamera) {
                super.preDraw(pGLState, pCamera);
                pGLState.enableDither();
            }
        };
        skySprite1.setWidth(SKY_WIDTH);
        skySprite1.setHeight(MainActivity.CAMERA_HEIGHT / 2);

        skySprite2 = new Sprite(SKY_WIDTH, 0, resourcesManager.skyRegion, vbom) {
            @Override
            protected void preDraw(GLState pGLState, Camera pCamera) {
                super.preDraw(pGLState, pCamera);
                pGLState.enableDither();
            }
        };
        skySprite2.setFlipped(true, false);
        skySprite2.setWidth(SKY_WIDTH);
        skySprite2.setHeight(MainActivity.CAMERA_HEIGHT / 2);

        tribuna1Sprite = new Sprite(0, 327, resourcesManager.tribuna1Region, vbom);
        tribuna1Sprite.setWidth(1024);
        tribuna1Sprite.setHeight(173);

        tribuna2Sprite = new Sprite(0, 327, resourcesManager.tribuna2Region, vbom);
        tribuna2Sprite.setWidth(1024);
        tribuna2Sprite.setHeight(173);

        menuPlayersPositions = new ArrayList<Pair<Float, Float>>();
        menuPlayersPositions.add(new Pair<Float, Float>(160f, 400f));
        menuPlayersPositions.add(new Pair<Float, Float>(280f, 400f));
        menuPlayersPositions.add(new Pair<Float, Float>(370f, 400f));
        menuPlayersPositions.add(new Pair<Float, Float>(470f, 400f));
        menuPlayersPositions.add(new Pair<Float, Float>(560f, 400f));
        menuPlayersPositions.add(new Pair<Float, Float>(650f, 480f));
        menuPlayersPositions.add(new Pair<Float, Float>(740f, 400f));

        menuPlayersZIndex = new ArrayList<Integer>();
        menuPlayersZIndex.add(new Integer(1));
        menuPlayersZIndex.add(new Integer(1));
        menuPlayersZIndex.add(new Integer(1));
        menuPlayersZIndex.add(new Integer(1));
        menuPlayersZIndex.add(new Integer(1));
        menuPlayersZIndex.add(new Integer(2));
        menuPlayersZIndex.add(new Integer(1));

        menuPlayersSprite = new Sprite[menuPlayersPositions.size()];

        int menuPlayerCounter = 0;
        for(Pair<Float,Float> menuPlayerPosition : menuPlayersPositions) {
            Sprite figuraSprite = new Sprite(menuPlayerPosition.value1, menuPlayerPosition.value2, resourcesManager.menuPlayersRegion[menuPlayerCounter], vbom);
            figuraSprite.setZIndex(menuPlayersZIndex.get(menuPlayerCounter));
            menuPlayersSprite[menuPlayerCounter] = figuraSprite;
            menuPlayerCounter++;
        }

        //touch to play entities
        Text touchToPlayText = new Text(0, 0, resourcesManager.menuFont, "Touch to play!", new TextOptions(HorizontalAlign.LEFT), vbom);
        touchToPlayText.setColor(Color.YELLOW);
        touchToPlayEntity = new Entity((MainActivity.CAMERA_WIDTH - touchToPlayText.getWidth())/2f, 560);
        Rectangle touchToPlayRect = new Rectangle(-10, -5, touchToPlayText.getWidth()+20, touchToPlayText.getHeight() + 10, vbom);
        touchToPlayEntity.attachChild(touchToPlayRect);
        touchToPlayEntity.attachChild(touchToPlayText);
        touchToPlayRect.setColor(Color.BLACK);
        touchToPlayRect.setAlpha(0.5f);

        //counter
        countersSprite = new Sprite[resourcesManager.countRegions.length];
        int counterPos = 0;
        for(ITextureRegion counterRegion : resourcesManager.countRegions) {
            countersSprite[counterPos] = new Sprite(0, 0, counterRegion, vbom);
            counterPos++;
        }

        //attach sky
        attachChild(skySprite1);
        attachChild(skySprite2);

        //attach stadium
        attachChild(stadiumSprite);

        //attach tribuna to stadium
        stadiumSprite.attachChild(tribuna1Sprite);
        stadiumSprite.attachChild(tribuna2Sprite);

        Rectangle playersRectangles = new Rectangle(stadiumSprite.getX(), stadiumSprite.getY(), stadiumSprite.getWidth(), stadiumSprite.getHeight(), vbom);
        playersRectangles.setAlpha(0f);
        stadiumSprite.attachChild(playersRectangles);

        //attach players
        for(Sprite figuraSprite : menuPlayersSprite) {
            playersRectangles.attachChild(figuraSprite);
        }

        //sort players z-index order
        playersRectangles.sortChildren();

        //attach splash
        attachChild(splash);

        //attach text
        attachChild(touchToPlayEntity);

        //touch alpha modifier
        IEntityModifier touchToPlayTextModifier = new LoopEntityModifier(
                new SequenceEntityModifier(
                        new DelayModifier(0.4f),
                        new FadeOutModifier(0.2f),
                        new FadeInModifier(0.4f)
                ));

        IEntityModifier touchToPlayRectModifier = new LoopEntityModifier(
                new SequenceEntityModifier(
                        new DelayModifier(0.4f),
                        new AlphaModifier(0.2f, touchToPlayRect.getAlpha(), 0.05f),
                        new AlphaModifier(0.4f, 0.05f, touchToPlayRect.getAlpha())
                ));

        touchToPlayText.registerEntityModifier(touchToPlayTextModifier);
        touchToPlayRect.registerEntityModifier(touchToPlayRectModifier);

        //first sky movement modifiers
        skySprite1.registerEntityModifier(new MoveXModifier(SKY_MOVEMENT_TIME, skySprite1.getX(), skySprite1.getX() - SKY_WIDTH, new SkyMovementModifierListener()));
        skySprite2.registerEntityModifier(new MoveXModifier(SKY_MOVEMENT_TIME, skySprite2.getX(), skySprite2.getX()-SKY_WIDTH, new SkyMovementModifierListener()));

        //splash modifier
        splash.registerEntityModifier(
                new LoopEntityModifier(
                        new SequenceEntityModifier(
                                new DelayModifier(6f),
                                new ParallelEntityModifier(
                                        new SequenceEntityModifier(new ScaleModifier(0.4f, SPLASH_INITIAL_SCALE, 1.1f), new ScaleModifier(0.4f, 1.1f, SPLASH_INITIAL_SCALE)),
                                        new RotationModifier(0.8f, splash.getRotation(), 720)))));

        //tribuna modifiers
        tribuna1Sprite.registerEntityModifier(new LoopEntityModifier(new PathModifier(3f, getTribuna1Path())));
        tribuna2Sprite.registerEntityModifier(new LoopEntityModifier(new PathModifier(2.1f, getTribuna2Path())));

        //update handlers
        registerUpdateHandler(new MenuUpdateHandler());

        setOnSceneTouchListener(new IOnSceneTouchListener() {
            @Override
            public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
                if(pSceneTouchEvent.isActionUp()) {
                    if(!countDownStarted) {
                        startCounter();
                    }
                }
                return true;
            }
        });
    }

    private void startCounter() {
        countDownStarted = true;
        touchToPlayEntity.setVisible(false);
        registerCountModifiers(2, new CounterListener() {
            @Override
            public void counterFinished() {
                SceneManager.getInstance().loadGameScene(engine);
                touchToPlayEntity.setVisible(true);
                countDownStarted = false;
            }
        });
    }

    private interface CounterListener {
        void counterFinished();
    }

    private void registerCountModifiers(final int index, final CounterListener counterListener) {
        final Sprite counterSprite = (index >= 0) ? countersSprite[index] : null;
        if(counterSprite != null) {
            counterSprite.setX((MainActivity.CAMERA_WIDTH-counterSprite.getWidth())/2f);
            counterSprite.setY((MainActivity.CAMERA_HEIGHT-counterSprite.getHeight())/2f);
            counterSprite.setScale(0f);
            attachChild(counterSprite);

            IEntityModifier moveModifier = null;
            if(index == 0) {
                moveModifier = new MoveXModifier(0.2f, counterSprite.getX(), MainActivity.CAMERA_WIDTH);
            }
            else if(index == 1) {
                moveModifier = new MoveYModifier(0.2f, counterSprite.getY(), MainActivity.CAMERA_HEIGHT);
            }
            else if(index == 2) {
                moveModifier = new MoveXModifier(0.2f, counterSprite.getX(), -(counterSprite.getWidth()*1.5f));
            }

            final IEntityModifier counterModifier = new SequenceEntityModifier(
                    new IEntityModifier.IEntityModifierListener() {
                        @Override
                        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                        }
                        @Override
                        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                            //register modifier
                            registerCountModifiers(index-1, counterListener);
                            counterSprite.detachSelf();
                        }
                    },
                    new ScaleModifier(0.6f, 0, 1),
                    moveModifier
            );
            counterSprite.registerEntityModifier(counterModifier);
            counterModifier.setAutoUnregisterWhenFinished(true);
        }
        else {
            counterListener.counterFinished();
        }
    }

    private PathModifier.Path getTribuna1Path() {
        float[] coordinatesX = {tribuna1Sprite.getX(), tribuna1Sprite.getX() - 1.5f, tribuna1Sprite.getX() + 2f, tribuna1Sprite.getX()};
        float[] coordinatesY = {tribuna1Sprite.getY(), tribuna1Sprite.getY() + 1.5f, tribuna1Sprite.getY() - 1f, tribuna1Sprite.getY()};
        PathModifier.Path path = new PathModifier.Path(coordinatesX, coordinatesY);
        return path;
    }

    private PathModifier.Path getTribuna2Path() {
        float[] coordinatesX = {tribuna1Sprite.getX(), tribuna1Sprite.getX() + 1.5f, tribuna1Sprite.getX() - 2f, tribuna1Sprite.getX()};
        float[] coordinatesY = {tribuna1Sprite.getY(), tribuna1Sprite.getY() - 1.5f, tribuna1Sprite.getY() + 1f, tribuna1Sprite.getY()};
        PathModifier.Path path = new PathModifier.Path(coordinatesX, coordinatesY);
        return path;
    }


    private class SkyMovementModifierListener implements IEntityModifier.IEntityModifierListener {
        @Override
        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
        }
        @Override
        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
            //only set the flag once
            if(pItem == skySprite1) {
                finishedMoveSky = true;
            }
        }
    }

    @Override
    public void onBackKeyPressed() {
        System.exit(0);
    }

    @Override
    public SceneManager.SceneType getSceneType() {
        return SceneManager.SceneType.SCENE_MENU;
    }

    @Override
    public void disposeScene() {
    }

    private class MenuUpdateHandler implements IUpdateHandler {

        @Override
        public void onUpdate(float pSecondsElapsed) {

            camera.setZoomFactorDirect(1.0f);

            if(finishedMoveSky) {
                finishedMoveSky = false;

                Sprite lefterSprite = skySprite1.getX() < skySprite2.getX() ? skySprite1 : skySprite2;
                Sprite righterSprite = skySprite1.getX() > skySprite2.getX() ? skySprite1 : skySprite2;

                lefterSprite.setX(righterSprite.getX() + SKY_WIDTH);
                lefterSprite.registerEntityModifier(new MoveXModifier(SKY_MOVEMENT_TIME, lefterSprite.getX(), lefterSprite.getX()-SKY_WIDTH, new SkyMovementModifierListener()));

                righterSprite.setX(0);
                righterSprite.registerEntityModifier(new MoveXModifier(SKY_MOVEMENT_TIME, righterSprite.getX(), righterSprite.getX()-SKY_WIDTH, new SkyMovementModifierListener()));

            }
        }

        @Override
        public void reset() {

        }
    }
}