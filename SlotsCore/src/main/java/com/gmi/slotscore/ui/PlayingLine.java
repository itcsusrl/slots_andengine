package com.gmi.slotscore.ui;

import com.gmi.slotscore.dominio.Linea;

import org.andengine.entity.sprite.Sprite;

/**
 * Created by pablo on 01/10/14.
 */
public class PlayingLine extends Sprite {

    public PlayingLine(BaseScene scene, Linea line, float pX, float pY) {
        super(pX, pY, line.getLineTextureRegion(), scene.vbom);
    }
}
