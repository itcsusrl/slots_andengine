package com.gmi.slotscore.ui.scene;

import com.gmi.slotscore.MainActivity;
import com.gmi.slotscore.ui.BaseScene;
import com.gmi.slotscore.ui.SceneManager;

import org.andengine.engine.camera.Camera;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.util.GLState;

/**
 * Created by pablo on 01/12/14.
 */
public class SplashScene extends BaseScene {

    private Sprite splash;

    @Override
    public void createScene() {
        splash = new Sprite(0, 0, resourcesManager.splashRegion, vbom);
        splash.setScale(1.2f);
        splash.setX((MainActivity.CAMERA_WIDTH - splash.getWidth()) / 2f);
        splash.setY((MainActivity.CAMERA_HEIGHT - splash.getHeight()) / 2f);
        attachChild(splash);
    }

    @Override
    public void onBackKeyPressed() {
    }

    @Override
    public SceneManager.SceneType getSceneType() {
        return SceneManager.SceneType.SCENE_SPLASH;
    }

    @Override
    public void disposeScene() {
        splash.detachSelf();
        splash.dispose();
        this.detachSelf();
        this.dispose();
    }
}