package com.gmi.slotscore.ui.minigame;

import com.gmi.slotscore.MainActivity;
import com.gmi.slotscore.ui.AbstractGame;
import com.gmi.slotscore.ui.BaseScene;
import com.gmi.slotscore.ui.scene.GameScene;
import com.gmi.slotscore.util.ThreadUtil;

import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.MoveModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;

import java.util.Hashtable;

/**
 * Created by pablo on 05/11/14.
 */
public class MinorMajorGame extends AbstractGame {

    private Hashtable<Integer,TextureRegion> cardTextures;

    private boolean readyToPlay;

    private float INITIAL_CARDS_BACK_POSITION_X;
    private float INITIAL_CARDS_BACK_POSITION_Y;

    private float INITIAL_CARDS_1_ROTATION = 10;
    private float INITIAL_CARDS_2_ROTATION = 15;

    private int cartaMiniJuegoMenorMayorJugador;
    private int cartaMiniJuegoMenorMayorMaquina;

    private BitmapTextureAtlas backgroundTextureAtlas;
    private ITextureRegion backgroundTextureRegion;
    private Sprite backgroundSprite;

    private BitmapTextureAtlas youWinTextureAtlas;
    private ITextureRegion youWinTextureRegion;
    private Sprite youWinSprite;

    private BitmapTextureAtlas gameOverTextureAtlas;
    private ITextureRegion gameOverTextureRegion;
    private Sprite gameOverSprite;

    private BitmapTextureAtlas greenArrowTextureAtlas;
    private ITextureRegion greenArrowTextureRegion;
    private Sprite greenArrowUpSprite;
    private Sprite greenArrowDownSprite;

    private BitmapTextureAtlas withdrawTextureAtlas;
    private ITextureRegion withdrawTextureRegion;
    private Sprite withdrawSprite;

    private BitmapTextureAtlas cardsDeckTextureAtlas;
    private ITextureRegion cardsDeckTextureRegion;
    private Sprite cardsDeckSprite;

    private BitmapTextureAtlas cardsBackTextureAtlas;
    private ITextureRegion cardsBackTextureRegion;
    private CardEntity[] cardsEntities;

    private MinorMajorPopup minorMajorPopup;

    private boolean initialized;

    private GameScene gameScene;

    public MinorMajorGame(BaseScene baseScene, float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(baseScene, pX, pY, pWidth, pHeight, pVertexBufferObjectManager);
        this.gameScene = (GameScene) baseScene;
    }

    @Override
    public void loadResources() {
        if (!initialized) {

            cardTextures = new Hashtable<Integer, TextureRegion>();

            //background
            backgroundTextureAtlas = new BitmapTextureAtlas(scene.activity.getTextureManager(), 800, 600, TextureOptions.BILINEAR);
            backgroundTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(backgroundTextureAtlas, scene.activity, "minigame_BG.jpg", 0, 0);
            backgroundTextureAtlas.load();
            backgroundSprite = new Sprite(0, 0, backgroundTextureRegion, getVertexBufferObjectManager());
            backgroundSprite.setSize(1024, 768);

            //you win
            youWinTextureAtlas = new BitmapTextureAtlas(scene.activity.getTextureManager(), 650, 154, TextureOptions.BILINEAR);
            youWinTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(youWinTextureAtlas, scene.activity, "you_win.png", 0, 0);
            youWinTextureAtlas.load();
            youWinSprite = new Sprite(0, 0, youWinTextureRegion, getVertexBufferObjectManager());

            //game over
            gameOverTextureAtlas = new BitmapTextureAtlas(scene.activity.getTextureManager(), 566, 243, TextureOptions.BILINEAR);
            gameOverTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(gameOverTextureAtlas, scene.activity, "game_over.png", 0, 0);
            gameOverTextureAtlas.load();
            gameOverSprite = new Sprite(0, 0, gameOverTextureRegion, getVertexBufferObjectManager());

            //cards deck
            cardsDeckTextureAtlas = new BitmapTextureAtlas(scene.activity.getTextureManager(), 361, 478, TextureOptions.BILINEAR);
            cardsDeckTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(cardsDeckTextureAtlas, scene.activity, "mg_minormajor/cards_deck.png", 0, 0);
            cardsDeckTextureAtlas.load();
            cardsDeckSprite = new Sprite(20, (MainActivity.CAMERA_HEIGHT - cardsDeckTextureRegion.getHeight()) / 2f, cardsDeckTextureRegion, getVertexBufferObjectManager());

            //green arrow
            greenArrowTextureAtlas = new BitmapTextureAtlas(scene.activity.getTextureManager(), 361, 478, TextureOptions.BILINEAR);
            greenArrowTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(greenArrowTextureAtlas, scene.activity, "green_arrow.png", 0, 0);
            greenArrowTextureAtlas.load();
            greenArrowDownSprite = new Sprite(200, 0, greenArrowTextureRegion, getVertexBufferObjectManager());
            greenArrowUpSprite = new Sprite(0, 0, greenArrowTextureRegion, getVertexBufferObjectManager());
            greenArrowUpSprite.setRotation(180);

            //withdraw
            withdrawTextureAtlas = new BitmapTextureAtlas(scene.activity.getTextureManager(), 295, 90, TextureOptions.BILINEAR);
            withdrawTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(withdrawTextureAtlas, scene.activity, "withdraw.png", 0, 0);
            withdrawTextureAtlas.load();
            withdrawSprite = new Sprite(0, 0, withdrawTextureRegion, getVertexBufferObjectManager());

            //cards deck
            cardsDeckTextureAtlas = new BitmapTextureAtlas(scene.activity.getTextureManager(), 361, 478, TextureOptions.BILINEAR);
            cardsDeckTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(cardsDeckTextureAtlas, scene.activity, "mg_minormajor/cards_deck.png", 0, 0);
            cardsDeckTextureAtlas.load();
            cardsDeckSprite = new Sprite(20, (MainActivity.CAMERA_HEIGHT - cardsDeckTextureRegion.getHeight()) / 2f, cardsDeckTextureRegion, getVertexBufferObjectManager());

            //cards back
            INITIAL_CARDS_BACK_POSITION_X = cardsDeckSprite.getX() + 50;
            INITIAL_CARDS_BACK_POSITION_Y = cardsDeckSprite.getY() - 17;
            cardsBackTextureAtlas = new BitmapTextureAtlas(scene.activity.getTextureManager(), 323, 418, TextureOptions.BILINEAR);
            cardsBackTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(cardsBackTextureAtlas, scene.activity, "mg_minormajor/cards_back.png", 0, 0);
            cardsBackTextureAtlas.load();
            cardsEntities = new CardEntity[3];
            for(int i=0; i<3;i++) {
                cardsEntities[i] = new CardEntity(INITIAL_CARDS_BACK_POSITION_X, INITIAL_CARDS_BACK_POSITION_Y, new Sprite(0, 0, cardsBackTextureRegion, getVertexBufferObjectManager()));
            }

            minorMajorPopup = new MinorMajorPopup();
            minorMajorPopup.lightDown(true);
        }
    }

    @Override
    public void createScene() {
        if (!initialized) {
            initialized = true;
            attachChild(backgroundSprite);
            attachChild(cardsDeckSprite);
            for(int i=0; i< cardsEntities.length; i++) {
                //attach to scene
                attachChild(cardsEntities[i]);
            }
            //attach popup
            attachChild(minorMajorPopup);
            //attach youwin
            youWinSprite.setVisible(false);
            attachChild(youWinSprite);
            //attach gameover
            gameOverSprite.setVisible(false);
            attachChild(gameOverSprite);
        }

        //remove previously
        //added front cards
        for(CardEntity cardEntity : cardsEntities) {
            if(cardEntity.cardFront != null) {
                scene.detachEntity(cardEntity.cardFront);
                cardEntity.cardFront = null;
            }
        }
    }

    @Override
    public void enterGame() {
        System.out.println("--------> enter game minor major");
        scene.activity.logicaMaquina.inicializarMinijuegoMenorMayor();
        clearPreviousHand(true);
        startNewHand();
        gameScene.appearPlayingForText(scene.activity.logicaMaquina.montoActualMenorMayor, false);
        gameScene.currentGame = GameScene.CURRENT_GAME.MINIGAME;
    }

    @Override
    public void exitGame() {

        //remove the popup
        youWinSprite.setVisible(false);
        gameOverSprite.setVisible(false);
        minorMajorPopup.lightDown(true);
        gameScene.disappearPlayingForText(false);

        //finish the game and get the
        //credits the player had before
        //playing
        float earnedCredits = scene.activity.logicaMaquina.finalizarMinijuegoMenorMayor();
        gameScene.creditsUpdateHandler.setCredits(scene.activity.maquina.montoJugador, scene.activity.maquina.montoJugador - earnedCredits, false);

    }

    public void clearPreviousHand(boolean immediate) {
        if(immediate) {
            //remove front cards
            for(CardEntity cardEntity : cardsEntities) {
                cardEntity.setPosition(INITIAL_CARDS_BACK_POSITION_X, INITIAL_CARDS_BACK_POSITION_Y);
                cardEntity.removeCardFront(true);
            }
        }
        else {

            final Sprite cardFront1 = cardsEntities[1].cardFront;
            final Sprite cardFront2 = cardsEntities[2].cardFront;

            MoveYModifier moveYModifier = new MoveYModifier(0.5f, cardFront1.getY(), MainActivity.CAMERA_HEIGHT);
            moveYModifier.setAutoUnregisterWhenFinished(true);
            moveYModifier.addModifierListener(new IModifier.IModifierListener<IEntity>() {
                @Override
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                }
                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    cardsEntities[1].setPosition(INITIAL_CARDS_BACK_POSITION_X, INITIAL_CARDS_BACK_POSITION_Y);
                    cardsEntities[1].removeCardFront(false);
                }
            });
            cardFront1.registerEntityModifier(moveYModifier);

            MoveXModifier moveXModifier = new MoveXModifier(0.5f, cardFront2.getX(), MainActivity.CAMERA_WIDTH);
            moveXModifier.setAutoUnregisterWhenFinished(true);
            moveYModifier.addModifierListener(new IModifier.IModifierListener<IEntity>() {
                @Override
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                }
                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    cardsEntities[2].setPosition(INITIAL_CARDS_BACK_POSITION_X, INITIAL_CARDS_BACK_POSITION_Y);
                    cardsEntities[2].removeCardFront(false);
                }
            });
            cardFront2.registerEntityModifier(moveXModifier);
        }
    }

    private void startNewHand() {

        cartaMiniJuegoMenorMayorMaquina = scene.activity.logicaMaquina.generarCartaMinijuegoMenorMayor(true, null);
        cartaMiniJuegoMenorMayorJugador = scene.activity.logicaMaquina.generarCartaMinijuegoMenorMayor(false, cartaMiniJuegoMenorMayorMaquina);

        //transformamos a indice 0
        cartaMiniJuegoMenorMayorMaquina--;
        cartaMiniJuegoMenorMayorJugador--;

        System.out.println("--> CARTA 1 :: " + cartaMiniJuegoMenorMayorJugador);
        System.out.println("--> CARTA 2 :: " + cartaMiniJuegoMenorMayorMaquina);

        //reset old values
        for(CardEntity cardEntity : cardsEntities) {
            cardEntity.setPosition(INITIAL_CARDS_BACK_POSITION_X, INITIAL_CARDS_BACK_POSITION_Y);
            cardEntity.setRotation(0);
            cardEntity.cardBack.setPosition(0, 0);
            cardEntity.cardBack.setRotation(0);
            cardEntity.removeCardFront(true);
        }

        //first card move
        MoveModifier moveModifier1 = new MoveModifier(1, cardsEntities[1].getX(), 420, cardsEntities[1].getY(), 210);
        RotationModifier rotationModifier1 = new RotationModifier(1, cardsEntities[1].getRotation(), INITIAL_CARDS_1_ROTATION);
        ParallelEntityModifier parallelEntityModifier1 = new ParallelEntityModifier(moveModifier1, rotationModifier1);
        parallelEntityModifier1.setAutoUnregisterWhenFinished(true);
        cardsEntities[1].registerEntityModifier(parallelEntityModifier1);

        //second card move
        MoveModifier moveModifier2 = new MoveModifier(1, cardsEntities[2].getX(), 750, cardsEntities[2].getY(), 250);
        RotationModifier rotationModifier2 = new RotationModifier(1, cardsEntities[2].getRotation(), INITIAL_CARDS_2_ROTATION);
        ParallelEntityModifier parallelEntityModifier2 = new ParallelEntityModifier(moveModifier2, rotationModifier2);
        parallelEntityModifier2.setAutoUnregisterWhenFinished(true);
        cardsEntities[2].registerEntityModifier(parallelEntityModifier2);
        parallelEntityModifier2.addModifierListener(new IModifier.IModifierListener<IEntity>() {
            @Override
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
            }
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ThreadUtil.sleep(200);
                        //load the card
                        cardsEntities[2].loadCard(cartaMiniJuegoMenorMayorMaquina);
                        //delay after card image loading to
                        //don't have a glitch on next image
                        //animation
                        ThreadUtil.sleep(200);
                        cardsEntities[2].showCard(new IModifier.IModifierListener() {
                            @Override
                            public void onModifierStarted(IModifier pModifier, Object pItem) {
                            }
                            @Override
                            public void onModifierFinished(IModifier pModifier, Object pItem) {
                                //load the card
                                cardsEntities[1].loadCard(cartaMiniJuegoMenorMayorJugador);
                                readyToPlay = true;
                            }
                        });
                        minorMajorPopup.lightUp();
                    }
                }).start();
            }
        });
    }

    private Sprite loadCardSprite(int index) {
        TextureRegion cardTextureRegion = cardTextures.get(index);
        if(cardTextureRegion == null) {
            BitmapTextureAtlas cardTextureAtlas = new BitmapTextureAtlas(scene.activity.getTextureManager(), 361, 478, TextureOptions.BILINEAR);
            cardTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(cardTextureAtlas, scene.activity, "mg_minormajor/cards_" + (index + 1) + ".png", 0, 0);
            cardTextureAtlas.load();
            cardTextures.put(index, cardTextureRegion);
        }
        Sprite cardSprite = new Sprite(0, 0, cardTextureRegion, getVertexBufferObjectManager());
        return cardSprite;
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if(gameScene.currentGame == GameScene.CURRENT_GAME.MINIGAME) {
            if(readyToPlay) {
                if(greenArrowDownSprite.contains(pSceneTouchEvent.getX(), pSceneTouchEvent.getY())) {
                    if(pSceneTouchEvent.isActionUp()) {
                        evaluatePlayerSelection(true);
                        minorMajorPopup.lightDown(false);
                    }
                }
                else if(greenArrowUpSprite.contains(pSceneTouchEvent.getX(), pSceneTouchEvent.getY())) {
                    if(pSceneTouchEvent.isActionUp()) {
                        evaluatePlayerSelection(false);
                        minorMajorPopup.lightDown(false);
                    }
                }
                else if(withdrawSprite.contains(pSceneTouchEvent.getX(), pSceneTouchEvent.getY())) {
                    withdraw();
                }
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    public void withdraw() {
        gameScene.gotoSlots();
    }

    private void evaluatePlayerSelection(boolean minor) {

        readyToPlay = false;
        final boolean[] playerWin = {scene.activity.logicaMaquina.apostarMinijuegoMenorMayor(minor)};

        cardsEntities[1].showCard(new IModifier.IModifierListener() {
            @Override
            public void onModifierStarted(IModifier pModifier, Object pItem) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ThreadUtil.sleep(350);
                        if (playerWin[0]) {

                            gameScene.bonusCreditsUpdateHandler.setCredits(scene.activity.logicaMaquina.montoActualMenorMayor * 2f, scene.activity.logicaMaquina.montoActualMenorMayor, false);
                            showHandState(true);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    ThreadUtil.sleep(500);
                                    clearPreviousHand(false);
                                    ThreadUtil.sleep(500);
                                    startNewHand();
                                }
                            }).start();
                        } else {
                            showHandState(false);

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    ThreadUtil.sleep(1500);
                                    gameScene.gotoSlots();
                                }
                            }).start();
                        }
                    }
                }).start();
            }

            @Override
            public void onModifierFinished(IModifier pModifier, Object pItem) {
                if (playerWin[0]) {
                    System.out.println("-------> PLAYER WIN !!!");
                } else {
                    System.out.println("-------> PLAYER LOSE !!!");
                }
            }
        });
    }

    private void showHandState(final boolean win) {

        final Sprite msgSprite = win ? youWinSprite : gameOverSprite;

        msgSprite.setScale(0.3f);
        msgSprite.setVisible(true);
        msgSprite.setPosition((MainActivity.CAMERA_WIDTH - msgSprite.getWidth()) / 2f, (MainActivity.CAMERA_HEIGHT - msgSprite.getHeight()) / 2f);

        //scale
        ScaleModifier scaleModifier = new ScaleModifier(0.2f, msgSprite.getScaleX(), 1f);
        //rotation
        RotationModifier rotationModifier = new RotationModifier(0.5f, 0, 360*2);

        ParallelEntityModifier parallelEntityModifier = new ParallelEntityModifier(scaleModifier, rotationModifier);
        parallelEntityModifier.setAutoUnregisterWhenFinished(true);

        parallelEntityModifier.addModifierListener(new IModifier.IModifierListener<IEntity>() {
            @Override
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
            }
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ThreadUtil.sleep(1000);
                        //move
                        MoveXModifier moveXModifier = new MoveXModifier(0.3f, msgSprite.getX(), MainActivity.CAMERA_WIDTH);
                        moveXModifier.setAutoUnregisterWhenFinished(true);
                        msgSprite.registerEntityModifier(moveXModifier);
                    }
                }).start();
            }
        });

        //execute the modifiers
        msgSprite.registerEntityModifier(parallelEntityModifier);
    }

    private class MinorMajorPopup extends Rectangle {

        private static final float WIDTH = 380;
        private static final int HEIGHT = 350;
        private Text text;

        public MinorMajorPopup() {
            super((MainActivity.CAMERA_WIDTH - WIDTH) / 2f, (MainActivity.CAMERA_HEIGHT - HEIGHT) / 2f, WIDTH, HEIGHT, MinorMajorGame.this.getVertexBufferObjectManager());

            setColor(Color.BLACK);

            greenArrowDownSprite.setPosition(30, 80);
            greenArrowUpSprite.setPosition(200, 80);
            withdrawSprite.setPosition(40, 230);

            text = new Text(20, 10, scene.resourcesManager.bigFont, "Lower or Higher?", MinorMajorGame.this.getVertexBufferObjectManager());

            attachChild(text);
            attachChild(greenArrowDownSprite);
            attachChild(greenArrowUpSprite);
            attachChild(withdrawSprite);

        }

        public void lightUp() {
            //popup
            AlphaModifier alphaModifier = new AlphaModifier(0.5f, getAlpha(), 0.7f);
            alphaModifier.setAutoUnregisterWhenFinished(true);
            registerEntityModifier(alphaModifier);
            //arrow up
            alphaModifier = new AlphaModifier(0.5f, getAlpha(), 1f);
            alphaModifier.setAutoUnregisterWhenFinished(true);
            greenArrowDownSprite.registerEntityModifier(alphaModifier);
            //arrow down
            alphaModifier = new AlphaModifier(0.5f, getAlpha(), 1f);
            alphaModifier.setAutoUnregisterWhenFinished(true);
            greenArrowUpSprite.registerEntityModifier(alphaModifier);
            //withdraw
            alphaModifier = new AlphaModifier(0.5f, getAlpha(), 1f);
            alphaModifier.setAutoUnregisterWhenFinished(true);
            withdrawSprite.registerEntityModifier(alphaModifier);
            //text
            alphaModifier = new AlphaModifier(0.5f, getAlpha(), 1f);
            alphaModifier.setAutoUnregisterWhenFinished(true);
            text.registerEntityModifier(alphaModifier);
        }

        public void lightDown(boolean immediate) {
            if(immediate) {
                setAlpha(0f);
                greenArrowDownSprite.setAlpha(0f);
                greenArrowUpSprite.setAlpha(0f);
                withdrawSprite.setAlpha(0f);
                text.setAlpha(0f);
            }
            else {
                //popup
                AlphaModifier alphaModifier = new AlphaModifier(0.5f, getAlpha(), 0f);
                alphaModifier.setAutoUnregisterWhenFinished(true);
                registerEntityModifier(alphaModifier);
                //arrow up
                alphaModifier = new AlphaModifier(0.5f, getAlpha(), 0f);
                alphaModifier.setAutoUnregisterWhenFinished(true);
                greenArrowDownSprite.registerEntityModifier(alphaModifier);
                //arrow down
                alphaModifier = new AlphaModifier(0.5f, getAlpha(), 0f);
                alphaModifier.setAutoUnregisterWhenFinished(true);
                greenArrowUpSprite.registerEntityModifier(alphaModifier);
                //withdraw
                alphaModifier = new AlphaModifier(0.5f, getAlpha(), 0f);
                alphaModifier.setAutoUnregisterWhenFinished(true);
                withdrawSprite.registerEntityModifier(alphaModifier);
                //text
                alphaModifier = new AlphaModifier(0.5f, getAlpha(), 0f);
                alphaModifier.setAutoUnregisterWhenFinished(true);
                text.registerEntityModifier(alphaModifier);
            }
        }
    }

    private class CardEntity extends Entity {

        private Sprite cardBack;
        private Sprite cardFront;

        public CardEntity(float x, float y, Sprite cardBack) {
            setPosition(x, y);
            this.cardBack = cardBack;
            attachChild(cardBack);
        }

        public void removeCardFront(boolean immediate) {
            if(cardFront != null) {
                if(immediate) {
                    setPosition(INITIAL_CARDS_BACK_POSITION_X, INITIAL_CARDS_BACK_POSITION_Y);
                    cardBack.setPosition(0, 0);
                    cardFront.setVisible(false);
                    cardFront = null;
                }
                else {
                    MoveXModifier moveModifier = new MoveXModifier(1f, cardFront.getX(), MainActivity.CAMERA_WIDTH + cardFront.getWidth());
                    moveModifier.setAutoUnregisterWhenFinished(true);
                    moveModifier.addModifierListener(new IEntityModifier.IEntityModifierListener() {
                        @Override
                        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                        }
                        @Override
                        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                            setPosition(INITIAL_CARDS_BACK_POSITION_X, INITIAL_CARDS_BACK_POSITION_Y);
                            cardBack.setPosition(0, 0);
                            cardFront.setVisible(false);
                            cardFront = null;
                        }
                    });
                }
            }
        }

        public void loadCard(int index) {
            //load card sprite
            cardFront = loadCardSprite(index);
            cardFront.setVisible(false);
            //attach it to the scene
            attachChild(cardFront);
            //reorder the z-indexes
            cardBack.setZIndex(1);
            cardFront.setZIndex(0);
            //sort childs by z-index
            sortChildren();
        }

        public void showCard(final IModifier.IModifierListener listener) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while(cardFront == null) {
                        ThreadUtil.sleep(100);
                    }
                    cardFront.setVisible(true);
                    MoveYModifier moveYModifier = new MoveYModifier(1f, cardBack.getY(), MainActivity.CAMERA_HEIGHT);
                    moveYModifier.addModifierListener(listener);
                    moveYModifier.setAutoUnregisterWhenFinished(true);
                    cardBack.registerEntityModifier(moveYModifier);
                }
            }).start();
        }
    }
}