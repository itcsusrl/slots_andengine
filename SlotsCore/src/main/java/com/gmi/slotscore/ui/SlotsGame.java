package com.gmi.slotscore.ui;

import com.gmi.slotscore.MainActivity;
import com.gmi.slotscore.constante.JugadaTipo;
import com.gmi.slotscore.dominio.JugadaLineaGanadora;
import com.gmi.slotscore.dominio.Linea;
import com.gmi.slotscore.dominio.util.Pair;
import com.gmi.slotscore.ui.scene.GameScene;
import com.gmi.slotscore.util.ThreadUtil;

import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.DelayModifier;
import org.andengine.entity.modifier.FadeInModifier;
import org.andengine.entity.modifier.FadeOutModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.ParallelEntityModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.modifier.ScaleModifier;
import org.andengine.entity.modifier.SequenceEntityModifier;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.modifier.IModifier;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by pablo on 05/11/14.
 */
public class SlotsGame extends AbstractGame implements RollerGroup.RollerGroupListener {

    private MainActivity activity;
    private GameScene gameScene;

    //enums
    private enum PRIZE_ELEMENT_TYPE {LINE_PRIZE, NON_LINE_PRIZE, SHOW_ALL_WINNING_LINES}
    public enum SLOTS_GAME_STATE {IDLE, BETTING, PRIZING}

    //sky
    private Sprite skySprite;

    //net
    private Sprite netSprite;

    //marco superior
    private Sprite marcoSuperiorSprite;

    //marco inferior
    private Sprite marcoInferiorSprite;

    //bonus game
    private Sprite bonusGameSprite;

    //rollers
    private RollerGroup rollerGroup;

    //playing lines
    private Sprite playingLinesGrid;
    private PlayingLineCircle[] playingLinesCircles = new PlayingLineCircle[10];

    //player money before betting
    private float betPreviousPlayerAmount;

    private boolean cancelPrizing;

    private SLOTS_GAME_STATE state = SLOTS_GAME_STATE.IDLE;

    private List<Pair<Integer, Integer>> playingLinesPositions = new ArrayList<Pair<Integer,Integer>>();
    private List<Pair<Integer, Integer>> playingLinesCirclesPositions = new ArrayList<Pair<Integer,Integer>>();

    public SlotsGame(GameScene gameScene, float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(gameScene, pX, pY, pWidth, pHeight, pVertexBufferObjectManager);
        activity = (MainActivity) gameScene.activity;
        this.gameScene = gameScene;
    }

    @Override
    public void loadResources() {

        //sky
        skySprite = new Sprite(0, 0, scene.resourcesManager.skyRegion, getVertexBufferObjectManager());
        skySprite.setSize(1024, 768);

        //net
        netSprite = new Sprite(0, -25, scene.resourcesManager.netTextureRegion, getVertexBufferObjectManager());
        netSprite.setSize(1024, 768);

        //marco superior
        marcoSuperiorSprite = new Sprite(0, 0, scene.resourcesManager.marcoSuperiorTextureRegion, getVertexBufferObjectManager());
        marcoSuperiorSprite.setSize(1024, 161);

        //marco inferior
        marcoInferiorSprite = new Sprite(0, 0, scene.resourcesManager.marcoInferiorTextureRegion, getVertexBufferObjectManager());
        marcoInferiorSprite.setSize(1024, 105);

        //bonus game
        bonusGameSprite = new Sprite(0, 0, scene.resourcesManager.bonusGameTextureRegion, getVertexBufferObjectManager());

        //playing lines positions
        playingLinesPositions.add(new Pair<Integer, Integer>(0, 195));
        playingLinesPositions.add(new Pair<Integer, Integer>(0, 54));
        playingLinesPositions.add(new Pair<Integer, Integer>(0, 380));
        playingLinesPositions.add(new Pair<Integer, Integer>(0, 5));
        playingLinesPositions.add(new Pair<Integer, Integer>(0, 28));
        playingLinesPositions.add(new Pair<Integer, Integer>(0, 100));
        playingLinesPositions.add(new Pair<Integer, Integer>(0, 247));
        playingLinesPositions.add(new Pair<Integer, Integer>(0, 285));
        playingLinesPositions.add(new Pair<Integer, Integer>(0, 43));
        playingLinesPositions.add(new Pair<Integer, Integer>(0, 88));

        //playing lines circles positions
        playingLinesCirclesPositions.add(new Pair<Integer, Integer>(19, 194));
        playingLinesCirclesPositions.add(new Pair<Integer, Integer>(19, 54));
        playingLinesCirclesPositions.add(new Pair<Integer, Integer>(19, 380));
        playingLinesCirclesPositions.add(new Pair<Integer, Integer>(19, 5));
        playingLinesCirclesPositions.add(new Pair<Integer, Integer>(19, 428));
        playingLinesCirclesPositions.add(new Pair<Integer, Integer>(19, 100));
        playingLinesCirclesPositions.add(new Pair<Integer, Integer>(19, 333));
        playingLinesCirclesPositions.add(new Pair<Integer, Integer>(19, 287));
        playingLinesCirclesPositions.add(new Pair<Integer, Integer>(19, 148));
        playingLinesCirclesPositions.add(new Pair<Integer, Integer>(19, 240));

    }

    @Override
    public void createScene() {

        //create the roller group
        rollerGroup = new RollerGroup(scene, this);

        setWidth(skySprite.getWidth());
        setHeight(skySprite.getHeight());

        //add main sprite
        attachChild(skySprite);

        //add rollers
        attachChild(rollerGroup);

        //add main sprite childrens
        attachChild(netSprite);

        //add playing lines
        addPlayingLines();

        //add upper/lower frames
        attachChild(marcoSuperiorSprite);
        attachChild(marcoInferiorSprite);

        //add bonus
        bonusGameSprite.setVisible(false);
        attachChild(bonusGameSprite);

        //set positions
        marcoInferiorSprite.setY(skySprite.getHeight() - marcoInferiorSprite.getHeight());

    }

    @Override
    public void enterGame() {
        state = SLOTS_GAME_STATE.IDLE;
        gameScene.currentGame = GameScene.CURRENT_GAME.SLOTS;
        if(activity.logicaMaquina.cantidadTiradasGratis > 0) {
            gameScene.appearFreeRollsText(activity.logicaMaquina.cantidadTiradasGratis, false);
        }
    }

    @Override
    public void exitGame() {
        gameScene.disappearFreeRollsText();
    }

    @Override
    public void finishRolling() {
        if(activity.maquina.logicaMaquina.jugadasLineasGanadoras.size() == 0) {
            state = SLOTS_GAME_STATE.IDLE;
            //update credits
            gameScene.creditsUpdateHandler.setCredits(activity.maquina.montoJugador, betPreviousPlayerAmount, true);
        }
        else {
            //evaluate the prizes to show
            evaluatePrizes();
        }
    }

    private void evaluatePrizes() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                List<JugadaLineaGanadora> jugadaLineaGanadoraList = activity.maquina.logicaMaquina.jugadasLineasGanadoras;
                if (jugadaLineaGanadoraList.size() > 0) {

                    //create a prizes chain
                    final LinkedList<PrizeElement> prizes = new LinkedList<PrizeElement>();

                    //iterate the list of prizes
                    List<JugadaLineaGanadora> linePrizes = new ArrayList<JugadaLineaGanadora>();
                    List<JugadaLineaGanadora> nonlinePrizes = new ArrayList<JugadaLineaGanadora>();
                    for (JugadaLineaGanadora jugadaLineaGanadora : jugadaLineaGanadoraList) {
                        if (jugadaLineaGanadora.getLinea() != null) {
                            linePrizes.add(jugadaLineaGanadora);
                        } else {
                            nonlinePrizes.add(jugadaLineaGanadora);
                        }
                    }

                    //add line prizes
                    if (linePrizes.size() > 0) {
                        List<Linea> winningLines = new ArrayList<Linea>();
                        for (JugadaLineaGanadora jugadaLineaGanadora : linePrizes) {
                            PrizeElement prizeElement = new PrizeElement();
                            prizeElement.type = PRIZE_ELEMENT_TYPE.LINE_PRIZE;
                            prizeElement.jugadaLineaGanadora = jugadaLineaGanadora;
                            prizes.offer(prizeElement);
                            winningLines.add(jugadaLineaGanadora.getLinea());
                        }
                        //add a prize chain that shows
                        //all the winning lines at once
                        PrizeElement prizeElement = new PrizeElement();
                        prizeElement.type = PRIZE_ELEMENT_TYPE.SHOW_ALL_WINNING_LINES;
                        prizeElement.winningLines = winningLines;
                        prizes.offer(prizeElement);
                    }

                    //add nonline prizes
                    if (nonlinePrizes.size() > 0) {

                        //reference bonus game
                        JugadaLineaGanadora jugadaLineaGanadoraBonusGame = null;

                        //add bonus first
                        for (JugadaLineaGanadora jugadaLineaGanadora : nonlinePrizes) {
                            if(jugadaLineaGanadora.getTipo() != JugadaTipo.BONUS_GAME) {
                                PrizeElement prizeElement = new PrizeElement();
                                prizeElement.type = PRIZE_ELEMENT_TYPE.NON_LINE_PRIZE;
                                prizeElement.jugadaLineaGanadora = jugadaLineaGanadora;
                                prizes.offer(prizeElement);
                            }
                            else {
                                jugadaLineaGanadoraBonusGame = jugadaLineaGanadora;
                            }
                        }

                        //add bonus game at the end
                        if(jugadaLineaGanadoraBonusGame != null) {
                            PrizeElement prizeElement = new PrizeElement();
                            prizeElement.type = PRIZE_ELEMENT_TYPE.NON_LINE_PRIZE;
                            prizeElement.jugadaLineaGanadora = jugadaLineaGanadoraBonusGame;
                            prizes.offer(prizeElement);
                        }
                    }

                    //show the prizes on the ui
                    showPrizeElementsChain(prizes, betPreviousPlayerAmount);
                }
            }
        });
    }

    private void showPrizeElementsChain(final LinkedList<PrizeElement> prizes, final float previousPrize) {

        //get prize to show
        final PrizeElement prizeElement = prizes.poll();
        if(prizeElement != null) {

            //search for bonus prize
           boolean haveBonus = false;
            for(JugadaLineaGanadora jugadaLineaGanadora : activity.maquina.logicaMaquina.jugadasLineasGanadoras) {
                if(jugadaLineaGanadora.getTipo() == JugadaTipo.BONUS) {
                    haveBonus = true;
                    break;
                }
            }

            //if there is no bonus prize
            //then refresh the possible > 0
            //free rolls
            if(!haveBonus) {
                refreshFreeRolls();
            }

            state = SLOTS_GAME_STATE.PRIZING;
            if(cancelPrizing) {
                //increment credits
                gameScene.creditsUpdateHandler.setCredits(activity.maquina.montoJugador, previousPrize, false, 1f, new GameScene.CreditsUpdateHandlerListener() {
                    @Override
                    public void creditsUpdateFinished() {
                        for(JugadaLineaGanadora jugadaLineaGanadora : activity.maquina.logicaMaquina.jugadasLineasGanadoras) {
                            if(jugadaLineaGanadora.getTipo() == JugadaTipo.BONUS) {
                                gameScene.appearFreeRollsText(activity.logicaMaquina.cantidadTiradasGratis, true);
                            }
                            if(jugadaLineaGanadora.getTipo() == JugadaTipo.BONUS_GAME) {
                                moveToMinigameMinorMajor();
                            }
                        }

                        state = SLOTS_GAME_STATE.IDLE;
                    }
                });
            }
            else {

                if(prizeElement.type == PRIZE_ELEMENT_TYPE.LINE_PRIZE) {

                    //calculate the amount winned
                    //with the current prize
                    final float montoGanadoJugada = prizeElement.jugadaLineaGanadora.getJugadaGanadora().multiplicador * activity.maquina.dineroApostadoPorLinea;

                    //get the line ui object
                    final PlayingLine playingLine = createPlayingLine(prizeElement.jugadaLineaGanadora.getLinea().getId()-1);
                    playingLinesGrid.attachChild(playingLine);

                    //create winning lines animation
                    LoopEntityModifier loopEntityModifier = new LoopEntityModifier(
                            new SequenceEntityModifier(
                                    new FadeInModifier(0.35f),
                                    new FadeOutModifier(0.15f)), 4);
                    loopEntityModifier.addModifierListener(new IModifier.IModifierListener<IEntity>() {
                        @Override
                        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                        }
                        @Override
                        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                            showPrizeElementsChain(prizes, previousPrize + montoGanadoJugada);
                            scene.detachEntity(playingLine);
                        }
                    });

                    rollerGroup.shine(prizeElement.jugadaLineaGanadora.getJugadaLinea().getPosicionFiguras(), null);

                    //show prize popup animation
                    gameScene.moneyPrizeUpdateHandler.setPrize(montoGanadoJugada);

                    //increment credits
                    gameScene.creditsUpdateHandler.setCredits(previousPrize + montoGanadoJugada, previousPrize, false, 2.4f);

                    //execute animation
                    loopEntityModifier.setAutoUnregisterWhenFinished(true);
                    playingLine.registerEntityModifier(loopEntityModifier);
                }
                else if(prizeElement.type == PRIZE_ELEMENT_TYPE.SHOW_ALL_WINNING_LINES) {
                    for(Linea linea : prizeElement.winningLines) {
                        //get the line ui object
                        PlayingLine playingLine = createPlayingLine(linea.getId()-1);
                        playingLinesGrid.attachChild(playingLine);

                        SequenceEntityModifier sequenceEntityModifier = new SequenceEntityModifier(
                                new FadeInModifier(0.25f),
                                new FadeOutModifier(0.25f));

                        //create winning lines animation
                        LoopEntityModifier loopEntityModifier = new LoopEntityModifier(sequenceEntityModifier, 3);
                        loopEntityModifier.setAutoUnregisterWhenFinished(true);
                        loopEntityModifier.addModifierListener(new IModifier.IModifierListener<IEntity>() {
                            @Override
                            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                            }
                            @Override
                            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                                showPrizeElementsChain(prizes, previousPrize);
                                scene.detachEntity(pItem);
                            }
                        });

                        //execute animation
                        playingLine.registerEntityModifier(loopEntityModifier);
                    }
                }
                else if(prizeElement.type == PRIZE_ELEMENT_TYPE.NON_LINE_PRIZE) {

                    if(prizeElement.jugadaLineaGanadora.getTipo() == JugadaTipo.SCATTER) {

                        System.out.println("----------------> SCATTER PRIZE");

                        //calculate the amount winned
                        //with the current prize
                        final float montoGanadoJugada = prizeElement.jugadaLineaGanadora.getJugadaGanadora().multiplicador * activity.maquina.dineroApostadoPorLinea;

                        //shineAndRotate rollers winning figures
                        rollerGroup.shine(prizeElement.jugadaLineaGanadora.getJugadaLinea().getPosicionFiguras(), new RollerGroup.ShineListener() {
                            @Override
                            public void shineFinished() {
                                System.out.println("----------------> SHINE SCATTER");
                                showPrizeElementsChain(prizes, previousPrize + montoGanadoJugada);
                            }
                        });

                        //show prize popup animation
                        gameScene.moneyPrizeUpdateHandler.setPrize(montoGanadoJugada);

                        //increment credits
                        gameScene.creditsUpdateHandler.setCredits(previousPrize + montoGanadoJugada, previousPrize, false, 2.4f);
                    }
                    else if(prizeElement.jugadaLineaGanadora.getTipo() == JugadaTipo.BONUS) {

                        System.out.println("----------------> BONUS PRIZE");

                        //show prize popup animation
                        gameScene.freeRollPrizeUpdateHandler.setFreeRolls(activity.logicaMaquina.cantidadTiradasGratis);
                        //shineAndRotate rollers winning figures
                        rollerGroup.shine(prizeElement.jugadaLineaGanadora.getJugadaLinea().getPosicionFiguras(), new RollerGroup.ShineListener() {
                            @Override
                            public void shineFinished() {
                                System.out.println("----------------> SHINE BONUS");
                                showPrizeElementsChain(prizes, previousPrize);
                            }
                        });
                    }
                    else if(prizeElement.jugadaLineaGanadora.getTipo() == JugadaTipo.BONUS_GAME) {
                        showBonusGamePrize(prizeElement, prizes, previousPrize);
                    }
                    else {
                        System.out.println("---------------> OTHER PRIZE");
                        showPrizeElementsChain(prizes, previousPrize);
                    }
                }
            }
        }
        else {
            state = SLOTS_GAME_STATE.IDLE;
            refreshFreeRolls();
        }
    }

    private void refreshFreeRolls() {
        //refresh free rolls text
        if(activity.logicaMaquina.cantidadTiradasGratis > 0) {
            gameScene.appearFreeRollsText(activity.logicaMaquina.cantidadTiradasGratis, true);
        }
    }

    private void showBonusGamePrize(PrizeElement prizeElement, final LinkedList<PrizeElement> prizes, final float previousPrize) {

        //shine rollers winning figures
        rollerGroup.shine(prizeElement.jugadaLineaGanadora.getJugadaLinea().getPosicionFiguras(), null);

        bonusGameSprite.setScale(0.3f);
        bonusGameSprite.setVisible(true);
        bonusGameSprite.setPosition((MainActivity.CAMERA_WIDTH - bonusGameSprite.getWidth()) / 2f, (MainActivity.CAMERA_HEIGHT - bonusGameSprite.getHeight()) / 2f);

        //scale
        ScaleModifier scaleModifier = new ScaleModifier(0.2f, bonusGameSprite.getScaleX(), 1.4f);
        //rotation
        RotationModifier rotationModifier = new RotationModifier(0.5f, 0, 360*2);

        ParallelEntityModifier parallelEntityModifier = new ParallelEntityModifier(scaleModifier, rotationModifier);
        parallelEntityModifier.setAutoUnregisterWhenFinished(true);

        parallelEntityModifier.addModifierListener(new IModifier.IModifierListener<IEntity>() {
            @Override
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
            }
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        ThreadUtil.sleep(1000);
                        moveToMinigameMinorMajor();
                        showPrizeElementsChain(prizes, previousPrize);
                    }
                }).start();
            }
        });

        //execute the modifiers
        bonusGameSprite.registerEntityModifier(parallelEntityModifier);

    }

    private void moveToMinigameMinorMajor() {
        MoveXModifier moveXModifier = new MoveXModifier(0.3f, bonusGameSprite.getX(), -(bonusGameSprite.getWidth()*1.5f));
        moveXModifier.setAutoUnregisterWhenFinished(true);
        bonusGameSprite.registerEntityModifier(moveXModifier);
        gameScene.gotoMinigameMinorMajor();
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        if(gameScene.currentGame == GameScene.CURRENT_GAME.SLOTS) {
            if(pSceneTouchEvent.isActionUp()) {
                if(pSceneTouchEvent.getX() < MainActivity.CAMERA_WIDTH / 2f) {
                    incrementPlayingLines();
                }
                else {
                    if(state == SLOTS_GAME_STATE.IDLE) {
                        if(rollerGroup.state == RollerGroup.STATE.STOPPED) {
                            bet();
                            cancelPrizing = false;
                        }
                    }

                    if(state == SLOTS_GAME_STATE.PRIZING) {
                        if(!cancelPrizing) {
                            cancelPrizing = true;
                        }
                    }

                    if(rollerGroup.state == RollerGroup.STATE.ROLLING) {
                        rollerGroup.stop();
                    }
                }

                System.out.println("-----------------> STATE :: " + state + " ____ cancePrizing :: " + cancelPrizing);

            }
            return true;
        }
        else {
            return false;
        }
    }

    private void bet() {

        state = SLOTS_GAME_STATE.BETTING;

        //TODO change me, currently hardcoded
        activity.maquina.dineroApostadoPorLinea = 1f;

        //reset user credits
        if(activity.logicaMaquina.cantidadTiradasGratis <= 0) {
            //save the previous value
            betPreviousPlayerAmount = activity.maquina.montoJugador - (activity.maquina.dineroApostadoPorLinea * activity.maquina.lineasApostadas);
            //update ui
            gameScene.creditsUpdateHandler.setCredits(betPreviousPlayerAmount, betPreviousPlayerAmount + (activity.maquina.dineroApostadoPorLinea * activity.maquina.lineasApostadas), false, 0.5f);
        }
        else {
            if(activity.logicaMaquina.cantidadTiradasGratis == 1) {
                //animate out free rolls text
                gameScene.disappearFreeRollsText();
            }
            //save the previous value
            betPreviousPlayerAmount = activity.maquina.montoJugador;
        }

        //execute the bet
        float ultimoMontoJugador = activity.logicaMaquina.apostarSlots(false);

        //search for bonus
        boolean existsBonus = false;
        List<JugadaLineaGanadora> jugadaLineaGanadoraList = activity.logicaMaquina.jugadasLineasGanadoras;
        for(JugadaLineaGanadora jugadaLineaGanadora : jugadaLineaGanadoraList) {
            if(jugadaLineaGanadora.getTipo() == JugadaTipo.BONUS) {
                existsBonus = true;
            }
        }

        //refresh the ui
        //for free rolls
        if(!existsBonus && activity.logicaMaquina.cantidadTiradasGratis > 0) {
            refreshFreeRolls();
        }

        //get the bet result
        int[] tiradaActualIndices = activity.logicaMaquina.tiradaActualIndices;

        //get the possible fictional roll
        int[][] fictional = activity.logicaMaquina.tiradaFicticia;

        //adjust indexes to be zero-based
        for(int i=0; i<tiradaActualIndices.length; i++) {
            tiradaActualIndices[i]--;
        }

        //do the ui roll
        if(fictional == null) {
            rollerGroup.roll(tiradaActualIndices); //real roll
        }
        else {
            rollerGroup.rollFictional(fictional); //fictional roll
        }

        //show roll information in stdout
        gameScene.showRollInformation();
    }

    private class PrizeElement {
        public PRIZE_ELEMENT_TYPE type;
        public JugadaLineaGanadora jugadaLineaGanadora;
        public List<Linea> winningLines;
    }

    private void incrementPlayingLines() {
        activity.maquina.logicaMaquina.incrementarLineas();

        //light up last line circle
        playingLineCircleLightUp(activity.maquina.lineasApostadas-1);

        //light up last line
        playingLinesLightUp(activity.maquina.lineasApostadas-1);

        //light down lines after last line circle
        for(int i=activity.maquina.lineasApostadas-1; i<activity.maquina.configuracion.getLineas().size(); i++ ) {
            playingLineCircleLightDown(i);
        }
    }

    private void playingLinesCirclesControl() {
        for(int i=0; i<playingLinesCircles.length; i++) {
            float newAlphaValue = 0;
            if(i < activity.maquina.lineasApostadas) {
                newAlphaValue = 1;
            }

            AlphaModifier alphaModifier = new AlphaModifier(0.5f, playingLinesCircles[i].getAlpha(), newAlphaValue);
            alphaModifier.setAutoUnregisterWhenFinished(true);
            playingLinesCircles[i].registerEntityModifier(alphaModifier);

        }
    }

    private void playingLineCircleLightUp(int index) {
        AlphaModifier alphaModifier = new AlphaModifier(0.5f, playingLinesCircles[index].getAlpha(), 1f);
        alphaModifier.setAutoUnregisterWhenFinished(true);
        playingLinesCircles[index].registerEntityModifier(alphaModifier);
    }

    private void playingLineCircleLightDown(int index) {
        AlphaModifier alphaModifier = new AlphaModifier(0.5f, playingLinesCircles[index].getAlpha(), 0f);
        alphaModifier.setAutoUnregisterWhenFinished(true);
        playingLinesCircles[index].registerEntityModifier(alphaModifier);
    }

    private void playingLinesLightUp(int index) {

        PlayingLine playingLine = createPlayingLine(index);
        FadeInModifier fadeInModifier = new FadeInModifier(0.25f);
        DelayModifier delayModifier = new DelayModifier(1f);
        FadeOutModifier fadeOutModifier = new FadeOutModifier(0.25f);
        SequenceEntityModifier sequenceEntityModifier = new SequenceEntityModifier(fadeInModifier, delayModifier, fadeOutModifier);
        sequenceEntityModifier.setAutoUnregisterWhenFinished(true);

        if(playingLine.getParent() == null) {
            sequenceEntityModifier.addModifierListener(new IModifier.IModifierListener<IEntity>() {
                @Override
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                }
                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                    scene.detachEntity(pItem);
                }
            });
            playingLine.setAlpha(0f);
            playingLine.registerEntityModifier(sequenceEntityModifier);
            playingLinesGrid.attachChild(playingLine);
        }
    }

    private PlayingLine createPlayingLine(int index) {
        Linea linea = activity.maquina.configuracion.getLineas().get(index);
        Pair<Integer, Integer> playingLinePosition = playingLinesPositions.get(index);
        PlayingLine playingLine = new PlayingLine(scene, linea, playingLinePosition.value1, playingLinePosition.value2);
        return playingLine;
    }

    private void addPlayingLines() {

        playingLinesGrid = new Sprite(0, 180, scene.resourcesManager.playingLinesGridTextureRegion, getVertexBufferObjectManager());
        attachChild(playingLinesGrid);

        int LINES_QNTY = activity.maquina.configuracion.getLineas().size();
        for(int i=0; i<LINES_QNTY; i++) {

            Linea linea = activity.maquina.configuracion.getLineas().get(i);

            //attach circles
            Pair<Integer, Integer> playingLineCirclePosition = playingLinesCirclesPositions.get(i);
            PlayingLineCircle playingLineCircle = new PlayingLineCircle(scene, linea, playingLineCirclePosition.value1, playingLineCirclePosition.value2);
            playingLinesCircles[i] = playingLineCircle;
            playingLinesGrid.attachChild(playingLineCircle);
        }

        //show the playing lines
        playingLinesCirclesControl();
    }
}