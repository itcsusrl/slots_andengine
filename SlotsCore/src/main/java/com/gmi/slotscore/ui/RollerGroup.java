package com.gmi.slotscore.ui;

import com.gmi.slotscore.MainActivity;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.IOnAreaTouchListener;
import org.andengine.entity.scene.ITouchArea;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.input.touch.TouchEvent;

/**
 * Created by pablo on 25/09/14.
 */
public class RollerGroup extends Rectangle implements IOnAreaTouchListener, Roller.RollerListener {

    public enum STATE {STOPPED, ROLLING}

    private MainActivity activity;
    private BaseScene scene;
    private Roller[] rollers = new Roller[5];
    public STATE state;
    private RollerGroupListener listener;

    public RollerGroup(BaseScene scene, RollerGroupListener listener) {
        super(100, 0, 840, MainActivity.CAMERA_HEIGHT, scene.activity.getVertexBufferObjectManager());
        this.scene = scene;
        this.setAlpha(0f);
        this.listener = listener;
        this.activity = scene.activity;
        this.state = STATE.STOPPED;
        addRollers();
    }

    private void addRollers() {

        Roller roller0 = new Roller(scene, activity.maquina.configuracion.getRodillos().get(0).getFiguras(), 0, this, 0f);
        scene.registerTouchArea(roller0);
        attachChild(roller0);
        rollers[0] = roller0;

        Roller roller1 = new Roller(scene, activity.maquina.configuracion.getRodillos().get(1).getFiguras(), 170, this, 0.1f);
        scene.registerTouchArea(roller1);
        attachChild(roller1);
        rollers[1] = roller1;

        Roller roller2 = new Roller(scene, activity.maquina.configuracion.getRodillos().get(2).getFiguras(), 340, this, 0.2f);
        scene.registerTouchArea(roller2);
        attachChild(roller2);
        rollers[2] = roller2;

        Roller roller3 = new Roller(scene, activity.maquina.configuracion.getRodillos().get(3).getFiguras(), 510, this, 0.3f);
        scene.registerTouchArea(roller3);
        attachChild(roller3);
        rollers[3] = roller3;

        Roller roller4 = new Roller(scene, activity.maquina.configuracion.getRodillos().get(4).getFiguras(), 680, this, 0.4f);
        scene.registerTouchArea(roller4);
        attachChild(roller4);
        rollers[4] = roller4;

    }

    public void roll(int[] rollIndexes) {
        if(state == STATE.STOPPED) {
            state = STATE.ROLLING;
            for(int i=0; i<rollers.length; i++) {
                if(rollers[i] != null) { //--> IF only for dev purposes
                    rollers[i].roll(rollIndexes[i]);
                }
            }
        }
    }

    public void rollFictional(int[][] fictional) {
        if(state == STATE.STOPPED) {
            state = STATE.ROLLING;
            for(int i=0; i<rollers.length; i++) {
                if(rollers[i] != null) { //--> IF only for dev purposes
                    rollers[i].rollFictional(fictional[i]);
                }
            }
        }
    }

    public interface ShineListener {
        void shineFinished();
    }

    public void shine(final int[][] shineIndexes, final ShineListener listener) {

        AnimatedSprite.IAnimationListener animationListener = new AnimatedSprite.IAnimationListener() {
            @Override
            public void onAnimationStarted(AnimatedSprite pAnimatedSprite, int pInitialLoopCount) {
            }
            @Override
            public void onAnimationFrameChanged(AnimatedSprite pAnimatedSprite, int pOldFrameIndex, int pNewFrameIndex) {
            }
            @Override
            public void onAnimationLoopFinished(AnimatedSprite pAnimatedSprite, int pRemainingLoopCount, int pInitialLoopCount) {
            }
            @Override
            public void onAnimationFinished(AnimatedSprite pAnimatedSprite) {
                pAnimatedSprite.setVisible(false);
                scene.detachEntity(pAnimatedSprite);
            }
        };

        for(int j=0; j<shineIndexes.length; j++) {
            for(int k=0; k<shineIndexes[0].length; k++) {
                int index = shineIndexes[j][k];
                if(index != -1) {
                    if(rollers[j] != null) {
                        rollers[j].shineAndRotate(index, 180, animationListener);
                    }
                }
            }
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2500);
                    if(listener != null) {
                        listener.shineFinished();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void stop() {
        if(state == STATE.ROLLING) {
            for(int i=0; i<rollers.length; i++) {
                if(rollers[i] != null) { // ----> IF only for dev purposes
                    rollers[i].stop();
                }
            }
        }
    }

    private void evaluateRollersStopped() {
        boolean stopped = true;
        for(int i=0; i<rollers.length; i++) {
            if(rollers[i] != null) { // ----> DEV ONLY
                if(rollers[i].state != Roller.ROLLER_STATE.STOPPED) {
                    stopped = false;
                    break;
                }
            }
        }

        if(stopped) {
            listener.finishRolling();
            state = STATE.STOPPED;
        }
    }

    @Override
    public boolean onAreaTouched(TouchEvent pSceneTouchEvent, ITouchArea pTouchArea, float pTouchAreaLocalX, float pTouchAreaLocalY) {

        return true;
    }

    @Override
    public void finishRolling() {
        evaluateRollersStopped();
    }

    public interface RollerGroupListener {
        void finishRolling();
    }
}
