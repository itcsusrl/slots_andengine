package com.gmi.slotscore.ui;

import android.app.Activity;

import com.gmi.slotscore.MainActivity;
import com.gmi.slotscore.util.CameraTransformerHandler;
import com.gmi.slotscore.util.ResourcesManager;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.entity.IEntity;
import org.andengine.entity.scene.Scene;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.util.adt.pool.EntityDetachRunnablePoolUpdateHandler;

/**
 * Created by pablo on 01/12/14.
 */
public abstract class BaseScene extends Scene {

    protected Engine engine;
    public MainActivity activity;
    public ResourcesManager resourcesManager;
    protected VertexBufferObjectManager vbom;
    protected SmoothCamera camera;

    private EntityDetachRunnablePoolUpdateHandler detachPoolHandler;

    public BaseScene() {
        this.resourcesManager = ResourcesManager.getInstance();
        this.engine = resourcesManager.engine;
        this.activity = resourcesManager.activity;
        this.vbom = resourcesManager.vbom;
        this.camera = resourcesManager.camera;
        createScene();

        //update handlers
        detachPoolHandler =  new EntityDetachRunnablePoolUpdateHandler();

        registerUpdateHandler(detachPoolHandler);
    }

    public abstract void createScene();

    public abstract void onBackKeyPressed();

    public abstract SceneManager.SceneType getSceneType();

    public abstract void disposeScene();

    public void detachEntity(IEntity entity) {
        detachPoolHandler.scheduleDetach(entity);
    }
}
