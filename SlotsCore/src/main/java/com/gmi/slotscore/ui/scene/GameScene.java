package com.gmi.slotscore.ui.scene;

import com.gmi.slotscore.MainActivity;
import com.gmi.slotscore.dominio.JugadaLineaGanadora;
import com.gmi.slotscore.ui.BaseScene;
import com.gmi.slotscore.ui.SceneManager;
import com.gmi.slotscore.ui.SlotsGame;
import com.gmi.slotscore.ui.minigame.MinorMajorGame;
import com.gmi.slotscore.util.CameraTransformerHandler;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.IUpdateHandler;
import org.andengine.entity.Entity;
import org.andengine.entity.IEntity;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.IEntityModifier;
import org.andengine.entity.modifier.LoopEntityModifier;
import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.RotationModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.background.Background;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;
import org.andengine.util.modifier.IModifier;
import org.andengine.util.modifier.ease.EaseLinear;

import java.util.List;

/**
 * Created by pablo on 03/12/14.
 */
public class GameScene extends BaseScene implements CameraTransformerHandler.CameraTransformerListener {

    private static final String INITIAL_CREDITS_TITLE = "CREDITS :: ";
    private static final String INITIAL_CREDITS_VALUE = "            ";

    private static final String INITIAL_FREE_ROLLS_TITLE = "FREE ROLLS :: ";
    private static final String INITIAL_FREE_ROLLS_VALUE = "   ";

    private static final String INITIAL_PLAYING_FOR_TITLE = "PLAYING FOR :: ";
    private static final String INITIAL_PLAYING_FOR_VALUE = "            ";

    public static final int CURRENCY_MULTIPLIER = 100;

    //enums
    public enum CURRENT_GAME {SLOTS, MINIGAME,MOVING}
    private enum PRIZE_HANDLER_STATE {NOT_VISIBLE, INITIALIZING, APPEARING, SHOWING_PRIZE, DISAPPEARING}

    //information hud
    private HUD hud;

    //texts
    private Text creditsText;
    private Text freeRollsText;
    private Text playingForText;

    //popup back
    private Sprite moneyPopupBackSprite;

    //free roll back
    private Sprite freeRollBackSprite;

    //loading icon
    private Sprite loadingIconSprite;

    //update handlers
    public CreditsUpdateHandler creditsUpdateHandler;
    public MoneyPrizeUpdateHandler moneyPrizeUpdateHandler;
    public FreeRollPrizeUpdateHandler freeRollPrizeUpdateHandler;
    public BonusCreditsUpdateHandler bonusCreditsUpdateHandler;
    public CameraTransformerHandler cameraTransformerHandler;

    //money prize entity
    private MoneyPrizePopupEntity moneyPrizePopupEntity;

    //free roll entity
    private FreeRollPrizePopupEntity freeRollPrizePopupEntity;

    //loading entity
    private LoadingEntity loadingEntity;

    //minigames
    public MinorMajorGame minorMajorMinigame;
    public SlotsGame slotsGame;

    public CURRENT_GAME currentGame = CURRENT_GAME.SLOTS;

    @Override
    public void createScene() {

        //basic ui config
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        //basic configuration
        setBackground(new Background(0.05804f, 0.1274f, 0.1784f));

        //create the base game entities
        createBaseEntities();

        //load slots game resources
        slotsGame.loadResources();
        minorMajorMinigame.load();

        //create slots game
        slotsGame.createScene();
        attachChild(slotsGame);

        //create minigame
        attachChild(minorMajorMinigame);

        //zoom
        camera.setZoomFactorDirect(MainActivity.CAMERA_FULL_ZOOM);

        //configure touch
        configureTouch();

        //configure the update handlers
        configureUpdateHandlers();

        //configure hud
        configureHUD();

        //show the credits
        creditsUpdateHandler.setCredits(activity.maquina.montoJugador, activity.maquina.montoJugador, true);


    }

    private void createBaseEntities() {

        //slots game
        slotsGame = new SlotsGame(this, 0, 0, 0, 0, vbom);

        //minigames
        minorMajorMinigame = new MinorMajorGame(this, MainActivity.CAMERA_WIDTH, 0, MainActivity.CAMERA_WIDTH, MainActivity.CAMERA_HEIGHT, vbom);

        //prize background
        moneyPopupBackSprite = new Sprite(0, 0, resourcesManager.popupBackTextureRegion, vbom);
        moneyPopupBackSprite.setAlpha(0.7f);

        //free roll
        freeRollBackSprite = new Sprite(0, 0, resourcesManager.freeRollBackTextureRegion, vbom);
        freeRollBackSprite.setAlpha(0.7f);

        //texts
        creditsText = new Text(0, 0, resourcesManager.bigFont, INITIAL_CREDITS_TITLE + INITIAL_CREDITS_VALUE, new TextOptions(HorizontalAlign.LEFT), vbom);
        freeRollsText = new Text(0, 0, resourcesManager.bigFont, INITIAL_FREE_ROLLS_TITLE + INITIAL_FREE_ROLLS_VALUE, new TextOptions(HorizontalAlign.LEFT), vbom);
        playingForText = new Text(0, 0, resourcesManager.bigFont, INITIAL_PLAYING_FOR_TITLE + INITIAL_PLAYING_FOR_VALUE, new TextOptions(HorizontalAlign.LEFT), vbom);

        //hud entities
        moneyPrizePopupEntity = new MoneyPrizePopupEntity();
        freeRollPrizePopupEntity = new FreeRollPrizePopupEntity();

        //loading layer
        loadingEntity = new LoadingEntity();

        //hud
        hud = new HUD();

    }

    @Override
    public void onBackKeyPressed() {
        if(currentGame == CURRENT_GAME.MINIGAME) {
            minorMajorMinigame.withdraw();
        }
        else {
            SceneManager.getInstance().loadMenuScene(engine);
        }
    }

    @Override
    public SceneManager.SceneType getSceneType() {
        return SceneManager.SceneType.SCENE_GAME;
    }

    @Override
    public void disposeScene() {
        camera.setHUD(null);
    }

    public interface CreditsUpdateHandlerListener {
        void creditsUpdateFinished();
    }

    public class CreditsUpdateHandler implements IUpdateHandler {

        private int previousValue;
        private int previousCredits;
        private int credits;
        private boolean direct;
        private boolean updating;
        private CreditsUpdateHandlerListener creditsUpdateHandlerListener;
        private EaseLinear easeLinear = EaseLinear.getInstance();

        private float currentTime;
        private float maxTime;
        private float DEFAULT_TIME = 2.5f;

        public void setCredits(float credits, float previousCredits, boolean direct) {
            setCredits(credits, previousCredits, direct, DEFAULT_TIME);
        }

        public void setCredits(float credits, float previousCredits, boolean direct, float time) {
            setCredits(credits, previousCredits, direct, time, null);
        }

        public void setCredits(float credits, float previousCredits, boolean direct, float time, CreditsUpdateHandlerListener creditsUpdateHandlerListener) {
            this.maxTime = time;
            this.direct = direct;
            this.previousCredits = (int)(previousCredits * CURRENCY_MULTIPLIER);
            this.credits = (int)(credits * CURRENCY_MULTIPLIER);
            this.updating = true;
            this.currentTime = 0f;
            this.creditsUpdateHandlerListener = creditsUpdateHandlerListener;
            calculatePosition();
            if(!creditsText.isVisible()) {
                creditsText.setVisible(true);
            }
        }

        private void calculatePosition() {

            //calculate y position
            creditsText.setY(creditsText.getHeight());

            //save old text value
            String creditsTextOld = creditsText.getText().toString().replaceAll(INITIAL_CREDITS_VALUE, "0");

            //calculate old text width
            creditsText.setText(creditsTextOld);
            float oldTextWidth = creditsText.getWidth();

            //calculate new text width
            creditsText.setText(INITIAL_CREDITS_TITLE + credits);
            float newTextWidth = creditsText.getWidth();

            //back to old text
            creditsText.setText(creditsTextOld);

            if(newTextWidth > oldTextWidth) {
                creditsText.setX(MainActivity.CAMERA_WIDTH - newTextWidth - 20);
            }
        }

        @Override
        public void onUpdate(float pSecondsElapsed) {
            if(updating) {
                if(direct) {
                    showCreditsValue(credits);
                }
                else {
                    float creditsDiff = credits - previousCredits;
                    float percentage = easeLinear.getPercentage(currentTime, maxTime);
                    int creditsCalc = (int)(creditsDiff * percentage);
                    if(currentTime >= maxTime) {
                        showCreditsValue(credits);
                        updating = false;
                        if(creditsUpdateHandlerListener != null) {
                            creditsUpdateHandlerListener.creditsUpdateFinished();
                        }
                    }
                    else {
                        showCreditsValue(previousCredits + creditsCalc);
                        currentTime += pSecondsElapsed;
                    }
                }
            }
        }

        private void showCreditsValue(int value) {
            if(value != previousValue) {
                creditsText.setText(INITIAL_CREDITS_TITLE + value);
                previousValue = value;
            }
        }

        @Override
        public void reset() {

        }
    }

    public class BonusCreditsUpdateHandler implements IUpdateHandler {

        private int previousValue;
        private int previousCredits;
        private int credits;
        private boolean direct;
        private boolean updating;
        private EaseLinear easeLinear = EaseLinear.getInstance();

        private float currentTime;
        private float maxTime;
        private float DEFAULT_TIME = 2.5f;

        public void setCredits(float credits, float previousCredits, boolean direct) {
            setCredits(credits, previousCredits, direct, DEFAULT_TIME);
        }

        public void setCredits(float credits, float previousCredits, boolean direct, float time) {
            this.maxTime = time;
            this.direct = direct;
            this.previousCredits = (int)(previousCredits * CURRENCY_MULTIPLIER);
            this.credits = (int)(credits * CURRENCY_MULTIPLIER);
            this.updating = true;
            this.currentTime = 0f;
            if(!playingForText.isVisible()) {
                playingForText.setVisible(true);
            }
        }

        @Override
        public void onUpdate(float pSecondsElapsed) {
            if(updating) {
                if(direct) {
                    showCreditsValue(credits);
                }
                else {
                    float creditsDiff = credits - previousCredits;
                    float percentage = easeLinear.getPercentage(currentTime, maxTime);
                    int creditsCalc = (int)(creditsDiff * percentage);
                    if(currentTime >= maxTime) {
                        showCreditsValue(credits);
                        updating = false;
                    }
                    else {
                        showCreditsValue(previousCredits + creditsCalc);
                        currentTime += pSecondsElapsed;
                    }
                }
            }
        }

        private void showCreditsValue(int value) {
            if(value != previousValue) {
                playingForText.setText(INITIAL_PLAYING_FOR_TITLE + value);
                previousValue = value;
            }
        }

        @Override
        public void reset() {

        }
    }

    public class MoneyPrizeUpdateHandler implements IUpdateHandler {

        private PRIZE_HANDLER_STATE state = PRIZE_HANDLER_STATE.NOT_VISIBLE;
        private EaseLinear prizeEase = EaseLinear.getInstance();
        private int prize;
        private float currentTime;
        private float maxTime;
        private String prizeTextStr;

        @Override
        public void onUpdate(float pSecondsElapsed) {

            if(state == PRIZE_HANDLER_STATE.INITIALIZING) {

                //set initial text
                moneyPrizePopupEntity.moneyPrizeText.setText(prizeTextStr);

                //set initial position
                moneyPrizePopupEntity.setY((MainActivity.CAMERA_HEIGHT - moneyPrizePopupEntity.moneyPrizeText.getHeight()) / 2f);

                //register modified
                MoveXModifier moveXModifier = new MoveXModifier(0.2f, -moneyPrizePopupEntity.moneyPrizeText.getWidth(), ((MainActivity.CAMERA_WIDTH - moneyPrizePopupEntity.moneyPrizeText.getWidth()) / 2f), new IEntityModifier.IEntityModifierListener() {
                    @Override
                    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                        moneyPrizePopupEntity.setVisible(true);
                    }
                    @Override
                    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                        //set state
                        state = PRIZE_HANDLER_STATE.SHOWING_PRIZE;
                    }
                });
                moveXModifier.setAutoUnregisterWhenFinished(true);
                moneyPrizePopupEntity.registerEntityModifier(moveXModifier);

                //reset vars
                currentTime = 0f;
                maxTime = 2f;

                //set state
                state = PRIZE_HANDLER_STATE.APPEARING;
            }
            else if(state == PRIZE_HANDLER_STATE.SHOWING_PRIZE) {
                if(currentTime >= maxTime) {
                    //register modified
                    MoveXModifier moveXModifier = new MoveXModifier(0.2f, moneyPrizePopupEntity.getX(), MainActivity.CAMERA_WIDTH, new IEntityModifier.IEntityModifierListener() {
                        @Override
                        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                        }
                        @Override
                        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                            //set state
                            state = PRIZE_HANDLER_STATE.NOT_VISIBLE;
                        }
                    });
                    moveXModifier.setAutoUnregisterWhenFinished(true);
                    moneyPrizePopupEntity.registerEntityModifier(moveXModifier);
                    //set state
                    state = PRIZE_HANDLER_STATE.DISAPPEARING;
                }
                else {
                    currentTime += pSecondsElapsed;
                    float percentage = prizeEase.getPercentage(currentTime, maxTime);
                    int prizeCalc = (prize - (int)(prize * percentage));
                    if(prizeCalc < 0) {
                        prizeCalc = 0;
                    }
                    moneyPrizePopupEntity.moneyPrizeText.setText(String.valueOf(prizeCalc));
                }
            }
        }

        public void setPrize(float prize) {
            this.prize = (int)(prize * CURRENCY_MULTIPLIER);
            this.prizeTextStr = String.valueOf(this.prize);
            state = PRIZE_HANDLER_STATE.INITIALIZING;
            moneyPrizePopupEntity.setVisible(true);
        }

        @Override
        public void reset() {

        }
    }

    public class FreeRollPrizeUpdateHandler implements IUpdateHandler {

        private PRIZE_HANDLER_STATE state = PRIZE_HANDLER_STATE.NOT_VISIBLE;
        private int freeRolls;
        private float currentTime;
        private float maxTime;

        @Override
        public void onUpdate(float pSecondsElapsed) {

            if(state == PRIZE_HANDLER_STATE.INITIALIZING) {

                //appear hud text
                appearFreeRollsText(freeRolls, false);

                //set initial text
                freeRollPrizePopupEntity.freeRollPrizeText.setText(freeRolls + " FREE ROLLS!");

                //set initial position
                freeRollPrizePopupEntity.setY((MainActivity.CAMERA_HEIGHT - freeRollPrizePopupEntity.freeRollPrizeText.getHeight()) / 2f);

                //register modified
                MoveXModifier moveXModifier = new MoveXModifier(0.2f, -freeRollPrizePopupEntity.freeRollPrizeText.getWidth(), ((MainActivity.CAMERA_WIDTH - freeRollPrizePopupEntity.freeRollPrizeText.getWidth()) / 2f), new IEntityModifier.IEntityModifierListener() {
                    @Override
                    public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                        freeRollPrizePopupEntity.setVisible(true);
                    }
                    @Override
                    public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                        //set state
                        state = PRIZE_HANDLER_STATE.SHOWING_PRIZE;
                    }
                });
                moveXModifier.setAutoUnregisterWhenFinished(true);
                freeRollPrizePopupEntity.registerEntityModifier(moveXModifier);

                //reset vars
                currentTime = 0f;
                maxTime = 2f;

                //set state
                state = PRIZE_HANDLER_STATE.APPEARING;
            }
            else if(state == PRIZE_HANDLER_STATE.SHOWING_PRIZE) {
                if(currentTime >= maxTime) {
                    //register modified
                    MoveXModifier moveXModifier = new MoveXModifier(0.2f, freeRollPrizePopupEntity.getX(), MainActivity.CAMERA_WIDTH, new IEntityModifier.IEntityModifierListener() {
                        @Override
                        public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                        }
                        @Override
                        public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                            //set state
                            state = PRIZE_HANDLER_STATE.NOT_VISIBLE;
                        }
                    });
                    moveXModifier.setAutoUnregisterWhenFinished(true);
                    freeRollPrizePopupEntity.registerEntityModifier(moveXModifier);
                    //set state
                    state = PRIZE_HANDLER_STATE.DISAPPEARING;
                }
                currentTime += pSecondsElapsed;
            }
        }

        public void setFreeRolls(int freeRolls) {
            this.freeRolls = freeRolls;
            state = PRIZE_HANDLER_STATE.INITIALIZING;
            freeRollPrizePopupEntity.setVisible(true);
        }

        @Override
        public void reset() {

        }
    }

    private class FreeRollPrizePopupEntity extends Entity {
        private Text freeRollPrizeText;
        public FreeRollPrizePopupEntity() {
            freeRollPrizeText = new Text(50, 0, resourcesManager.prizeFont, "                 ", new TextOptions(HorizontalAlign.CENTER), vbom);
            setVisible(false);
            attachChild(freeRollBackSprite);
            attachChild(freeRollPrizeText);
        }
    }

    private class MoneyPrizePopupEntity extends Entity {
        private Text moneyPrizeText;
        public MoneyPrizePopupEntity() {
            moneyPrizeText = new Text(50, 0, resourcesManager.prizeFont, "                 ", new TextOptions(HorizontalAlign.CENTER), vbom);
            setVisible(false);
            attachChild(moneyPopupBackSprite);
            attachChild(moneyPrizeText);
        }
    }

    private class LoadingEntity extends Rectangle {
        public LoadingEntity() {
            super(0, 0, MainActivity.CAMERA_WIDTH, MainActivity.CAMERA_HEIGHT, vbom);
            loadingIconSprite = new Sprite(0, 0, resourcesManager.loadingIconTextureRegion, vbom);
            loadingIconSprite.setX((MainActivity.CAMERA_WIDTH - loadingIconSprite.getWidth()) / 2f);
            loadingIconSprite.setY((MainActivity.CAMERA_HEIGHT - loadingIconSprite.getHeight()) / 2f);
            loadingIconSprite.registerEntityModifier(new LoopEntityModifier(new RotationModifier(1.5f, 0, 360)));
            attachChild(loadingIconSprite);
            setColor(Color.BLACK);
            setAlpha(0f);
            loadingIconSprite.setVisible(false);
        }

        public void lightUp(IModifier.IModifierListener modifierListener) {
            loadingIconSprite.setVisible(true);
            AlphaModifier alphaModifier = new AlphaModifier(0.25f, getAlpha(), 0.8f);
            alphaModifier.setAutoUnregisterWhenFinished(true);
            alphaModifier.addModifierListener(modifierListener);
            registerEntityModifier(alphaModifier);
        }

        public void lightDown() {
            AlphaModifier alphaModifier = new AlphaModifier(0.25f, getAlpha(), 0f);
            alphaModifier.setAutoUnregisterWhenFinished(true);
            registerEntityModifier(alphaModifier);
            loadingIconSprite.setVisible(false);
        }
    }

    public void appearFreeRollsText(int freeRolls, boolean direct) {
        freeRollsText.setY(freeRollsText.getHeight());
        freeRollsText.setText(INITIAL_FREE_ROLLS_TITLE + freeRolls);
        if(!direct) {
            MoveXModifier moveXModifier = new MoveXModifier(0.2f, freeRollsText.getX(), 20, new IEntityModifier.IEntityModifierListener() {
                @Override
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                    freeRollsText.setVisible(true);
                }
                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                }
            });
            moveXModifier.setAutoUnregisterWhenFinished(true);
            freeRollsText.registerEntityModifier(moveXModifier);
        }
        else {
            freeRollsText.setX(20);
            freeRollsText.setVisible(true);
        }
    }

    public void appearPlayingForText(float playingFor, boolean direct) {
        playingForText.setY(playingForText.getHeight());
        playingForText.setText(INITIAL_PLAYING_FOR_TITLE + ((int)(playingFor * GameScene.CURRENCY_MULTIPLIER)));
        if(!direct) {
            MoveXModifier moveXModifier = new MoveXModifier(0.2f, playingForText.getX(), 20, new IEntityModifier.IEntityModifierListener() {
                @Override
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                    playingForText.setVisible(true);
                }
                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                }
            });
            moveXModifier.setAutoUnregisterWhenFinished(true);
            playingForText.registerEntityModifier(moveXModifier);
        }
    }

    public void disappearFreeRollsText() {
        MoveXModifier moveXModifier = new MoveXModifier(0.2f, freeRollsText.getX(), -freeRollsText.getWidth(), new IEntityModifier.IEntityModifierListener() {
            @Override
            public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
            }
            @Override
            public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                freeRollsText.setVisible(false);
            }
        });
        moveXModifier.setAutoUnregisterWhenFinished(true);
        freeRollsText.registerEntityModifier(moveXModifier);
    }

    public void disappearPlayingForText(boolean direct) {
        playingForText.setY(playingForText.getHeight());
        if(!direct) {
            MoveXModifier moveXModifier = new MoveXModifier(0.2f, playingForText.getX(), -playingForText.getWidth(), new IEntityModifier.IEntityModifierListener() {
                @Override
                public void onModifierStarted(IModifier<IEntity> pModifier, IEntity pItem) {
                    playingForText.setVisible(true);
                }
                @Override
                public void onModifierFinished(IModifier<IEntity> pModifier, IEntity pItem) {
                }
            });
            moveXModifier.setAutoUnregisterWhenFinished(true);
            playingForText.registerEntityModifier(moveXModifier);
        }
        else {
            playingForText.setX(-playingForText.getWidth());
        }
    }

    public void showRollInformation() {
        if(activity.logicaMaquina.tiradaFicticia == null) {
            System.out.println("::: TIRADA ACTUAL :::");
            activity.logicaMaquina.mostrarTiradaActualConsola();
            List<JugadaLineaGanadora> jugadaLineaGanadoraList = activity.logicaMaquina.jugadasLineasGanadoras;
            for(JugadaLineaGanadora jugadaLineaGanadora : jugadaLineaGanadoraList) {

                if(jugadaLineaGanadora.getLinea() != null) {
                    int[][] formaLineaGanadora = jugadaLineaGanadora.getLinea().getForma();
                    for(int[] forma : formaLineaGanadora) {
                        for(int formaFinal : forma) {
                            System.out.print(formaFinal + "_");
                        }
                        System.out.println();
                    }
                }
                int[] figurasIds = jugadaLineaGanadora.getJugadaGanadora().figurasIds;
                System.out.print("Jugada ganadora :: ");
                for(int figuraId : figurasIds) {
                    System.out.print(figuraId + "_");
                }
                System.out.println();
            }
        }
    }

    public void gotoSlots() {
        currentGame = CURRENT_GAME.MOVING;
        cameraTransformerHandler.animateGotoFrame(this, 0.2f, slotsGame.getX(), slotsGame.getY(), slotsGame.getWidth(), slotsGame.getHeight(), null, new Runnable() {
            @Override
            public void run() {
                minorMajorMinigame.exitGame();
                slotsGame.enterGame();
            }
        });
    }

    public void gotoMinigameMinorMajor() {
        currentGame = CURRENT_GAME.MOVING;
        cameraTransformerHandler.animateGotoFrame(this, 0.4f, minorMajorMinigame.getX(), minorMajorMinigame.getY(), minorMajorMinigame.getWidth(), minorMajorMinigame.getHeight(), new Runnable() {
            @Override
            public void run() {
                minorMajorMinigame.clearPreviousHand(true);
            }
        }, new Runnable() {
            @Override
            public void run() {
                slotsGame.exitGame();
                minorMajorMinigame.enterGame();
            }
        });
    }

    public void showLoadingLayer(IModifier.IModifierListener modifierListener) {
        loadingEntity.lightUp(modifierListener);
    }

    public void hideLoadingLayer() {
        loadingEntity.lightDown();
    }

    @Override
    public void cameraAnimationStarted(Runnable runnable) {
        if(runnable != null) {
            new Thread(runnable).start();
        }
    }

    @Override
    public void cameraAnimationFinished(Runnable runnable) {
        if(runnable != null) {
            new Thread(runnable).start();
        }
    }

    private void configureUpdateHandlers() {
        creditsUpdateHandler = new CreditsUpdateHandler();
        moneyPrizeUpdateHandler = new MoneyPrizeUpdateHandler();
        freeRollPrizeUpdateHandler = new FreeRollPrizeUpdateHandler();
        bonusCreditsUpdateHandler = new BonusCreditsUpdateHandler();
        cameraTransformerHandler = new CameraTransformerHandler(camera);

        registerUpdateHandler(creditsUpdateHandler);
        registerUpdateHandler(moneyPrizeUpdateHandler);
        registerUpdateHandler(freeRollPrizeUpdateHandler);
        registerUpdateHandler(bonusCreditsUpdateHandler);
        registerUpdateHandler(cameraTransformerHandler);

    }

    private void configureTouch() {
        //basic touch configuration
        setOnAreaTouchTraversalFrontToBack();
        setTouchAreaBindingOnActionDownEnabled(true);
        setTouchAreaBindingOnActionMoveEnabled(true);
        //register touch areas
        registerTouchArea(slotsGame);
        registerTouchArea(minorMajorMinigame);
    }

    private void configureHUD() {

        //attach it to the camera
        camera.setHUD(hud);

        //credits text
        creditsText.setVisible(false);
        hud.attachChild(creditsText);

        //free rolls text
        freeRollsText.setVisible(false);
        hud.attachChild(freeRollsText);

        //playing for text
        playingForText.setVisible(false);
        hud.attachChild(playingForText);

        //money prize entity
        hud.attachChild(moneyPrizePopupEntity);

        //free roll prize entity
        hud.attachChild(freeRollPrizePopupEntity);

        //loading entity
        hud.attachChild(loadingEntity);

    }
}
