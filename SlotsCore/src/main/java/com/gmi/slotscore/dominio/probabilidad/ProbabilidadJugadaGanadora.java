package com.gmi.slotscore.dominio.probabilidad;

import com.gmi.slotscore.dominio.JugadaGanadora;

/**
 * Contiene informacion probabilistica relacionada
 * a una jugada ganadora especificamente
 * 
 * @author PabloMobile
 *
 */
public class ProbabilidadJugadaGanadora {

	/**
	 * La jugada ganadora sobre la cual
	 * las probabilidades se estan aplicando
	 */
	public JugadaGanadora jugadaGanadora;

	/**
	 * La cantidad de repeticiones se dan
	 * en la configuracion actual de la 
	 * maquina para esta jugada ganadora
	 */
	public long repeticiones;
	
	/**
	 * La cantidad de multiplicaciones que
	 * que paga la jugada ganadora entre
	 * todas sus repeticiones
	 */
	public long multiplicaciones;
	
	/**
	 * Porcentaje entre las jugadas ganadoras 
	 * que representa esta en particular. El
	 * valor usado para realizar el calculo
	 * es la variable 'multiplicaciones'
	 */
	public float porcentajePagoEntreGanadoras;
	
	/**
	 * Idem. pero solo cuenta las jugadas 
	 * de tipo NORMAL
	 */
	public float porcentajePagoEntreGanadorasNormales;
	
}