package com.gmi.slotscore.dominio;

import com.gmi.slotscore.constante.Sistema;
import com.gmi.slotscore.dominio.estadistica.EstadisticaMaquina;
import com.gmi.slotscore.dominio.probabilidad.ProbabilidadJugadaGanadora;
import com.gmi.slotscore.dominio.probabilidad.ProbabilidadMaquina;
import com.gmi.slotscore.logica.LogicaMaquina;
import com.gmi.slotscore.logica.configuracion.Configuracion;
import com.gmi.slotscore.persistencia.dao.ConfiguracionDAO;
import com.gmi.slotscore.util.MathUtil;
import com.gmi.slotscore.util.ReflectionUtil;
import com.gmi.slotscore.util.SlotsRandomGenerator;

import java.util.ArrayList;
import java.util.List;

/**
 * Represenacion de una maquina tragamonedas.
 * Esta maquina tiene rodillos que contienen figuras.
 * Tambien la maquina contiene una coleccion de jugadas 
 * ganadoras.
 * La maquina tambien posee una coleccion de posibles 
 * configuraciones de lineas.
 * 
 * Todas estas variables son configuradas en las subclases de 
 * la clase abstracta 'Configuracion', en donde se guarda la configuracion
 * basica de la maquina.
 * 
 * 
 * @author Pablo Caviglia
 *
 */
public class Maquina {

	public List<Rodillo> rodillos = new ArrayList<Rodillo>();
	public List<JugadaGanadora> jugadasGanadoras = new ArrayList<JugadaGanadora>();
	public List<Linea> lineas = new ArrayList<Linea>();
	public Configuracion configuracion;
	public LogicaMaquina logicaMaquina;
	
	/**
	 * Contiene informacion estadistica de la
	 * maquina, calculada en tiempo de ejecucion
	 * tras cada apuesta ejecutada en la misma
	 */
	public EstadisticaMaquina estadisticaMaquina;
	
	/**
	 * Contiene datos calculados algoritmicamente
	 * relacionados a las probabilidades de la
	 * maquina, a nivel general, como asi 
	 * tambien a nivel de jugadas
	 */
	public ProbabilidadMaquina probabilidad;

	/**
	 * El dinero asociado con el jugador
	 * actual. Esta variable define el estado
	 * del juego. Si es mayor que cero el
	 * jugador esta jugando, si es cero no lo
	 * esta. Si es negativo es que el hijo de puta
	 * nos quedo debiendo plata (BUG!!!).
	 */
	public float montoJugador;

	/**
	 * Cantidad de lineas apostadas durante
	 * la ejecucion del metodo apostar. Este
	 * valor multiplicado por el valor de apuesta
	 * por linea resulta en la obtencion del
	 * monto total de la apuesta
	*/
	public short lineasApostadas = 1; //valor por defecto
	
	/**
	 * El monto apostado durante la ejecucion
	 * del metodo apostar. Este valor debe de
	 * ser multiplicado por la cantidad de lineas
	 * apostadas para obtener el valor del monto
	 * total apostado
	*/ 
	public float dineroApostadoPorLinea;

	public Maquina(Class configuracionClass) {

		//inicializo el generador aleatorio
		SlotsRandomGenerator.getInstance();
		
		//inicializo la configuracion
		configuracion = (Configuracion)ReflectionUtil.createObject(configuracionClass.getName());
		configuracion.inicializarConfiguracion(this);
		
		//inicializamos objetos
		probabilidad = new ProbabilidadMaquina();
		probabilidad.probabilidadJugadasGanadoras = new ArrayList<ProbabilidadJugadaGanadora>();

		//seteo los campos necesarios al slot
		this.rodillos = configuracion.getRodillos();
		this.jugadasGanadoras = configuracion.getJugadasGanadoras();
		this.lineas = configuracion.getLineas();

		//inicializamos logica maquina
		logicaMaquina = new LogicaMaquina(this);
		
		//Carga desde la persistencia los
		//valores de configuracion de la
		//maquina
		logicaMaquina.cargarValoresConfiguracion();
		
		//inicializamos la estadistica
		estadisticaMaquina = new EstadisticaMaquina(this);

		//se inicializa el calculo de probabilidades
        if(Sistema.CALCULAR_ESTADISTICA_MAQUINA) {
            configuracion.inicializarCalculoProbabilidades();
        }

		//inicializo los valores iniciales
		//de algunas variables importantes
		lineasApostadas = 1;
		dineroApostadoPorLinea = Sistema.INCREMENTO_APUESTA;

    }
	
	/**
	 * Modifica el monto actual del jugador, 
	 * dejando registro del ultimo monto
	 * en la persistencia. El valor de 
	 * persistencia es usado ante cualquier
	 * inconveniente que la maquina se 
	 * cuelgue por alguna razon o se resetee
	 */
	public synchronized void incrementarMontoJugador(float incremento) {
		
		//puede recibir un numero negativo
		montoJugador = montoJugador + incremento;

		//ajustamos el redondeo incorrecto
		//causado por manejar numeros de
		//numero flotante
		montoJugador = (float)MathUtil.roundTwoDecimals(montoJugador);

        //persist
        logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.MONTO_JUGADOR, String.valueOf(montoJugador));
	}
	
	public synchronized void actualizarMontoJugador(float nuevoMonto) {
		//el monto del jugador
		//ahora es el monto 
		//asignado
		montoJugador = nuevoMonto;
		
		//actualizamos el valor en 
		//la persistencia
		logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.MONTO_JUGADOR, ""+nuevoMonto);

	}
	
	public void setDineroApostadoPorLinea(float nuevoDineroApostadoPorLinea) {
		
		//fix para acomodar redondeos
		if(nuevoDineroApostadoPorLinea < Sistema.INCREMENTO_APUESTA) {
			nuevoDineroApostadoPorLinea = Sistema.INCREMENTO_APUESTA;
		}
		
		this.dineroApostadoPorLinea = nuevoDineroApostadoPorLinea;
	}
}