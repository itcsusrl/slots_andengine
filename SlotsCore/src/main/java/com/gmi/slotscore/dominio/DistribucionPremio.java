package com.gmi.slotscore.dominio;

public class DistribucionPremio {

	public int id;
	public String formaJugada;
	public float acumulado;
	
	public DistribucionPremio(int id, String formaJugada, float acumulado) {
		this.id = id;
		this.formaJugada = formaJugada;
		this.acumulado = acumulado;
	}
}