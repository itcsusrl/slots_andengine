package com.gmi.slotscore.dominio.estadistica;

import com.gmi.slotscore.dominio.Maquina;
import com.gmi.slotscore.persistencia.dao.EstadisticaDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class EstadisticaMaquina {
	
	private Maquina maquina;

	public float dineroApostado;
	public float dineroGanado;
	
	public float dineroGanadoNormal;
	public float dineroGanadoScatter;
	public float dineroGanadoBonus;
	public float dineroGanadoJackpot;
	
	public long cantidadApuestas;
	public long cantidadApuestasGanadas;
	public long cantidadApuestasBonus;
	
	public List<GanadorTipoJugada> ganadoresTipoJugada = new ArrayList<GanadorTipoJugada>();
	
	public EstadisticaMaquina(Maquina maquina) {
		this.maquina = maquina;
        //obtenemos los valores desde
        //desde la persistencia
        Map<String, Float> valores = maquina.logicaMaquina.obtenerValoresEstadistica();
        //los seteamos en memoria
        dineroApostado = valores.get(EstadisticaDAO.DINERO_APOSTADO);
        dineroGanado = valores.get(EstadisticaDAO.DINERO_GANADO);
        dineroGanadoNormal = valores.get(EstadisticaDAO.DINERO_GANADO_NORMAL);
        dineroGanadoScatter = valores.get(EstadisticaDAO.DINERO_GANADO_SCATTER);
        dineroGanadoBonus = valores.get(EstadisticaDAO.DINERO_GANADO_BONUS);
        dineroGanadoJackpot = valores.get(EstadisticaDAO.DINERO_GANADO_JACKPOT);
        cantidadApuestas = valores.get(EstadisticaDAO.CANTIDAD_APUESTAS).longValue();
        cantidadApuestasGanadas = valores.get(EstadisticaDAO.CANTIDAD_APUESTAS_GANADAS).longValue();
        cantidadApuestasBonus = valores.get(EstadisticaDAO.CANTIDAD_APUESTAS_GANADAS_BONUS).longValue();
	}

	public void actualizarValoresEstadisticos() {
		maquina.logicaMaquina.actualizarValoresEstadisticos(dineroApostado, dineroGanado, dineroGanadoNormal, dineroGanadoScatter, dineroGanadoBonus, dineroGanadoJackpot, cantidadApuestas, cantidadApuestasGanadas, cantidadApuestasBonus);
	}
	
	public void registrarGanadorTipoJugada(String tipoJugada, int formatoJugada, int multiplicador, int tiradasGratuitas) {
		GanadorTipoJugada ganadorTipoJugada = obtenerGanadorTipoJugada(tipoJugada);
		if(ganadorTipoJugada == null) {
			ganadorTipoJugada = new GanadorTipoJugada();
			ganadorTipoJugada.ganadas = 1;
			ganadorTipoJugada.tipoJugada = tipoJugada;
			ganadorTipoJugada.formatoJugada = formatoJugada;
			ganadorTipoJugada.multiplicador = multiplicador;
			ganadorTipoJugada.tiradasGratuitas = tiradasGratuitas;
			//guardo la estadistica en la coleccion
			ganadoresTipoJugada.add(ganadorTipoJugada);
		}
		else {
			ganadorTipoJugada.ganadas++;
			ganadorTipoJugada.tiradasGratuitas += tiradasGratuitas;
		}
	}
	
	public GanadorTipoJugada obtenerGanadorTipoJugada(String tipoJugada) {
		for(GanadorTipoJugada gtj : ganadoresTipoJugada) {
			if(gtj.tipoJugada.equals(tipoJugada)) {
				return gtj;
			}
		}
		return null;
	}
}