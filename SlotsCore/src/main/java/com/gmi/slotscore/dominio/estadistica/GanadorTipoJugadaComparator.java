package com.gmi.slotscore.dominio.estadistica;

import java.util.Comparator;

public class GanadorTipoJugadaComparator implements Comparator<GanadorTipoJugada> {
	public int compare(GanadorTipoJugada arg0, GanadorTipoJugada arg1) {
		int mult1 = arg0.ganadas * arg0.multiplicador;
		int mult2 = arg1.ganadas * arg1.multiplicador;
		return mult2-mult1;
	}
}