package com.gmi.slotscore.dominio;

import java.util.Comparator;

public class MultiplicadorComparator implements Comparator<JugadaGanadora> {

	public int compare(JugadaGanadora arg0, JugadaGanadora arg1) {
		
		int mult1 = arg0.multiplicador;
		int mult2 = arg1.multiplicador;
		
		//si es bonus, debemos cambiar
		//la comparacion del multiplicador
		//a las tiradas gratuitas
		if(arg0 instanceof JugadaGanadoraBonus && arg1 instanceof JugadaGanadoraBonus) {
			JugadaGanadoraBonus jugadaGanadoraBonus1 = (JugadaGanadoraBonus)arg0;
			JugadaGanadoraBonus jugadaGanadoraBonus2 = (JugadaGanadoraBonus)arg1;
			mult1 = jugadaGanadoraBonus1.getTiradasGratis();
			mult2 = jugadaGanadoraBonus2.getTiradasGratis();
		}
		
		return mult2-mult1;
	}
}