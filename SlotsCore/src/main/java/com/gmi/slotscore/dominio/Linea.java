package com.gmi.slotscore.dominio;

import org.andengine.opengl.texture.region.ITextureRegion;

public class Linea {

	private int id;
	private int[][] forma;
    private ITextureRegion lineTextureRegion;
    private ITextureRegion circleTextureRegion;

	public Linea(int id, int[][] forma) {
		this.id = id;
		this.forma = forma;
	}
	
	public int[][] getForma() {
		return forma;
	}

	public void setForma(int[][] forma) {
		this.forma = forma;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public ITextureRegion getLineTextureRegion() {
        return lineTextureRegion;
    }

    public void setLineTextureRegion(ITextureRegion lineTextureRegion) {
        this.lineTextureRegion = lineTextureRegion;
    }

    public ITextureRegion getCircleTextureRegion() {
        return circleTextureRegion;
    }

    public void setCircleTextureRegion(ITextureRegion circleTextureRegion) {
        this.circleTextureRegion = circleTextureRegion;
    }
}