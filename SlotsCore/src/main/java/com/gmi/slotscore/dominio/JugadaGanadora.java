package com.gmi.slotscore.dominio;

import java.util.ArrayList;
import java.util.List;

import com.gmi.slotscore.constante.Sistema;
import com.gmi.slotscore.excepcion.FiguraInexistenteException;
import com.gmi.slotscore.excepcion.JugadaGanadoraInvalidaException;

/**
 * Representa una jugada ganadora. 
 * Indica cual es el factor multiplicador de
 * este tipo de jugada ganadora, y tambien el
 * tipo de jugada (scatter, normal, bonus)
 * 
 * @author Pablo Caviglia
 *
 */
public class JugadaGanadora {

	public Maquina maquina;
	public List<Figura> figuras = new ArrayList<Figura>();
	public int[] figurasIds;
	public int multiplicador;
	public int tipo;
	
	/**
	 * Variable que se inicializa solo 
	 * una vez por jugadaGanadora para
	 * identificar a cada jugada 
	 * ganadora unicamente
	 */
	private String formateado;
	
	public JugadaGanadora(int multiplicador, int tipo) {
		this.multiplicador = multiplicador;
		this.tipo = tipo;
	}
	
	public JugadaGanadora(List<Figura> figuras, int multiplicador, int tipo) {
		this.figuras = figuras;
		this.multiplicador = multiplicador;
		this.tipo = tipo;
	}
	
	public static JugadaGanadora configurarJugadaGanadora(Maquina maquina, String jugadaFormateada, List<Figura> figurasJugada, int tipo, int multiplicador) {
		
		JugadaGanadora jugadaGanadora = new JugadaGanadora(multiplicador, tipo);
		jugadaGanadora.maquina = maquina;
		String figuraAnterior = null;
		
		String[] jugadaFormateadaArray = jugadaFormateada.split(",");
		for(String idFiguraActualStr : jugadaFormateadaArray) {

			boolean existeFigura = false;
			int idJugadaFiguraActual = Integer.parseInt(idFiguraActualStr.trim());
			
			for(Figura figuraActual : figurasJugada) {
				if(idJugadaFiguraActual == figuraActual.getId()) {
					jugadaGanadora.figuras.add(figuraActual);
					existeFigura = true;
					break;
				}
			}
			
			if(!existeFigura) {
				throw new FiguraInexistenteException(idJugadaFiguraActual);
			}
			else {
				
				if(figuraAnterior == null) {
					figuraAnterior = idFiguraActualStr;
				}
				else {
					if(!figuraAnterior.equals(idFiguraActualStr)) {
						throw new JugadaGanadoraInvalidaException(jugadaFormateada);
					}
				}
			}
		}

		//creamos el array de ids de figura
		//para la jugada ganadora
		int[] figurasIds = new int[jugadaGanadora.figuras.size()];
		List<Figura> figurasJugadaGanadora = jugadaGanadora.figuras;
		for(int i=0; i<figurasJugadaGanadora.size(); i++) {
			figurasIds[i] = figurasJugadaGanadora.get(i).getId();
		}
		jugadaGanadora.figurasIds = figurasIds;
		
		return jugadaGanadora;
	}
	
	public String obtenerFormateado() {
		
		//lo inicializamos una vez
		//para no crear tantos 
		//strings innecesariamente
		if(formateado == null) {

			//importante inicializar el string
			formateado = "";
			
			for(int i=0; i<figuras.size(); i++) {
			
				//obtengo la figura actual
				Figura figuraActual = (Figura)figuras.get(i);
				formateado += "" + figuraActual.getId();
				
				if(i < (figuras.size()-1)) {
					formateado += ",";
				}
			}	
		}
		
		return formateado;
	}
	
	public String toString() {
		return obtenerFormateado() + " --> PAGA x " + multiplicador;
	}

	/**
	 * Compara la normal jugada recibida por parametro
	 * con la jugada ganadora y devuelve verdadero
	 * si esta compuesta de la misma manera
	 * @param jugada
	 * @return
	 */
	public boolean esJugadaNormalGanadora(int[] jugada, Integer idFiguraWild) {
		
		//contadores de tipos de figura
		int cantidadFigurasNormal = 0;
		int cantidadFigurasWild = 0;
		
		for(int i=0; i<figurasIds.length; i++) {
			if(idFiguraWild != null) {
				if(figurasIds[i] != jugada[i] && jugada[i] != idFiguraWild) {
					return false;
				}
				else {
					if(jugada[i] == idFiguraWild) {
						cantidadFigurasWild++;
					}
					else {
						cantidadFigurasNormal++;
					}
				}
			}
			else {
				if(figurasIds[i] != jugada[i]) {
					return false;
				}
				else {
					cantidadFigurasNormal++;
				}
			}
		}
		
		/**
		 * Si la cantidad de figuras wild
		 * en la jugada es igual a la 
		 * cantidad de rodillos, entonces
		 * consideramos que hay un premio
		 * de tipo wild
		 */
		if(Sistema.APUESTA_WILD == 1 && cantidadFigurasWild == maquina.rodillos.size()) {
			return true;
		}
		
		//si hay mas figuras wild que
		//normales, entonces la jugada
		//no se valida
		if(cantidadFigurasWild >= cantidadFigurasNormal) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Compara la jugada scatter recibida por parametro
	 * con la jugada ganadora y devuelve verdadero
	 * si esta compuesta de la misma manera
	 * @param jugada
	 * @return
	 */
	public boolean esJugadaScatterGanadora(int[][] jugada, int scatterId) {
		
		//obtengo la cantidad de figuras 
		//scatter de esta jugada ganadora
		int cantidadScatter = figuras.size();
		
		//guardamos la cantidad de figuras
		//scatter encontradas en la jugada
		//recibida por parametro
		int cantidadScatterRecibido = 0;
		
		for(int k=0; k<jugada[0].length; k++) {
			for(int i=0; i<jugada.length; i++) {
				
				//obtenemos el id de figura actual
				int figuraIdActual = jugada[i][k];
				
				//si la figura actual es scatter
				if(figuraIdActual == scatterId) {
					cantidadScatterRecibido++;
				}
			}
		}
		
		if(cantidadScatterRecibido >= cantidadScatter) {
			for(int k=0; k<jugada[0].length; k++) {
				for(int i=0; i<jugada.length; i++) {
					
					//obtenemos el id de figura actual
					int figuraIdActual = jugada[i][k];
					
					//System.out.print(figuraIdActual + "\t");
					
					//si la figura actual es scatter
					if(figuraIdActual == scatterId) {
						cantidadScatterRecibido++;
					}
				}
			}

			return true;
		}
		else {
			return false;
		}
	}
}