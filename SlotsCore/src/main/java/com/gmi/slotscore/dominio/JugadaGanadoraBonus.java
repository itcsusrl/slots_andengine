package com.gmi.slotscore.dominio;

import java.util.List;

import com.gmi.slotscore.constante.JugadaTipo;
import com.gmi.slotscore.excepcion.FiguraInexistenteException;

public class JugadaGanadoraBonus extends JugadaGanadora {

	private int tiradasGratis;
	
	public JugadaGanadoraBonus(int tiradasGratis) {
		super(0, JugadaTipo.BONUS);
		this.tiradasGratis = tiradasGratis;
	}
	
	public JugadaGanadoraBonus(List<Figura> figuras, int tiradasGratis) {
		super(figuras, 0, JugadaTipo.BONUS);
		this.tiradasGratis = tiradasGratis;
	}
	
	/**
	 * La jugada formateada son los id's de 
	 * las figuras separado por coma
	 * 
	 * La probabilidad del jugador es un 
	 * valor medido con una escala del 1 al 100
	 * 
	 * @param jugadaFormateada
	 * @param figurasJugada
	 * @return
	 */
	public static JugadaGanadoraBonus configurarJugadaGanadoraBonus(Maquina maquina, String jugadaFormateada, List<Figura> figurasJugada, int tiradasGratis) {
		
		JugadaGanadoraBonus jugadaGanadoraBonus = new JugadaGanadoraBonus(tiradasGratis);
		jugadaGanadoraBonus.maquina = maquina;

		String[] jugadaFormateadaArray = jugadaFormateada.split(",");
		for(String idFiguraActualStr : jugadaFormateadaArray) {

			boolean existeFigura = false;
			int idJugadaFiguraActual = Integer.parseInt(idFiguraActualStr.trim());
			for(Figura figuraActual : figurasJugada) {
				if(idJugadaFiguraActual == figuraActual.getId()) {
					jugadaGanadoraBonus.figuras.add(figuraActual);
					existeFigura = true;
					break;
				}
			}

			if(!existeFigura) {
				throw new FiguraInexistenteException(idJugadaFiguraActual);
			}
		}
		
		//creamos el array de ids de figura
		//para la jugada ganadora
		int[] figurasIds = new int[jugadaGanadoraBonus.figuras.size()];
		List<Figura> figurasJugadaGanadora = jugadaGanadoraBonus.figuras;
		for(int i=0; i<figurasJugadaGanadora.size(); i++) {
			figurasIds[i] = figurasJugadaGanadora.get(i).getId();
		}
		jugadaGanadoraBonus.figurasIds = figurasIds;
		
		return jugadaGanadoraBonus;
	}

	public int getTiradasGratis() {
		return tiradasGratis;
	}

	public void setTiradasGratis(int tiradasGratis) {
		this.tiradasGratis = tiradasGratis;
	}
}