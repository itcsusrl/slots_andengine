package com.gmi.slotscore.dominio;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa una jugada valida, que puede ser 
 * ganadora o no.
 * La jugada se representa para n-figuras, las cuales
 * tienen una relacion de 1-1 con cada rodillo. 
 * 
 * Ej.: Si existen 5 rodillos, la jugada va a formarse 
 * 	    por 5 figuras, una de cada carril.
 *  
 * La configuracion de la jugada esta dada por la linea
 * asociada que indica la forma de la jugada.
 *  
 * @author Pablo Caviglia
 *
 */
public class JugadaLinea {

    private int[][] posicionFiguras;
	private List<Figura> figuras = new ArrayList<Figura>();
	private Linea linea;
	private int tipo;

    public int[][] getPosicionFiguras() {
        return posicionFiguras;
    }

    public void setPosicionFiguras(int[][] posicionFiguras) {
        this.posicionFiguras = posicionFiguras;
    }
	public List<Figura> getFiguras() {
		return figuras;
	}
	public void setFiguras(List<Figura> figuras) {
		this.figuras = figuras;
	}
	public Linea getLinea() {
		return linea;
	}
	public void setLinea(Linea linea) {
		this.linea = linea;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
}