package com.gmi.slotscore.dominio;


public class Carta {

	private int id;

	public Carta(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}