package com.gmi.slotscore.dominio;

import org.andengine.opengl.texture.region.ITextureRegion;

/**
 * Cada tipo de figura debe de ser unica en cuanto 
 * a su identificador.
 * Las figuras son colocadas dentro de los rodillos de las
 * maquinas. Estas figuras pueden repetirse dentro de un mismo
 * rodillo, como tambien entre diferentes rodillos. 
 * 
 * @author Pablo Caviglia
 *
 */
public class Figura {

	private int id;
	private int tipo;
	private String descripcion;
	private Rodillo rodillo;
    private ITextureRegion textureRegion;
	
	public Figura(int id, String descripcion, int tipo) {

		this.id = id;
		this.descripcion = descripcion;
		this.tipo = tipo;
	}

	public Figura(int id, String descripcion) {

		this.id = id;
		this.descripcion = descripcion;
	}
	
	public Figura(int id, Rodillo rodillo) {

		this.id = id;
		this.rodillo = rodillo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Rodillo getRodillo() {
		return rodillo;
	}

	public void setRodillo(Rodillo rodillo) {
		this.rodillo = rodillo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	
	public String toString() {
		return String.valueOf(id);
	}

    public ITextureRegion getTextureRegion() {
        return textureRegion;
    }

    public void setTextureRegion(ITextureRegion textureRegion) {
        this.textureRegion = textureRegion;
    }
}