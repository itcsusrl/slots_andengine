package com.gmi.slotscore.dominio;

import java.util.ArrayList;
import java.util.List;

import com.gmi.slotscore.excepcion.FiguraInexistenteException;

/**
 * El rodillo contiene un conjunto de figuras que lo componen.
 * La maquina tragamonedas tiene un conjunto de rodillos.
 * 
 * @author Pablo Caviglia
 *
 */
public class Rodillo {

	private int id;
	private Maquina maquina;
	private List<Figura> figuras = new ArrayList<Figura>();

	public Rodillo(int id) {
		
		this.id = id;
	}

	public Rodillo(int id, Maquina maquina) {
		
		this.id = id;
		this.maquina = maquina;
	}
	
	public static Rodillo configurarRodillo(int id, String rodilloFormateado, List<Figura> figuras) {
		
		Rodillo rodillo = new Rodillo(id);
		
		String[] rodilloFormateadoArray = rodilloFormateado.split(",");
		for(String idRodilloActualStr : rodilloFormateadoArray) {
			
			boolean existeFigura = false;
			int idRodilloFiguraActual = Integer.parseInt(idRodilloActualStr.trim());
			
			for(Figura figuraActual : figuras) {
				if(idRodilloFiguraActual == figuraActual.getId()) {
					rodillo.getFiguras().add(figuraActual);
					existeFigura = true;
					break;
				}
			}
			
			if(!existeFigura) {
				throw new FiguraInexistenteException(idRodilloFiguraActual);
			}
		}
		
		return rodillo;
	}

	
	public List<Figura> getFiguras() {
		return figuras;
	}

	public void setFiguras(List<Figura> figuras) {
		this.figuras = figuras;
	}

	public Maquina getMaquina() {
		return maquina;
	}

	public void setMaquina(Maquina maquina) {
		this.maquina = maquina;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}