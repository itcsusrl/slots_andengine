package com.gmi.slotscore.dominio;

import java.util.List;

import com.gmi.slotscore.constante.JugadaTipo;

public class JugadaGanadoraJackpot extends JugadaGanadora {

	/**
	 * Cuando se crea la configuracion
	 * de la maquina se setea el valor
	 * de este campo que representa 
	 * cuantas figuras JACKPOT deben de
	 * salir para ganar el premio
	 */
	private static int CANTIDAD_FIGURAS_PREMIO;
	
	/**
	 * Monto asociado al JACKPOT
	 */
	public float monto;
	
	public JugadaGanadoraJackpot() {
		super(0, JugadaTipo.JACKPOT);
	}
	
	/**
	 * La jugada formateada son los id's de 
	 * las figuras separado por coma
	 * 
	 * La probabilidad del jugador es un 
	 * valor medido con una escala del 1 al 100
	 * 
	 * @return
	 */
	public static JugadaGanadoraJackpot configurarJugadaGanadoraJackpot(Maquina maquina) {

		JugadaGanadoraJackpot jugadaGanadoraJackpot = null;
		
		//obtenemos la figura JACKPOT
		Figura figuraJackpot = maquina.configuracion.obtenerFiguraJackpot();

		if(figuraJackpot != null) {
			
			//configuramos la cantidad de figuras
			//necesarias como la cantidad de rodillos
			//configurados en la maquina
			CANTIDAD_FIGURAS_PREMIO = maquina.configuracion.getRodillos().size();
			
			//instanciamos la jugada ganadora
			jugadaGanadoraJackpot = new JugadaGanadoraJackpot();
			jugadaGanadoraJackpot.maquina = maquina;
			
			//la cantidad de figuras del premio
			//es equivalente a la cantidad de 
			//rodillos
			for(int i=0; i<CANTIDAD_FIGURAS_PREMIO; i++) {
				jugadaGanadoraJackpot.figuras.add(figuraJackpot);
			}
			
			//creamos el array de ids de figura
			//para la jugada ganadora
			int[] figurasIds = new int[5];
			List<Figura> figurasJugadaGanadora = jugadaGanadoraJackpot.figuras;
			for(int i=0; i<figurasJugadaGanadora.size(); i++) {
				figurasIds[i] = figurasJugadaGanadora.get(i).getId();
			}
			jugadaGanadoraJackpot.figurasIds = figurasIds;
		}
		
		return jugadaGanadoraJackpot;
	}
	
	public String toString() {
		return obtenerFormateado() + " --> JACKPOT";
	}
}