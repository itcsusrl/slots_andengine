package com.gmi.slotscore.dominio;

import java.util.List;

import com.gmi.slotscore.constante.JugadaTipo;

public class JugadaGanadoraBonusGame extends JugadaGanadora {

	/**
	 * Cuando se crea la configuracion
	 * de la maquina se setea el valor
	 * de este campo que representa 
	 * cuantas figuras BONUS deben de
	 * salir para ganar el premio
	 */
	private static int CANTIDAD_FIGURAS_PREMIO = 3;
	
	/**
	 * Monto asociado al bonus 'menor mayor'
	 */
	public float monto;
	
	public JugadaGanadoraBonusGame() {
		super(0, JugadaTipo.BONUS_GAME);
	}
	
	/**
	 * La jugada formateada son los id's de 
	 * las figuras separado por coma
	 * 
	 * @return
	 */
	public static JugadaGanadoraBonusGame configurarJugadaGanadoraBonusGame(Maquina maquina) {

		JugadaGanadoraBonusGame jugadaGanadoraBonusGame = null;
		
		//obtenemos la figura 'menor mayor'
		Figura figuraBonusGame = maquina.configuracion.obtenerFiguraBonusGame();

		if(figuraBonusGame != null) {
			
			//instanciamos la jugada ganadora
			jugadaGanadoraBonusGame = new JugadaGanadoraBonusGame();
			jugadaGanadoraBonusGame.maquina = maquina;
			
			//la cantidad de figuras del premio
			//es equivalente a la cantidad de 
			//rodillos
			for(int i=0; i<CANTIDAD_FIGURAS_PREMIO; i++) {
				jugadaGanadoraBonusGame.figuras.add(figuraBonusGame);
			}
			
			//creamos el array de ids de figura
			//para la jugada ganadora
			int[] figurasIds = new int[CANTIDAD_FIGURAS_PREMIO];
			List<Figura> figurasJugadaGanadora = jugadaGanadoraBonusGame.figuras;
			for(int i=0; i<figurasJugadaGanadora.size(); i++) {
				figurasIds[i] = figurasJugadaGanadora.get(i).getId();
			}
			jugadaGanadoraBonusGame.figurasIds = figurasIds;
		}
		
		return jugadaGanadoraBonusGame;
	}
	
	public String toString() {
		return obtenerFormateado() + " --> BONUS MENOR o MAYOR";
	}
}