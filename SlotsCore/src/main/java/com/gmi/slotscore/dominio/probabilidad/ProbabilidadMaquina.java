package com.gmi.slotscore.dominio.probabilidad;

import java.util.List;

import com.gmi.slotscore.dominio.JugadaGanadora;

public class ProbabilidadMaquina {

	/**
	 * La probabilidad de la maquina y del
	 * jugador para ganar cada parte. Se
	 * definen las dos variables para 
	 * realizar el calculo una sola vez
	 */
	public float probabilidadJugador;
	public float probabilidadMaquina;
	
	/**
	 * Representa la cantidad de combinaciones
	 * diferentes que permite la configuracion
	 * actual
	 */
	public long combinaciones;

	/**
	 * El maximo apostable es un monto 
	 * imaginario en el que cada combinacion
	 * posible de la maquina representa una
	 * unidad apostada en la misma
	 */
	public long maximoApostable;
	
	/**
	 * Este valor se encuentra muy vinculado
	 * a la variable 'maximoPagable', y se
	 * puede ver como el monto maximo que paga
	 * la maquina. Este monto imaginario es 
	 * calculado sumandosele el multiplicador
	 * de cada jugada ganadora por cada repeticion
	 * que tiene la misma en la configuracion de
	 * la maquina. Por ej., si la jugada ganadora
	 * 1,1,1,1,1 tiene 3 repeticiones, y la jugada
	 * a su vez paga x 1000, significa que esta
	 * variable se va a ver incrementada en 3000
	 * unidades. Cachai huevon?
	 */
	public long maximoPagable;
	
	/**
	 * Lista contenedora de probabilidades
	 * de jugadas ganadoras
	 */
	public List<ProbabilidadJugadaGanadora> probabilidadJugadasGanadoras;
	
	/**
	 * Registra la aparicion de una jugada ganadora.
	 * Este metodo se encarga automaticamente de 
	 * chequear si la jugada ya fue registrada para
	 * no crearla nuevamente, y a la vez modificar
	 * los valores probabilisticos de la misma
	 *
	 * @param jugadaGanadora
	 */
	public void registrarJugadaGanadora(JugadaGanadora jugadaGanadora) {
	
		//obtenemos el objeto
		ProbabilidadJugadaGanadora probabilidadJugadaGanadora = obtenerProbabilidadJugadaGanadora(jugadaGanadora);
		
		//si es nula lo instanciamos
		//e inicializamos
		if(probabilidadJugadaGanadora == null) {
			probabilidadJugadaGanadora = new ProbabilidadJugadaGanadora();
			probabilidadJugadaGanadora.jugadaGanadora = jugadaGanadora;
			probabilidadJugadasGanadoras.add(probabilidadJugadaGanadora);
		}
		
		//incrementamos los contadores
		//de la probabilidad de la jugada
		probabilidadJugadaGanadora.repeticiones++;
		probabilidadJugadaGanadora.multiplicaciones += probabilidadJugadaGanadora.jugadaGanadora.multiplicador;
	
		//incrementamos el maximo pagable
		maximoPagable += probabilidadJugadaGanadora.jugadaGanadora.multiplicador;
		
	}
	
	/**
	 * Dada la forma de una jugada ganadora, devuelve el
	 * objeto de probabilidad de la misma
	 * 
	 * @return
	 */
	public ProbabilidadJugadaGanadora obtenerProbabilidadJugadaGanadora(JugadaGanadora jugadaGanadora) {
		
		//recorremos la lista de probabiliadades
		//de jugadas ganadoras para obtener la
		//necesaria
		for(ProbabilidadJugadaGanadora probabilidadJugadaGanadora : probabilidadJugadasGanadoras) {
			if(probabilidadJugadaGanadora.jugadaGanadora == jugadaGanadora) {
				return probabilidadJugadaGanadora;
			}
		}
		
		return null;
	}
}