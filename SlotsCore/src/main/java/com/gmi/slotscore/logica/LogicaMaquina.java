package com.gmi.slotscore.logica;

import com.gmi.slotscore.constante.FiguraTipo;
import com.gmi.slotscore.constante.JugadaTipo;
import com.gmi.slotscore.constante.Sistema;
import com.gmi.slotscore.dominio.Carta;
import com.gmi.slotscore.dominio.Figura;
import com.gmi.slotscore.dominio.JugadaGanadora;
import com.gmi.slotscore.dominio.JugadaGanadoraBonus;
import com.gmi.slotscore.dominio.JugadaLinea;
import com.gmi.slotscore.dominio.JugadaLineaGanadora;
import com.gmi.slotscore.dominio.Linea;
import com.gmi.slotscore.dominio.Maquina;
import com.gmi.slotscore.dominio.Rodillo;
import com.gmi.slotscore.dominio.estadistica.EstadisticaMaquina;
import com.gmi.slotscore.dominio.estadistica.GanadorTipoJugadaComparator;
import com.gmi.slotscore.excepcion.SystemException;
import com.gmi.slotscore.persistencia.dao.ConfiguracionDAO;
import com.gmi.slotscore.persistencia.dao.EstadisticaDAO;
import com.gmi.slotscore.util.MathUtil;
import com.gmi.slotscore.util.SlotsRandomGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Toda la logica correspondiente a la maquina tragamonedas, como
 * ejecutar una apuesta, calcular lineas ganadoras, montos, etc, se
 * realiza en esta clase.
 * 
 * @author Pablo Caviglia
 *
 */
public class LogicaMaquina {

	public Maquina maquina;
	
    //daos
    private ConfiguracionDAO configuracionDAO;
    private EstadisticaDAO estadisticaDAO;

	/**
	 * Representa una tirada en la maquina.
	 * El indice primario indica el reel y el 
	 * indice secundario indica el valor comenzando
	 * desde la primer linea horizontal
	 * 
	 */
	public int[][] tiradaActualValores;
	
	/**
	 * Representa una tirada en la maquina.
	 * El indice primario indica el rodillo y el 
	 * indice secundario indica el indice dentro
	 * desde la primer linea horizontal
	 * 
	 */	
	public int[] tiradaActualIndices;
	
	
	/**
	 * La tirada ficticia es aquella que fue
	 * modificada con valores especificos que
	 * no tienen porque respetar el orden de
	 * las figuras en los rodillos. Generalmente
	 * es usado cuando se cancelan jugadas porque
	 * se pasan del monto de pago maximo configurado
	 * en la maquina
	 */
	public int[][] tiradaFicticia;
	
	/**
	 * Una vez que se ejecuta una tirada, las posibles
	 * jugadas (ganadoras o no) son almacendas en esta
	 * lista
	 */
	public List<JugadaLinea> jugadasLineas = new ArrayList<JugadaLinea>();
	
	/**
	 * Una vez calculadas las jugadas, se guardan en
	 * esta coleccion la lista de jugadas ganadoras
	 * con su respectivo monto
	 */
	public List<JugadaLineaGanadora> jugadasLineasGanadoras = new ArrayList<JugadaLineaGanadora>();
	
    //minijuego menor mayor
    public int manoMenorMayor;
    public float montoActualMenorMayor;
    public float montoGanadoMenorMayor;
    public int cartaMaquinaMenorMayor;
    public int cartaJugadorMenorMayor;

    /**
	 * Distribuye montos de premios cancelados
	 * entre las jugadas ganadoras disponibles
	 * en la configuracion usada actualmente
	 * por la maquina. La distribucion se hace
	 * respetando los porcentajes de pago que
	 * posee cada jugada ganadora individualmente.
	 * Ej.: Si una jugada ganadora significa el
	 * 28% de los premios entregados en total entre
	 * todas las jugadas ganadoras, significa que de
	 * 100$ se van a repartir 28$ para esa jugada
	 * ganadora en particular.
	 */
	public GeneradorPremios generadorPremios;

    //monto distribuido para el jackpot
    public float montoDistribuidoJackpot;

    //monto distribuido para el
    //minijuego 'menor o mayor'
    public float montoDistribuidoMenorMayor;

    //indica el monto con el que se
    //ingresa al bonus menor mayor
	private float montoIngresoBonusMenorMayor;

    //cantidad de tiradas gratis
    //generadas por el 'bonus'
    public int cantidadTiradasGratis;

    //el monto total entregado a traves de
    //tiradas gratuitas (bonus)
    public float montoEntragadoTiradasGratuitas;

    //obtengo la cantidad de rodillos para
    //analizar la cantidad de numeros aleatorios
    //a generar
    private int cantRodillos;

    //chequeo el tamano vertical de las lineas
    private int tamanoY;

    //obtengo la cantidad de figuras por rodillo
    private int cantFigurasRodillo;


    /**
	 * Constructor de clase
	 * @param maquina
	 */
	public LogicaMaquina(Maquina maquina) {
		//creo una maquina y le asigno la configuracion
		this.maquina = maquina;
		maquina.logicaMaquina = this;
		//creamos el distribuidor de premios
		generadorPremios = new GeneradorPremios(maquina);
		//instaciamos los DAO's necesarios
		configuracionDAO = new ConfiguracionDAO();
		estadisticaDAO = new EstadisticaDAO();
		//inicializo la estructura 'tiradaActual'
		inicializarTiradaActual();

        cantRodillos = maquina.rodillos.size();
        tamanoY = maquina.lineas.get(0).getForma().length;
        cantFigurasRodillo = maquina.rodillos.get(0).getFiguras().size();

    }
	
	private void inicializarTiradaActual() {
		//inicializo el array de la tirada actual
		//con el tamano de una linea de ejemplo
		int tamanoX = maquina.lineas.get(0).getForma().length;
		int tamanoY = maquina.lineas.get(0).getForma()[0].length;
		//creo la estructura para guardar una tirada
		tiradaActualValores = new int[tamanoX][tamanoY];
		tiradaActualIndices = new int[maquina.rodillos.size()];
	}

	/**
	 * Ejecutor de la apuesta.
	 * Devuelve el monto del jugador luego
	 * de realizada la apuesta 
	 */
	public synchronized float apostarSlots(boolean demo) {

        long t1 = System.currentTimeMillis();

		//recorro la cantidad de rodillos para generar
		//los numeros aleatorios necesarios
		for(int i=0; i<cantRodillos; i++) {
			
			//genero el numero aleatorio para el rodillo actual
			int numeroAleatorio = (int)(SlotsRandomGenerator.getInstance().nextFloat() * cantFigurasRodillo)+1;

//            //TODO delete me
//            switch(i) {
//                case 0:
//                    numeroAleatorio = 13;
//                    break;
//                case 1:
//                    numeroAleatorio = 1
//                    break;
//                case 2:
//                    numeroAleatorio = 13;
//                    break;
//                case 3:
//                    numeroAleatorio = 13;
//                    break;
//                case 4:
//                    numeroAleatorio = 13;
//                    break;
//            }
			
			//obtengo el rodillo actual
			Rodillo rodilloActual = maquina.rodillos.get(i);
			List<Figura> figurasRodillo = rodilloActual.getFiguras();
			
			//cargo la linea superior de la tirada con 
			//los id y los indices de las figuras
			tiradaActualValores[0][i] = figurasRodillo.get(numeroAleatorio-1).getId();
			tiradaActualIndices[i] = numeroAleatorio;
			
			//obtengo el numero correspondiente a la
			//siguiente figura dentro del mismo rodillo
			int siguienteFiguraIndex = numeroAleatorio - 1;
			
			//recorro verticalmente cada rodillo
			for(int j=1; j<tamanoY; j++) {
				//incremento el id para la siguiente figura
				siguienteFiguraIndex++;
				//chequeo si la siguiente figura es mayor
				//al tamano de figuras dentro del rodillo.
				//En caso de que sea mayor comienza en 0 de nuevo
				if(siguienteFiguraIndex > (cantFigurasRodillo-1)) {
					siguienteFiguraIndex = 0;
				}
				//cargo la linea siguiente a la superior (siguienteFiguraIndex++)
				tiradaActualValores[j][i] = figurasRodillo.get(siguienteFiguraIndex).getId();
			}
		}

		if(demo) {
			//configuramos el maximo de lineas
			//y el maximo apostable por linea
			maquina.lineasApostadas = (short)maquina.lineas.size();
			maquina.dineroApostadoPorLinea = Sistema.APUESTA_MAXIMA_POR_LINEA;
			//limpio la jugada anterior
			jugadasLineas.clear();
			//generamos un premio en modo demo
			generadorPremios.generarJugadaGanadoraDemo();
			//buscamos la posicion adecuada para dar
			//solamente el premio que nos devuelve en 
			//una sola linea
			tiradaFicticia = generadorPremios.tiradaFicticia;
			//agregamos la jugada que fue generada
			//a la lista de jugadas ganadoras
			List<JugadaLineaGanadora> jugadas = new ArrayList<JugadaLineaGanadora>();
			jugadas.add(generadorPremios.premioGenerado);
			//copiamos la lista
			//de jugadas ganadoras
			jugadasLineasGanadoras = new ArrayList<JugadaLineaGanadora>(jugadas);
			return 0f;
		}
		else {

			//flag que indica si la
			//apuesta actual es gratuita
			boolean apuestaGratuita = false;
			//si la tirada actual es gratuita o
			//'bonus', no decrementamos el monto
			//de apuesta del jugador
			if(cantidadTiradasGratis == 0) {
                //decremento el monto apostado
				float apostado = maquina.lineasApostadas * maquina.dineroApostadoPorLinea;
				decrementarMontoJugador(apostado);
			}
			else {
				//configuramos el flag
				apuestaGratuita = true;
				//decrementamos la cantidad
				//de tiradas gratuitas
				cantidadTiradasGratis--;
			}

			//borramos la tirada ficticia
			tiradaFicticia = null;
			generadorPremios.tiradaFicticia = null;
			generadorPremios.premioGenerado = null;
			
			//limpio la jugada anterior
			jugadasLineas.clear();

			//obtengo las jugadas de la tirada actual
			obtenerJugadasLineas();

            //obtengo la jugada scatter de la tirada actual
            obtenerJugadaScatter();

            //obtengo la jugada bonus menor mayor de la tirada actual
            obtenerJugadaBonusGame();

            //obtengo la jugada bonus de la tirada actual
			obtenerJugadaBonus();

			//obtengo las jugadas en lineas ganadoras
			List<JugadaLineaGanadora> jugadasLineasGanadoras = obtenerJugadasLineasGanadoras(jugadasLineas);

			//distribuimos un porcentaje dado
			//del monto de la apuesta para el
			//pozo JACKPOT
			distribuirMontoPremioJackpot();


            //si no hubieron jugadas convencionales
            //verificamos si existe un premio jackpot
            //o bonus
            if(jugadasLineasGanadoras.size() == 0) {
                //Chequeamos que el distribuidor de premios
                //tenga algun premio disponible para dar
                boolean tienePremio = generadorPremios.generarPremio();
                if(tienePremio) {
                    //si tiene premio buscamos la posicion
                    //adecuada para dar solamente el premio
                    //que nos devuelve en una sola linea
                    tiradaFicticia = generadorPremios.tiradaFicticia;
                    //agregamos la jugada que fue generada
                    //a la lista de jugadas ganadoras
                    jugadasLineasGanadoras.add(generadorPremios.premioGenerado);
                }
            }


			//calculo y pago el dinero que se ha
			//ganado en la ultima tirada ejecutada
			pagoJugadasLineasGanadoras(apuestaGratuita, jugadasLineasGanadoras);

			//asignamos las tiradas gratuitas
			//generados por jugadas bonus
			asignarTiradasGratuitas(jugadasLineasGanadoras, apuestaGratuita);

			//asignamos el juego bonus menor
			//mayor en caso de ser necesario
			asignarBonusGame(jugadasLineasGanadoras);

            //calcula la posicion de las
            //figuras de las jugadas ganadoras
            calcularPosicionFigurasJugadasGandoras(jugadasLineasGanadoras);

			//reconfiguro los valores posibles
			//de apuestas despues de haber
            //ejecutado esta apuesta totalmente
            reconfigurarPosiblesApuestas();

			//registramos las estadisticas
			//de la apuesta
			registrarEstadisticasApuesta(jugadasLineasGanadoras, apuestaGratuita);

			//copiamos la lista
			//de jugadas ganadoras
			this.jugadasLineasGanadoras = new ArrayList<JugadaLineaGanadora>(jugadasLineasGanadoras);

			//si el jugador ya no cuenta
			//con mas creditos, finalizamos
			//el juego
			if(maquina.montoJugador <= 0) {
				finalizaSesionJuego();
			}

            long t2 = System.currentTimeMillis();
            System.out.println("----------> tiempo ejecucion apuesta :: " + (t2-t1));

			//devolvemos el monto 
			//actual del jugador
			return maquina.montoJugador;
		}
	}

    private void calcularPosicionFigurasJugadasGandoras(List<JugadaLineaGanadora> jugadas) {
        for(JugadaLineaGanadora jugadaLineaGanadora : jugadas) {

            JugadaLinea jugadaLinea = jugadaLineaGanadora.getJugadaLinea();
            int CANTIDAD_FIGURAS_RODILLO = maquina.configuracion.getRodillos().get(0).getFiguras().size();
            int[][] posicionFiguras = new int[tiradaActualValores[0].length][tiradaActualValores.length];

            if(jugadaLineaGanadora.getLinea() == null) {
                int figuraId = jugadaLinea.getFiguras().get(0).getId();
                for(int j=0; j<tiradaActualValores[0].length; j++) {
                    for(int k=0; k<tiradaActualValores.length; k++) {
                        if(tiradaActualValores[k][j] == figuraId) {
                            int posicionRodillo = tiradaActualIndices[j];
                            int posicionFigura = posicionRodillo;
                            if((posicionFigura + k) > CANTIDAD_FIGURAS_RODILLO) {
                                posicionFigura = posicionFigura - CANTIDAD_FIGURAS_RODILLO + k;
                            }
                            else {
                                posicionFigura += k;
                            }
                            posicionFiguras[j][k] = posicionFigura;
                        }
                        else {
                            posicionFiguras[j][k] = -1;
                        }
                    }
                }
            }
            else {
                int cantidadFigurasPremio = jugadaLineaGanadora.getCantidadFigurasJugada();
                int[][] formaPremio = jugadaLinea.getLinea().getForma();
                for(int j=0; j<tiradaActualValores[0].length; j++) {
                    for(int k=0; k<tiradaActualValores.length; k++) {
                        if(formaPremio[k][j] == 1 && cantidadFigurasPremio > 0) {
                            int posicionRodillo = tiradaActualIndices[j];
                            int posicionFigura = posicionRodillo;
                            if((posicionFigura + k) > CANTIDAD_FIGURAS_RODILLO) {
                                posicionFigura = posicionFigura - CANTIDAD_FIGURAS_RODILLO + k;
                            }
                            else {
                                posicionFigura += k;
                            }
                            posicionFiguras[j][k] = posicionFigura;
                            cantidadFigurasPremio--;
                        }
                        else {
                            posicionFiguras[j][k] = -1;
                        }
                    }
                }
            }

            //set figures position
            jugadaLinea.setPosicionFiguras(posicionFiguras);
        }
    }
	
    public void inicializarMinijuegoMenorMayor() {
        //si es la primer mano
        //el valor inicial ganado
        //es el monto de entrada
        montoActualMenorMayor = montoIngresoBonusMenorMayor;
    }

    /**
	 * Obtiene la carta correspondiente
	 * a la maquina en el minijuego 
	 * 'menor o mayor'
	 * @return
	 */
	public int generarCartaMinijuegoMenorMayor(boolean maquina, Integer exclude) {
		
		int[] cartasAdmitidas;

        if(maquina) {
            cartasAdmitidas = new int[]{3,4,5,6,7,8,9,10};
        }
        else {
            cartasAdmitidas = new int[]{1,2,3,4,5,6,7,8,9,10,11,12};
        }

        System.out.println("-------");
        System.out.println("----------> EXCLUDE :: " + exclude);
        System.out.println("----------> ADMITIDAS :: ");
        for(int carta : cartasAdmitidas) {
            System.out.print(carta + ",");
        }
        System.out.println("-------");


        if(exclude != null) {
            int excludeInt = exclude.intValue();
            int[] cartasAdmitidasFilter = new int[cartasAdmitidas.length-1];
            int i = 0;
            for(int cartaAdmitida : cartasAdmitidas) {
                if(cartaAdmitida != excludeInt) {
                    cartasAdmitidasFilter[i] = cartaAdmitida;
                    i++;
                }
            }
            cartasAdmitidas = cartasAdmitidasFilter;
        }

		//elegimos una carta aleatoria
        int cartaId = cartasAdmitidas[(int)(SlotsRandomGenerator.getInstance().nextFloat() * ((float)cartasAdmitidas.length))];
		
		//obtenemos la carta
        if(maquina) {
            cartaMaquinaMenorMayor = cartaId;
        }
        else {
            cartaJugadorMenorMayor = cartaId;
        }
		
		return cartaId;
	}
	
	public float finalizarMinijuegoMenorMayor() {
		
		//incrementamos al monto del
		//jugador el monto ganado en
		//el minijuego
		maquina.montoJugador = (float)MathUtil.roundTwoDecimals(maquina.montoJugador + montoGanadoMenorMayor);
		
		//seteamos valor a otra
		//variable antes de 
		//resetear la misma
		float montoGanadorAntesReset = (float)MathUtil.roundTwoDecimals(montoGanadoMenorMayor);
		
		//reconfigura variables
		reconfigurarVariablesMenorMayor();
		
		return montoGanadorAntesReset;
	}
	
	public void reconfigurarVariablesMenorMayor() {
		montoActualMenorMayor = 0f;
		montoGanadoMenorMayor = 0f;
		manoMenorMayor = 0;
		cartaMaquinaMenorMayor = 0;
		cartaJugadorMenorMayor = 0;
	}
	
	public boolean apostarMinijuegoMenorMayor(boolean minor) {

		boolean ganaMinijuego = (minor && cartaJugadorMenorMayor < cartaMaquinaMenorMayor) || (!minor && cartaJugadorMenorMayor > cartaMaquinaMenorMayor);

		if(ganaMinijuego) {
			if(manoMenorMayor == 0) {
				montoGanadoMenorMayor = montoActualMenorMayor;
			}
			else {
				montoGanadoMenorMayor = (float)MathUtil.roundTwoDecimals(montoGanadoMenorMayor * 2f);
                montoActualMenorMayor = (float)MathUtil.roundTwoDecimals((montoActualMenorMayor * 2f));
			}
		}
		else {
			manoMenorMayor = 0;
			montoActualMenorMayor = 0f;
			montoGanadoMenorMayor = 0f;
		}
		
		//incrementamos el numero
		//de mano del jugador
		manoMenorMayor++;
		
		return ganaMinijuego;
	}
	
	/**
	 * En base al parametro de si el minijuego
	 * se debe dar como ganado, y a la seleccion
	 * de carta 'menor o mayor' dada por el usuario,
	 * se obtiene la carta que satisfaga esas
	 * caracteristicas
	 * 
	 * @param gana
	 * @param mayor
	 * @return
	 */
	private int obtenerCartaIdMinijuego(boolean gana, boolean mayor) {
		
		int cartaId = 0;
		//lista contenedora
		//de numeros validos
		List<Integer> numerosValidos = new ArrayList<Integer>();
		//obtenemos las cartas
		List<Carta> cartas = maquina.configuracion.getCartas();
		
		//recorremos la lista de cartas
		for(Carta carta : cartas) {
			
			if(gana) {
				if(mayor) {
					if(carta.getId() > cartaMaquinaMenorMayor) {
						numerosValidos.add(carta.getId());
					}
				}
				else {
					if(carta.getId() < cartaMaquinaMenorMayor) {
						numerosValidos.add(carta.getId());
					}
				}
			}
			else {
				if(mayor) {
					if(carta.getId() < cartaMaquinaMenorMayor) {
						numerosValidos.add(carta.getId());
					}
				}
				else {
					if(carta.getId() > cartaMaquinaMenorMayor) {
						numerosValidos.add(carta.getId());
					}
				}
			}
		}

		if(numerosValidos != null && numerosValidos.size() > 0) {
			//obtenemos una figura aleatoria
			//de la lista de numeros validos
			cartaId = numerosValidos.get(((int)(SlotsRandomGenerator.getInstance().nextFloat() * (float)numerosValidos.size())));
		}
		
		return cartaId;
	}

	/**
	 * Graba estadisticas relacionadas
	 * a la ejecucion de la ultima 
	 * apuesta
	 */
	private void registrarEstadisticasApuesta(List<JugadaLineaGanadora> jugadas, boolean apuestaGratuita) {
		
		EstadisticaMaquina estadisticaMaquina = maquina.estadisticaMaquina;
		
		/**
		 * Solo si la apuesta no es 
		 * gratuita registramos como
		 * que fue dinero apostado 
		 * por el jugador
		 */
		if(!apuestaGratuita) {
			//incremento la cantidad de  jugadas
			//totales hechas (una por linea)
			estadisticaMaquina.cantidadApuestas += maquina.lineasApostadas;
			//incremento la cantidad de dinero
			//apostado entre todas las lineas
			estadisticaMaquina.dineroApostado += (((float)maquina.lineasApostadas) * maquina.dineroApostadoPorLinea);
		}
		
		//agrego a las estadisticas las jugadas ganadoras
		for(JugadaLineaGanadora jlg : jugadas) {
			int tiradasGratuitas = 0;
			//asigno los premio de bonus
			if(jlg.getTipo() == JugadaTipo.BONUS) {
				JugadaGanadoraBonus jugadaGanadoraBonus = ((JugadaGanadoraBonus)jlg.getJugadaGanadora());
				tiradasGratuitas = jugadaGanadoraBonus.getTiradasGratis();
				estadisticaMaquina.cantidadApuestasBonus += tiradasGratuitas;
			}
			else if(jlg.getTipo() == JugadaTipo.JACKPOT) {
				
				/**
				 * 		  !!! IMPORTANTE !!!
				 * 		
				 * No registramos el monto ganado con el
				 * JACKPOT ya que ese monto ya se asigna
				 * como ganado cada vez que se realiza una
				 * apuesta maxima y se retira dinero para
				 * este pozo.
                 *
                 * TODO mmm... me parece q esto ya no aplica con android
				 *  
				 */
				
				//incremento en uno la cantidad de 
				//jugadas totales ganadas
				estadisticaMaquina.cantidadApuestasGanadas++;
			}
			else {
				
				/**
				 * Solo si la jugada no fue generada por el 
				 * distribuidor de premios se suma el dinero
				 * ganado al modulo estadistico. Esto se hace
				 * debido a que esa suma se va realizando 
				 * gradualmente por el modulo EvaluadorJugada
				 * cuando las jugadas son canceladas y el monto
				 * restante es distribuido. Con esto logramos  
				 * que los los valores estadisticos no se 
				 * disparen si sale un premio grande generado
				 * por el distribuidor de premios. 
				 */
				if(!jlg.isGenerado()) {
					//calculamos el monto ganado
					float dineroGanado = (((float)jlg.getJugadaGanadora().multiplicador) * maquina.dineroApostadoPorLinea);
					//incremento la cantidad de dinero ganado en esta apuesta
					estadisticaMaquina.dineroGanado += dineroGanado;
				}
				//incremento en uno la cantidad de
				//jugadas totales ganadas
				estadisticaMaquina.cantidadApuestasGanadas++;
			}
			//registro el tipo de jugada ganada
			estadisticaMaquina.registrarGanadorTipoJugada(jlg.getJugadaGanadora().obtenerFormateado(), jlg.getTipo(), jlg.getJugadaGanadora().multiplicador, tiradasGratuitas);
		}

		//ordeno la lista de tipo de jugadas ganadoras
		Collections.sort(estadisticaMaquina.ganadoresTipoJugada, new GanadorTipoJugadaComparator());
		//actualizamos los valores
		//estadisticos generales

        System.out.println(":::::::::::::: CANCELAMOS ESCRITURA DISCO VALORES ESTADISTICOS");
//		estadisticaMaquina.actualizarValoresEstadisticos();
		
	}
	
	/**
	 * Si se ha generado un bonus menor mayor
	 * debemos configurar las variables 
	 * necesarias para dejar pronta la logica
	 * para jugar
	 */
	private void asignarBonusGame(List<JugadaLineaGanadora> jugadas) {
		//recorro la lista de jugadas ganadoras
		for(JugadaLineaGanadora jugadaLineaGanadora : jugadas) {
			//si la linea ganadora es de tipo bonus
			if(jugadaLineaGanadora.getTipo() == JugadaTipo.BONUS_GAME) {
				//calculamos el monto con el
				//que entra el jugador al
				//bonus menor mayor
				montoIngresoBonusMenorMayor = (float)MathUtil.roundTwoDecimals((((float)maquina.lineasApostadas) * maquina.dineroApostadoPorLinea));
				//salimos
				break;
			}
		}
	}
	
	/**
	 * Asigna las tiradas gratuitas generadas
	 * por las jugadas ganadoras de tipo bonus
	 */
	private void asignarTiradasGratuitas(List<JugadaLineaGanadora> jugadas, boolean tiradaGratuita) {
		//creamos un iterador de las jugadas ganadoras
		Iterator<JugadaLineaGanadora> jugadasIt = jugadas.iterator();
		//recorro la lista de jugadas ganadoras
		while(jugadasIt.hasNext()) {
			//obtenemos la proxima jugada ganadora
			JugadaLineaGanadora jugadaLineaGanadora = jugadasIt.next();
			//si la linea ganadora es de tipo bonus
			if(jugadaLineaGanadora.getTipo() == JugadaTipo.BONUS) {
				//si la tirada es gratuita ya no consideramos la jugada bonus
				//actual y la borramos de la lista de jugadas ganadoras
				if(tiradaGratuita) {
					//removemos el elemento de la lista
					jugadasIt.remove();
				}
				else {
                    //incremento la cantidad de tiradas
                    //gratuitas generadas por el bonus
					JugadaGanadoraBonus jugadaGanadoraBonus = (JugadaGanadoraBonus)jugadaLineaGanadora.getJugadaGanadora();
					cantidadTiradasGratis += jugadaGanadoraBonus.getTiradasGratis();
				}
			}
		}
	}
	
	/**
	 * Calcula y paga al jugador las jugadas
	 * ganadoras de la ultima tirada ejecutada
	 */
	private void pagoJugadasLineasGanadoras(boolean actualizarMontoJugadaGratuita, List<JugadaLineaGanadora> jugadas) {
		float totalAPagar = 0f;
		//recorro la lista de jugadas ganadoras
		for(JugadaLineaGanadora jugadaLineaGanadora : jugadas) {
			//si la linea ganadora no es bonus
			if(jugadaLineaGanadora.getTipo() != JugadaTipo.BONUS && jugadaLineaGanadora.getTipo() != JugadaTipo.BONUS_GAME) {
				float montoGanadoJugada = 0;
				//si la jugada es NORMAL o SCATTER
				if(jugadaLineaGanadora.getTipo() == JugadaTipo.NORMAL || jugadaLineaGanadora.getTipo() == JugadaTipo.SCATTER || jugadaLineaGanadora.getTipo() == JugadaTipo.WILD) {
					//obtengo el multiplicador de premio
					int multiplicador = jugadaLineaGanadora.getJugadaGanadora().multiplicador;
					//calculamos el monto ganado
					//por la jugada actual
					montoGanadoJugada = multiplicador * maquina.dineroApostadoPorLinea;
					//incremento al total a pagar el multiplicador
					//de esta jugada por el monto apostado por linea
					totalAPagar += montoGanadoJugada;
				}
				else if(jugadaLineaGanadora.getTipo() == JugadaTipo.JACKPOT) { //SI LA JUGADA ES JACKPOT
					//incrementamos al monto de pago
					//total el monto del JACKPOT
					montoGanadoJugada = montoDistribuidoJackpot;
					totalAPagar += montoDistribuidoJackpot;
					//reseteamos a cero el monto
					//del JACKPOT en el distribuidor
					montoDistribuidoJackpot = 0;
					//El monto del JACKPOT comienza
					//nuevamente desde cero
					actualizarValorConfiguracion(ConfiguracionDAO.JACKPOT_MONTO_ACTUAL, String.valueOf(0));
				}
				
				//si la apuesta actual fue gratuita 
				//actualizamos el monto de gratuitas
				if(actualizarMontoJugadaGratuita) {
					montoEntragadoTiradasGratuitas += montoGanadoJugada;
					//incrementamos la variable que guarda la
					//cantidad de dinero ganada por jugadas 
					//generadas durante tiradas gratuitas
					maquina.estadisticaMaquina.dineroGanadoBonus += montoGanadoJugada;
				}
				else {
					//incremento la cantidad de dinero ganada en este bonus
					//pero discriminado ese dinero solo por premios scatter
					if(jugadaLineaGanadora.getTipo() == JugadaTipo.SCATTER) {
						maquina.estadisticaMaquina.dineroGanadoScatter += montoGanadoJugada;	
					}
					else if(jugadaLineaGanadora.getTipo() == JugadaTipo.NORMAL || jugadaLineaGanadora.getTipo() == JugadaTipo.WILD) {
						maquina.estadisticaMaquina.dineroGanadoNormal += montoGanadoJugada;	
					}
					else if(jugadaLineaGanadora.getTipo() == JugadaTipo.JACKPOT) {
						maquina.estadisticaMaquina.dineroGanadoJackpot += montoGanadoJugada;	
					}
				}
			}
		}
		//incremento el valor al monto
		maquina.incrementarMontoJugador(totalAPagar);
	}
	
	private void decrementarMontoJugador(float valor) {
		//decremento el valor al monto
		maquina.incrementarMontoJugador(-valor);
		//hago un chequeo en caso de que
		//haya un bug y quede el valor
		//en negativo
		if(maquina.montoJugador < 0) {
			throw new RuntimeException("El monto del jugador nunca puede ser menor que cero!");
		}
	}

    private void obtenerJugadaScatter() {

        //modelo de figura scatter
        //(si fue configurado por el usuario)
        Figura figuraScatter = null;

        //coleccion con figuras donde sera
        //buscada la figura scatter
        List<Figura> figuras = maquina.configuracion.getFiguras();

        for(Figura figuraActual : figuras) {
            if(figuraActual.getTipo() == FiguraTipo.SCATTER) {
                figuraScatter = figuraActual;
                break;
            }
        }

        if(figuraScatter != null) {
            JugadaLinea jugadaLineaScatter = new JugadaLinea();
            jugadaLineaScatter.setTipo(JugadaTipo.SCATTER);
            for(int j=0; j<tiradaActualValores[0].length; j++) {
                for(int k=0; k<tiradaActualValores.length; k++) {
                    if(tiradaActualValores[k][j] == figuraScatter.getId()) {
                        jugadaLineaScatter.getFiguras().add(figuraScatter);
                    }
                }
            }
            if(jugadaLineaScatter.getFiguras().size() > 0) {
                jugadasLineas.add(jugadaLineaScatter);
            }
        }
    }


    private void obtenerJugadaBonusGame() {

        //modelo de figura bonus menor mayor
        Figura figuraBonusGame = null;

        //coleccion con figuras donde sera
        //buscada la figura scatter
        List<Figura> figuras = maquina.configuracion.getFiguras();

        for(Figura figuraActual : figuras) {
            if(figuraActual.getTipo() == FiguraTipo.BONUS_GAME) {
                figuraBonusGame = figuraActual;
                break;
            }
        }

        if(figuraBonusGame != null) {
            JugadaLinea jugadaLineaBonusGame = new JugadaLinea();
            jugadaLineaBonusGame.setTipo(JugadaTipo.BONUS_GAME);
            for(int j=0; j<tiradaActualValores[0].length; j++) {
                for(int k=0; k<tiradaActualValores.length; k++) {
                    if(tiradaActualValores[k][j] == figuraBonusGame.getId()) {
                        jugadaLineaBonusGame.getFiguras().add(figuraBonusGame);
                    }
                }
            }
            if(jugadaLineaBonusGame.getFiguras().size() > 0) {
                jugadasLineas.add(jugadaLineaBonusGame);
            }
        }
    }

    private void obtenerJugadaBonus() {
		
		//modelo de figura bonus 
		//(si fue configurado por el usuario)
		Figura figuraBonus = null;
		
		//coleccion con figuras donde sera 
		//buscada la figura scatter
		List<Figura> figuras = maquina.configuracion.getFiguras();
		
		for(Figura figuraActual : figuras) {
			if(figuraActual.getTipo() == FiguraTipo.BONUS) {
				figuraBonus = figuraActual;
				break;
			}
		}
		
		if(figuraBonus != null) {
            JugadaLinea jugadaLineaBonus = new JugadaLinea();
            jugadaLineaBonus.setTipo(JugadaTipo.BONUS);
            //recorro la tirada para compararla
            //con la forma de la linea
            for(int j=0; j<tiradaActualValores[0].length; j++) {
                for(int k=0; k<tiradaActualValores.length; k++) {
                    if(tiradaActualValores[k][j] == figuraBonus.getId()) {
                        jugadaLineaBonus.getFiguras().add(figuraBonus);
                    }
                }
            }
            if(jugadaLineaBonus.getFiguras().size() > 0) {
                jugadasLineas.add(jugadaLineaBonus);
            }
		}
	}
	
	private void obtenerJugadasLineas() {
		
		//recorro las lineas apostadas
		for(int i=0; i<maquina.lineasApostadas; i++) {

            //obtengo la forma de linea actual
            Linea linea = maquina.lineas.get(i);
            int[][] formaLinea = linea.getForma();

            //creo la jugada de linea para guardar datos
            JugadaLinea jugadaLinea = new JugadaLinea();
            jugadaLinea.setTipo(JugadaTipo.NORMAL);
            jugadaLinea.setLinea(linea);

            //agrego la jugada linea a la coleccion
            jugadasLineas.add(jugadaLinea);

            //recorro la tirada para compararla
            //con la forma de la linea
            for(int j=0; j<tiradaActualValores[0].length; j++) {
                for(int k=0; k<tiradaActualValores.length; k++) {
                    if(formaLinea[k][j] == 1) {
                        //obtengo la figura correspondinete
                        Figura figura = maquina.configuracion.obtenerFiguraPorId(tiradaActualValores[k][j]);
                        //la agrego al objeto jugadaLinea
                        jugadaLinea.getFiguras().add(figura);
                    }
                }
            }

            //cantida de figuras
            //wild en la linea
            int cantidadWild = 0;
            Figura figuraWild = maquina.configuracion.obtenerFiguraWild();

            //chequeamos si la jugada
            //es una jugada de tipo wild
            if(figuraWild != null) {
                for(int k=0; k<jugadaLinea.getFiguras().size(); k++) {

                    if(jugadaLinea.getFiguras().get(k).getId() == figuraWild.getId()) {
                        cantidadWild++;
                    }
                }

                //si la cantidad de figuras wild
                //es igual a la cantidad de rodillos
                //entonces es una jugada de tipo wild
                if(cantidadWild == maquina.rodillos.size()) {
                    jugadaLinea.setTipo(JugadaTipo.WILD);
                }
            }
		}
	}
	
	public void actualizarValoresEstadisticos(float dineroApostado, float dineroGanado, float dineroGanadoNormal, float dineroGanadoScatter, float dineroGanadoBonus, float dineroGanadoJackpot, long cantidadApuestas, long cantidadApuestasGanadas, long cantidadApuestasBonus) {
        try {
            //persistimos
            estadisticaDAO.actualizarValoresEstadisticos(dineroApostado, dineroGanado, dineroGanadoNormal, dineroGanadoScatter, dineroGanadoBonus, dineroGanadoJackpot, cantidadApuestas, cantidadApuestasGanadas, cantidadApuestasBonus);
        } catch (Exception e) {
            e.printStackTrace();
            throw new SystemException(e.getMessage());
        }
	}
	
	/**
	 * Devuelve todos los valores estadisticos
	 * registrados en la persistencia
	 * @return
	 */
	public Map<String, Float> obtenerValoresEstadistica() {
		Map<String, Float> valores = new HashMap<String, Float>();
        try {
            valores = estadisticaDAO.obtenerValores();
        } catch (Exception e) {
            throw new SystemException(e.getMessage());
		}
		return valores;
	}
	
	/**
	 * Resetea los valores estadisticos de 
	 * la maquina
	 */
	public void restablecerValoresEstadisticos() {

		maquina.estadisticaMaquina.dineroApostado = 0f; 
		maquina.estadisticaMaquina.dineroGanado = 0f; 
		maquina.estadisticaMaquina.dineroGanadoNormal = 0f; 
		maquina.estadisticaMaquina.dineroGanadoScatter = 0f; 
		maquina.estadisticaMaquina.dineroGanadoBonus = 0f; 
		maquina.estadisticaMaquina.dineroGanadoJackpot = 0f; 
		maquina.estadisticaMaquina.cantidadApuestas = 0; 
		maquina.estadisticaMaquina.cantidadApuestasGanadas = 0; 
		maquina.estadisticaMaquina.cantidadApuestasBonus = 0;
		
		//forzamos la escritura de los nuevos valores
		maquina.estadisticaMaquina.actualizarValoresEstadisticos();
		
	}
	
	/**
	 * Actualiza el valor de un registro de la
	 * tabla 'configuracion'
	 * @param codigo
	 * @param nuevoValor
	 */
	public void actualizarValorConfiguracion(String codigo, String nuevoValor) {
        System.out.println(":::::::::::::: CANCELAMOS ESCRITURA DISCO VALOR CONFIGURACION");
//        try {
//            //ejecutamos el cambio en la persistencia
//            configuracionDAO.actualizarValor(codigo, nuevoValor);
//        }
//        catch (Exception e) {
//            throw new SystemException(e.getMessage());
//        }
	}
	
	public Map<String, String> obtenerValoresConfiguracion() {
		try {
			return configuracionDAO.obtenerValores();
		} catch (Exception e) {
			throw new SystemException(e.getMessage());
		}
	}
	
	/**
	 * Carga desde la persistencia los
	 * valores de configuracion de la
	 * maquina
	 */
	public void cargarValoresConfiguracion() {
		//obtenemos todos los valores de
		//configuracion desde la persistencia
		Map<String, String> valoresConfiguracion = obtenerValoresConfiguracion();
		
		//obtenemos el valor de cada uno de
		//los campos que precisemos configurar
		String apuestaAdmitidaStr = valoresConfiguracion.get(ConfiguracionDAO.APUESTA_ADMITIDA);
		String apuestaMaximaPorLineaStr = valoresConfiguracion.get(ConfiguracionDAO.APUESTA_MAXIMA_POR_LINEA);
		String audioStr = valoresConfiguracion.get(ConfiguracionDAO.AUDIO);
		String cantidadLineasBonusStr = valoresConfiguracion.get(ConfiguracionDAO.CANTIDAD_LINEAS_APUESTA_GRATUITA);
		String factorConversionMonedaStr = valoresConfiguracion.get(ConfiguracionDAO.FACTOR_CONVERSION_MONEDA);
		String incrementoApuestaStr = valoresConfiguracion.get(ConfiguracionDAO.INCREMENTO_APUESTA);
		String montoApuestaGratuitaStr = valoresConfiguracion.get(ConfiguracionDAO.MONTO_APUESTA_GRATUITA);
		String apuestaWildStr = valoresConfiguracion.get(ConfiguracionDAO.APUESTA_WILD);

		String jackpotMontoMinimoStr = valoresConfiguracion.get(ConfiguracionDAO.JACKPOT_MONTO_MINIMO);
		String jackpotMontoActualStr = valoresConfiguracion.get(ConfiguracionDAO.JACKPOT_MONTO_ACTUAL);
		
		//finalmente los seteamos en memoria
		Sistema.APUESTA_ADMITIDA = Integer.parseInt(apuestaAdmitidaStr);
		Sistema.APUESTA_MAXIMA_POR_LINEA = Float.parseFloat(apuestaMaximaPorLineaStr);
		Sistema.CANTIDAD_LINEAS_APUESTA_GRATUITA = Short.parseShort(cantidadLineasBonusStr);
		Sistema.FACTOR_CONVERSION_MONEDA = Integer.parseInt(factorConversionMonedaStr);
		Sistema.INCREMENTO_APUESTA = Float.parseFloat(incrementoApuestaStr);
		Sistema.MONTO_APUESTA_GRATUITA = Float.parseFloat(montoApuestaGratuitaStr);
		Sistema.APUESTA_WILD = Byte.parseByte(apuestaWildStr);
		Sistema.JACKPOT_MONTO_MINIMO = Integer.parseInt(jackpotMontoMinimoStr);
		montoDistribuidoJackpot = Float.parseFloat(jackpotMontoActualStr);
	}
	
	/**
	 * Incrementa la cantidad de lineas 
	 * para apostar
	 */
	public void incrementarLineas() {
		
		//obtengo la cantidad actual de 
		//lineas apostadas e incremento uno
		int nuevasLineasApostadas = maquina.lineasApostadas + 1;
		
		//calculo el monto necesario para realizar esta apuesta
		float montoNecesario = (float)MathUtil.roundTwoDecimals((nuevasLineasApostadas) * maquina.dineroApostadoPorLinea);
		
		//si el monto necesario para realiazr la apuesta
		//es mayor a la que posee el jugador comenzamos
		//desde 1 linea apostada
		if(montoNecesario > maquina.montoJugador || nuevasLineasApostadas > maquina.lineas.size()) {
			maquina.lineasApostadas = (short)1;
		}
		else {
			maquina.lineasApostadas = (short)nuevasLineasApostadas;
		}
	}
	
	/**
	 * Incrementa la cantidad de dinero 
	 * apostado por linea. El incremento
	 * esta definido por la constante 
	 * Sistema.INCREMENTO_APUESTA
	 */
	public void incrementarApuesta() {

		//calculo el nuevo monto
		float nuevaApuesta = (float)MathUtil.roundTwoDecimals(maquina.dineroApostadoPorLinea + Sistema.INCREMENTO_APUESTA);
		
		//calculo el total de dinero necesario 
		//para poder cubrir la apuesta
		float nuevoMonto = (float)MathUtil.roundTwoDecimals(nuevaApuesta * maquina.lineasApostadas);
		
		//si el nuevo monto es mayor a lo que
		//puede apostar el jugador reinicio
		//al minimo el incremento de la apuesta
		if(nuevoMonto > maquina.montoJugador || nuevaApuesta > Sistema.APUESTA_MAXIMA_POR_LINEA) {
			maquina.setDineroApostadoPorLinea(Sistema.INCREMENTO_APUESTA);
		}
		else {
			maquina.setDineroApostadoPorLinea(nuevaApuesta);
		}
	}
	
	public void finalizaSesionJuego() {
		System.out.println("---> FINALIZA SESION JUEGO");
	}

    /**
     * Reconfiguro los limites de apuestas posibles
     * en base al monto que posee el jugador
     */
    public void reconfigurarPosiblesApuestas() {

        //si el monto del jugador es cero
        //se termino la partida actual
        if(maquina.montoJugador == 0f) {
            maquina.setDineroApostadoPorLinea(0f);
            maquina.lineasApostadas = (short)0;
        }
        else { //evaluamos una sub apuesta del monto actual del jugador

            //calculo el maximo apostable
            float costoApuestaActual = maquina.dineroApostadoPorLinea * ((float)maquina.lineasApostadas);

            //si el monto del jugador es menor al maximo apostable
            //tenemos que reevaluar las posibilidades de apuestas
            //del jugador
            if(maquina.montoJugador < costoApuestaActual) {

                boolean montoEncontrado = false;

                //recorremos la las lineas
                //de adelante hacia atras
                for(int i=maquina.lineas.size(); i>0; i--) {

                    //recorremos el monto saltando
                    //entre vuelta por apuesta minima
                    int cantidadIteraciones = (int)(Sistema.APUESTA_MAXIMA_POR_LINEA / Sistema.INCREMENTO_APUESTA);
                    for(int j=cantidadIteraciones; j>0; j--) {

                        /**
                         * Calculamos el monto actual en base
                         * a la cantidad de lineas actuales (i)
                         * y al monto por linea actual
                         * (j * Sistema.INCREMENTO_APUESTA)
                         */
                        float montoActual = (((float)j) * Sistema.INCREMENTO_APUESTA) * ((float)i);

                        //chequeamos si ya satisfacemos
                        //las necesidas del jugador
                        if(montoActual <= maquina.montoJugador) {
                            maquina.lineasApostadas = (short)i;
                            float dineroApostadoPorLineaTmp = (float)MathUtil.roundTwoDecimals(((float)j) * Sistema.INCREMENTO_APUESTA);
                            maquina.setDineroApostadoPorLinea(dineroApostadoPorLineaTmp);
                            montoEncontrado = true;
                            break;
                        }
                    }

                    if(montoEncontrado) {
                        break;
                    }
                }
            }
        }
    }

    /**
     * Distribuye un porcentaje dado del
     * monto de la apuesta actual.
     */
    public void distribuirMontoPremioJackpot() {

        //obtenemos los datos de la apuesta
        //para cerciorarnos de que es una
        //apuesta maxima
        short lineasApostadas = maquina.lineasApostadas;
        float dineroApostadoPorLinea = maquina.dineroApostadoPorLinea;
        float apuestaActualUsuario = lineasApostadas * dineroApostadoPorLinea;

        //calculamos el monto a extraer
        float montoExtraidoApuesta = ((Sistema.JACKPOT_PORCENTAJE_EXTRAIDO_APUESTA * apuestaActualUsuario) / 100f);

        //el monto extraido lo agregamos al
        //dinero 'ganado' en la maquina, ya
        //que el monto de la apuesta no va a
        //ser tocado, pero el monto extraido
        //de la apuesta ya va a ser ingresado
        //al pozo del JACKPOT
        maquina.estadisticaMaquina.dineroGanado += montoExtraidoApuesta;

        //incrementamos el monto
        montoDistribuidoJackpot += montoExtraidoApuesta;

        //guardamos en la persistencia
        //el nuevo monto del jackpot
        maquina.logicaMaquina.actualizarValorConfiguracion(ConfiguracionDAO.JACKPOT_MONTO_ACTUAL, String.valueOf(montoDistribuidoJackpot));
    }

    /**
     * Dada una lista de posibles jugadas ganadoras y una lista de jugadas ganadoras se evalua
     * cual de los elementos de la primer lista aplica a la segunda y se devuelve una lista
     * con ese conjunto.
     *
     * @return
     */
    public List<JugadaLineaGanadora> obtenerJugadasLineasGanadoras(List<JugadaLinea> jugadasLineas) {

        List<JugadaLineaGanadora> jugadasLineasGanadoras = new ArrayList<JugadaLineaGanadora>();
        List<JugadaGanadora> jugadasGanadoras = maquina.jugadasGanadoras;

		/*
		 * Recorremos la lista de jugadas por linea
		 * para chequear si hay algun premio
		 */
        for(JugadaLinea jugadaLinea : jugadasLineas) {

            //obtengo la lista de figuras que
            //forman el juego actual del jugador
            List<Figura> figurasJugadaLinea = jugadaLinea.getFiguras();

            //recorremos la lista de posibles
            //jugadas ganadoras para cada jugada
            for(JugadaGanadora jugadaGanadoraActual : jugadasGanadoras) {
                if(jugadaGanadoraActual.tipo == JugadaTipo.SCATTER && jugadaLinea.getTipo() == JugadaTipo.SCATTER) {

                    //flag que indica si se cumple la jugada
                    boolean jugadaValida = true;

                    //obtengo la lista de figuras necesarias
                    //para crear una jugada ganadora
                    List<Figura> figurasGanadoras = jugadaGanadoraActual.figuras;

                    //recorro la lista de figuras ganadoras
                    for(int i=0; i<figurasGanadoras.size(); i++) {

                        //figura ganadora actual
                        Figura figuraGanadoraActual = figurasGanadoras.get(i);

                        //figura jugada actual (si es nula es porque la jugada es scatter)
                        Figura figuraJugadaActual = figurasJugadaLinea.size() >= (i+1) ? figurasJugadaLinea.get(i) : null;

                        if(figuraJugadaActual == null || figuraJugadaActual.getId() != figuraGanadoraActual.getId()) {
                            jugadaValida = false;
                        }
                    }

                    //si la jugada es valida creo
                    //el objeto con el premio
                    if(jugadaValida) {

                        //creo la jugada de linea ganadora
                        JugadaLineaGanadora jugadaLineaGanadora = new JugadaLineaGanadora();
                        jugadaLineaGanadora.setTipo(JugadaTipo.SCATTER);
                        jugadaLineaGanadora.setFiguras(figurasJugadaLinea);
                        jugadaLineaGanadora.setJugadaGanadora(jugadaGanadoraActual);
                        jugadaLineaGanadora.setLinea(jugadaLinea.getLinea());
                        jugadaLineaGanadora.setJugadaLinea(jugadaLinea);

                        //agrego la linea de jugada ganadora a la coleccion
                        jugadasLineasGanadoras.add(jugadaLineaGanadora);

                        break;
                    }
                }
                if(jugadaGanadoraActual.tipo == JugadaTipo.BONUS_GAME && jugadaLinea.getTipo() == JugadaTipo.BONUS_GAME) {

                    //flag que indica si se cumple la jugada
                    boolean jugadaValida = true;

                    //obtengo la lista de figuras necesarias
                    //para crear una jugada ganadora
                    List<Figura> figurasGanadoras = jugadaGanadoraActual.figuras;

                    //recorro la lista de figuras ganadoras
                    for(int i=0; i<figurasGanadoras.size(); i++) {

                        //figura ganadora actual
                        Figura figuraGanadoraActual = figurasGanadoras.get(i);

                        //figura jugada actual (si es nula es porque la jugada es scatter)
                        Figura figuraJugadaActual = figurasJugadaLinea.size() >= (i+1) ? figurasJugadaLinea.get(i) : null;

                        if(figuraJugadaActual == null || figuraJugadaActual.getId() != figuraGanadoraActual.getId()) {
                            jugadaValida = false;
                        }
                    }

                    //si la jugada es valida creo
                    //el objeto con el premio
                    if(jugadaValida) {

                        //creo la jugada de linea ganadora
                        JugadaLineaGanadora jugadaLineaGanadora = new JugadaLineaGanadora();
                        jugadaLineaGanadora.setTipo(JugadaTipo.BONUS_GAME);
                        jugadaLineaGanadora.setFiguras(figurasJugadaLinea);
                        jugadaLineaGanadora.setJugadaGanadora(jugadaGanadoraActual);
                        jugadaLineaGanadora.setLinea(jugadaLinea.getLinea());
                        jugadaLineaGanadora.setJugadaLinea(jugadaLinea);

                        //agrego la linea de jugada ganadora a la coleccion
                        jugadasLineasGanadoras.add(jugadaLineaGanadora);

                        break;
                    }
                }
                else if(jugadaGanadoraActual.tipo == JugadaTipo.NORMAL && jugadaLinea.getTipo() == JugadaTipo.NORMAL) {

                    //flag que indica si se cumple la jugada
                    boolean jugadaValida = true;

                    //obtengo la lista de figuras necesarias
                    //para crear una jugada ganadora
                    List<Figura> figurasGanadoras = jugadaGanadoraActual.figuras;

                    //contadores de tipos de figura
                    int cantidadFigurasNormal = 0;
                    int cantidadFigurasWild = 0;

                    //recorro la lista de figuras ganadoras
                    for(int i=0; i<figurasGanadoras.size(); i++) {

                        //figura ganadora actual
                        Figura figuraGanadoraActual = figurasGanadoras.get(i);

                        //figura jugada actual (si es nula es porque la jugada es scatter)
                        Figura figuraJugadaActual = figurasJugadaLinea.get(i);

                        if((figuraJugadaActual.getId() != figuraGanadoraActual.getId()) && figuraJugadaActual.getTipo() != FiguraTipo.WILD) {
                            jugadaValida = false;
                            break;
                        }
                        else {

                            //si es una figura que se
                            //corresponde con la jugada
                            //ganadora
                            if(figuraJugadaActual.getTipo() == FiguraTipo.NORMAL) {

                                //incrementamos el contador
                                //de figuras normales
                                cantidadFigurasNormal++;
                            }
                            if(figuraJugadaActual.getTipo() == FiguraTipo.WILD) {

                                //incrementamos el contador
                                //de figuras wild
                                cantidadFigurasWild++;
                            }
                        }
                    }

                    //si hay mas figuras wild que
                    //normales, entonces la jugada
                    //no se valida
                    if(cantidadFigurasWild >= cantidadFigurasNormal) {
                        jugadaValida = false;
                    }

                    //si la jugada es valida creo
                    //el objeto con el premio
                    if(jugadaValida) {

                        //creo la jugada de linea ganadora
                        JugadaLineaGanadora jugadaLineaGanadora = new JugadaLineaGanadora();
                        jugadaLineaGanadora.setTipo(JugadaTipo.NORMAL);
                        jugadaLineaGanadora.setFiguras(figurasJugadaLinea);
                        jugadaLineaGanadora.setJugadaGanadora(jugadaGanadoraActual);
                        jugadaLineaGanadora.setLinea(jugadaLinea.getLinea());
                        jugadaLineaGanadora.setJugadaLinea(jugadaLinea);
                        jugadaLineaGanadora.setCantidadFigurasJugada(cantidadFigurasNormal + cantidadFigurasWild);

                        //agrego la linea de jugada ganadora a la coleccion
                        jugadasLineasGanadoras.add(jugadaLineaGanadora);

                        break;
                    }
                }
                else if(jugadaGanadoraActual.tipo == JugadaTipo.BONUS && jugadaLinea.getTipo() == JugadaTipo.BONUS) {

                    //flag que indica si se cumple la jugada
                    boolean jugadaValida = true;

                    //obtengo la lista de figuras necesarias
                    //para crear una jugada ganadora
                    List<Figura> figurasGanadoras = jugadaGanadoraActual.figuras;

                    //recorro la lista de figuras ganadoras
                    for(int i=0; i<figurasGanadoras.size(); i++) {

                        //figura ganadora actual
                        Figura figuraGanadoraActual = figurasGanadoras.get(i);

                        //figura jugada actual (si es nula es porque la jugada es bonus)
                        Figura figuraJugadaActual = figurasJugadaLinea.size() >= (i+1) ? figurasJugadaLinea.get(i) : null;

                        if(figuraJugadaActual == null || figuraJugadaActual.getId() != figuraGanadoraActual.getId()) {
                            jugadaValida = false;
                        }
                    }

                    //si la jugada es valida creo
                    //el objeto con el premio
                    if(jugadaValida) {

                        //creo la jugada de linea ganadora
                        JugadaLineaGanadora jugadaLineaGanadora = new JugadaLineaGanadora();
                        jugadaLineaGanadora.setTipo(JugadaTipo.BONUS);
                        jugadaLineaGanadora.setFiguras(figurasJugadaLinea);
                        jugadaLineaGanadora.setJugadaGanadora(jugadaGanadoraActual);
                        jugadaLineaGanadora.setLinea(jugadaLinea.getLinea());
                        jugadaLineaGanadora.setJugadaLinea(jugadaLinea);

                        //agrego la linea de jugada ganadora a la coleccion
                        jugadasLineasGanadoras.add(jugadaLineaGanadora);

                        break;
                    }
                }
                else if(jugadaGanadoraActual.tipo == JugadaTipo.WILD && jugadaLinea.getTipo() == JugadaTipo.WILD) {

                    //creo la jugada de linea ganadora
                    JugadaLineaGanadora jugadaLineaGanadora = new JugadaLineaGanadora();
                    jugadaLineaGanadora.setTipo(JugadaTipo.WILD);
                    jugadaLineaGanadora.setFiguras(figurasJugadaLinea);
                    jugadaLineaGanadora.setJugadaGanadora(jugadaGanadoraActual);
                    jugadaLineaGanadora.setLinea(jugadaLinea.getLinea());
                    jugadaLineaGanadora.setJugadaLinea(jugadaLinea);

                    //agrego la linea de jugada ganadora a la coleccion
                    jugadasLineasGanadoras.add(jugadaLineaGanadora);

                }
            }
        }

        return jugadasLineasGanadoras;
    }

    public String pad(String str, int size, char padChar) {
        StringBuffer padded = new StringBuffer(str);
        while (padded.length() < size) {
            padded.append(padChar);
        }
        return padded.toString();
    }

    public void mostrarTiradaIndicesActualConsola() {
        for(int i=0; i<tiradaActualIndices.length; i++) {
            System.out.print(tiradaActualIndices[i]+"\t");
        }
        System.out.println();
    }

    public void mostrarJugadasLineasConsola() {
        for(JugadaLinea jugadaLinea : jugadasLineas) {
            for(Figura figura : jugadaLinea.getFiguras()) {
                System.out.print("->" + figura.getId() + " \t");
            }
            System.out.println();
        }
    }

    public void mostrarTiradaActualConsola() {
        int[][] tiradaGenerica = null;
        if(tiradaFicticia != null) {
            //adaptamos la forma del array
            //ficticio para mostrarlo
            tiradaGenerica = new int[tiradaFicticia[0].length][tiradaFicticia.length];
            for(int i=0; i<tiradaGenerica.length; i++) {
                for(int j=0; j<tiradaGenerica[0].length; j++) {
                    tiradaGenerica[i][j] = tiradaFicticia[j][i];
                }
            }
        }
        else {
            tiradaGenerica = tiradaActualValores;
        }

        for(int i=0; i<tiradaGenerica.length; i++) {
            for(int j=0; j<tiradaGenerica[0].length; j++) {
                System.out.print(pad(String.valueOf(tiradaGenerica[i][j]), 4, ' ')+"\t");
            }
            System.out.println();
        }
        System.out.println();
    }
}