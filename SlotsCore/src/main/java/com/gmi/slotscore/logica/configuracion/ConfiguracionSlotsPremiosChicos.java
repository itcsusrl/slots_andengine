package com.gmi.slotscore.logica.configuracion;

import com.gmi.slotscore.dominio.Figura;

import com.gmi.slotscore.constante.FiguraTipo;
import com.gmi.slotscore.constante.JugadaTipo;
import com.gmi.slotscore.dominio.JugadaGanadora;
import com.gmi.slotscore.dominio.JugadaGanadoraBonus;
import com.gmi.slotscore.dominio.Rodillo;

public class ConfiguracionSlotsPremiosChicos extends Configuracion {

	protected void inicializarFiguras() {

		Figura figuraScatter = new Figura(0, "Scatter", FiguraTipo.SCATTER);
		Figura figuraBonus = new Figura(99, "Bonus", FiguraTipo.BONUS);
		Figura figuraWild = new Figura(100, "Wild", FiguraTipo.WILD);
		Figura figura1 = new Figura(1, "figura1", FiguraTipo.NORMAL);
		Figura figura3 = new Figura(3, "figura3", FiguraTipo.NORMAL);
		Figura figura4 = new Figura(4, "figura4", FiguraTipo.NORMAL);
		Figura figura5 = new Figura(5, "figura5", FiguraTipo.NORMAL);
		//Figura figura6 = new Figura(6, "figura6", FiguraTipo.NORMAL);
		Figura figura7 = new Figura(7, "figura7", FiguraTipo.NORMAL);
		Figura figura9 = new Figura(9, "figura9", FiguraTipo.NORMAL);
		Figura figura11 = new Figura(11, "figura11", FiguraTipo.NORMAL);
		Figura figura12 = new Figura(12, "figura12", FiguraTipo.NORMAL);
		
		figuras.add(figuraScatter);
		figuras.add(figuraBonus);
		figuras.add(figuraWild);
		figuras.add(figura1);
		figuras.add(figura3);
		figuras.add(figura4);
		figuras.add(figura5);
		//figuras.add(figura6);
		figuras.add(figura7);
		figuras.add(figura9);
		figuras.add(figura11);
		figuras.add(figura12);
		
	}

	protected void inicializarJugadasGanadoras() {

		/**
		 * PREMIOS GRANDES
		 */
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1", figuras, JugadaTipo.NORMAL, 50));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1,1", figuras, JugadaTipo.NORMAL, 100));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1,1,1", figuras, JugadaTipo.NORMAL, 200));
		
		/**
		 * PREMIOS MEDIANOS
		 */
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "5,5,5", figuras, JugadaTipo.NORMAL, 25));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "5,5,5,5", figuras, JugadaTipo.NORMAL, 50));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "5,5,5,5,5", figuras, JugadaTipo.NORMAL, 100));

		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7", figuras, JugadaTipo.NORMAL, 25));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7", figuras, JugadaTipo.NORMAL, 50));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7,7", figuras, JugadaTipo.NORMAL, 100));
		
		/*
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "6,6,6", figuras, JugadaTipo.NORMAL, 2));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "6,6,6,6", figuras, JugadaTipo.NORMAL, 20));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "6,6,6,6,6", figuras, JugadaTipo.NORMAL, 200));
		*/
		
		/**
		 * PREMIOS CHICOS
		 */
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4", figuras, JugadaTipo.NORMAL, 15));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4", figuras, JugadaTipo.NORMAL, 30));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4,4", figuras, JugadaTipo.NORMAL, 50));

		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3", figuras, JugadaTipo.NORMAL, 15));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3,3", figuras, JugadaTipo.NORMAL, 30));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3,3,3", figuras, JugadaTipo.NORMAL, 50));

        jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "9,9,9", figuras, JugadaTipo.NORMAL, 15));
        jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "9,9,9,9", figuras, JugadaTipo.NORMAL, 30));
        jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "9,9,9,9,9", figuras, JugadaTipo.NORMAL, 50));

		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "11,11,11", figuras, JugadaTipo.NORMAL, 20));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "12,12,12", figuras, JugadaTipo.NORMAL, 20));

		/**
		 * BONUS
		 */
		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99", figuras, 5));
		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99,99", figuras, 10));
		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99,99,99", figuras, 20));

		/**
		 * SCATTER
		 */
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0", figuras, JugadaTipo.SCATTER, 15));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0,0", figuras, JugadaTipo.SCATTER, 25));
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0,0,0", figuras, JugadaTipo.SCATTER, 50));

	}

	protected void inicializarRodillos() {

		//rodillo 1
		rodillos.add(Rodillo.configurarRodillo(1, "100,1,5,7,9,500,5,3,4,100,11,12,99,0,4,12,11", figuras));

		//rodillo 2
		rodillos.add(Rodillo.configurarRodillo(2, "100,1,12,7,9,5,7,4,12,11,500,5,99,0,3,12,11", figuras));

		//rodillo 3
		rodillos.add(Rodillo.configurarRodillo(3, "100,1,800,9,7,5,9,3,4,100,11,12,99,0,3,4,11", figuras));

		//rodillo 4
		rodillos.add(Rodillo.configurarRodillo(4, "100,1,5,7,9,5,4,0,3,5,99,0,800,3,4,7,4", figuras));

		//rodillo 5
		rodillos.add(Rodillo.configurarRodillo(5, "100,1,7,9,5,7,3,4,100,4,5,99,3,3,4,500,99", figuras));

	}
}