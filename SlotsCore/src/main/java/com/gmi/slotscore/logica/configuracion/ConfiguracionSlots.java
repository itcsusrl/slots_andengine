package com.gmi.slotscore.logica.configuracion;

import com.gmi.slotscore.constante.FiguraTipo;
import com.gmi.slotscore.constante.JugadaTipo;
import com.gmi.slotscore.dominio.Figura;
import com.gmi.slotscore.dominio.JugadaGanadora;
import com.gmi.slotscore.dominio.JugadaGanadoraBonus;
import com.gmi.slotscore.dominio.Rodillo;

public class ConfiguracionSlots extends Configuracion {

	protected void inicializarFiguras() {

		Figura figura1 = new Figura(0, "1", FiguraTipo.NORMAL);
		Figura figura2 = new Figura(1, "2", FiguraTipo.NORMAL);
		Figura figura3 = new Figura(3, "3", FiguraTipo.NORMAL);

        Figura figura4 = new Figura(4, "4", FiguraTipo.NORMAL);
		Figura figura5 = new Figura(5, "5", FiguraTipo.NORMAL);
		Figura figura6 = new Figura(6, "6", FiguraTipo.NORMAL);
		Figura figura7 = new Figura(7, "7", FiguraTipo.NORMAL);
		Figura figura8 = new Figura(9, "8", FiguraTipo.NORMAL);
		Figura figura9 = new Figura(11, "9", FiguraTipo.NORMAL);
		Figura figura10 = new Figura(12, "9", FiguraTipo.NORMAL);
		Figura figura11 = new Figura(99, "10", FiguraTipo.NORMAL);

        Figura figura12 = new Figura(100, "11", FiguraTipo.NORMAL);

		figuras.add(figura1);
        figuras.add(figura2);
        figuras.add(figura3);
        figuras.add(figura4);
        figuras.add(figura5);
        figuras.add(figura6);
        figuras.add(figura7);
        figuras.add(figura8);
        figuras.add(figura9);
        figuras.add(figura10);
        figuras.add(figura11);
        figuras.add(figura12);

	}

	protected void inicializarJugadasGanadoras() {

		/**
		 * PREMIOS GRANDES
		 */
		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1", figuras, JugadaTipo.NORMAL, 50));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1,1", figuras, JugadaTipo.NORMAL, 80));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1,1,1", figuras, JugadaTipo.NORMAL, 100));

		/**
		 * PREMIOS MEDIANOS
		 */
		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "5,5,5", figuras, JugadaTipo.NORMAL, 30));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "5,5,5,5", figuras, JugadaTipo.NORMAL, 45));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "5,5,5,5,5", figuras, JugadaTipo.NORMAL, 60));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7", figuras, JugadaTipo.NORMAL, 30));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7", figuras, JugadaTipo.NORMAL, 45));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7,7", figuras, JugadaTipo.NORMAL, 60));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "9,9,9", figuras, JugadaTipo.NORMAL, 30));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "9,9,9,9", figuras, JugadaTipo.NORMAL, 45));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "9,9,9,9,9", figuras, JugadaTipo.NORMAL, 60));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3", figuras, JugadaTipo.NORMAL, 30));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3,3", figuras, JugadaTipo.NORMAL, 45));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3,3,3", figuras, JugadaTipo.NORMAL, 60));


		/**
		 * PREMIOS CHICOS
		 */
		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4", figuras, JugadaTipo.NORMAL, 20));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4", figuras, JugadaTipo.NORMAL, 30));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4,4", figuras, JugadaTipo.NORMAL, 40));

		/*
		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "6,6,6", figuras, JugadaTipo.NORMAL, 10));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "6,6,6,6", figuras, JugadaTipo.NORMAL, 20));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "6,6,6,6,6", figuras, JugadaTipo.NORMAL, 30));
		*/

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "11,11,11", figuras, JugadaTipo.NORMAL, 30));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "11,11,11,11", figuras, JugadaTipo.NORMAL, 30));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "11,11,11,11,11", figuras, JugadaTipo.NORMAL, 40));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "12,12,12", figuras, JugadaTipo.NORMAL, 20));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "12,12,12,12", figuras, JugadaTipo.NORMAL, 30));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "12,12,12,12,12", figuras, JugadaTipo.NORMAL, 40));


		/**
		 * BONUS
		 */
		//bonus
		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99", figuras, 5));
		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99,99", figuras, 10));
		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99,99,99", figuras, 20));


		/**
		 * SCATTER
		 */
		//jugada scatter
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0", figuras, JugadaTipo.SCATTER, 20));

		//jugada scatter
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0,0", figuras, JugadaTipo.SCATTER, 40));

		//jugada scatter
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0,0,0", figuras, JugadaTipo.SCATTER, 60));

	}

	protected void inicializarRodillos() {

		//rodillo 1
		rodillos.add(Rodillo.configurarRodillo(1, "0,1,3,4,5,6,7,9,11,12,99,100,500,800", figuras));

		//rodillo 2
		rodillos.add(Rodillo.configurarRodillo(2, "0,1,3,4,5,6,7,9,11,12,99,100,500,800", figuras));

		//rodillo 3
		rodillos.add(Rodillo.configurarRodillo(3, "0,1,3,4,5,6,7,9,11,12,99,100,500,800", figuras));

		//rodillo 4
		rodillos.add(Rodillo.configurarRodillo(4, "0,1,3,4,5,6,7,9,11,12,99,100,500,800", figuras));

		//rodillo 5
		rodillos.add(Rodillo.configurarRodillo(5, "0,1,3,4,5,6,7,9,11,12,99,100,500,800", figuras));

	}
}