package com.gmi.slotscore.logica.configuracion;

import com.gmi.slotscore.constante.FiguraTipo;
import com.gmi.slotscore.constante.JugadaTipo;
import com.gmi.slotscore.dominio.Figura;
import com.gmi.slotscore.dominio.JugadaGanadora;
import com.gmi.slotscore.dominio.Rodillo;

public class ConfiguracionCincuentaCincuenta extends Configuracion {

	protected void inicializarFiguras() {

		Figura figura1 = new Figura(1, "1", FiguraTipo.NORMAL);
		Figura figura2 = new Figura(2, "2", FiguraTipo.NORMAL);
		//Figura figura3 = new Figura(3, "3", FiguraTipo.NORMAL);
		//Figura figura4 = new Figura(4, "4", FiguraTipo.NORMAL);
		
		figuras.add(figura1);
		figuras.add(figura2);
		//figuras.add(figura3);
		//figuras.add(figura4);
		
	}

	protected void inicializarJugadasGanadoras() {
		
		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1", figuras, JugadaTipo.NORMAL, 1));
//
//		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "2,2", figuras, JugadaTipo.NORMAL, 1));
//
//		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1", figuras, JugadaTipo.NORMAL, 1));
//
//		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "2,2,2", figuras, JugadaTipo.NORMAL, 1));
//
//		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1,1", figuras, JugadaTipo.NORMAL, 1));
//
//		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "2,2,2,2", figuras, JugadaTipo.NORMAL, 1));

	}

	protected void inicializarRodillos() {

		//rodillo 1
		rodillos.add(Rodillo.configurarRodillo(1, "1,2,1,2", figuras));

		//rodillo 2
		rodillos.add(Rodillo.configurarRodillo(2, "1,2,1,2", figuras));

		//rodillo 3
		rodillos.add(Rodillo.configurarRodillo(3, "1,2,1,2", figuras));

		//rodillo 4
		rodillos.add(Rodillo.configurarRodillo(4, "1,2,1,2", figuras));

		//rodillo 5
		rodillos.add(Rodillo.configurarRodillo(5, "1,2,1,2", figuras));

	}
}