package com.gmi.slotscore.logica.configuracion;

import com.gmi.slotscore.constante.FiguraTipo;
import com.gmi.slotscore.constante.JugadaTipo;
import com.gmi.slotscore.dominio.Figura;
import com.gmi.slotscore.dominio.JugadaGanadora;
import com.gmi.slotscore.dominio.JugadaGanadoraBonus;
import com.gmi.slotscore.dominio.Rodillo;

public class ConfiguracionGanaSiempre extends Configuracion {

	protected void inicializarFiguras() {

		Figura figura4 = new Figura(4, "figura4", FiguraTipo.NORMAL);
		Figura figura7 = new Figura(7, "figura7", FiguraTipo.NORMAL);
		Figura figura9 = new Figura(9, "figura9", FiguraTipo.NORMAL);
        Figura figuraScatter = new Figura(11, "scatter", FiguraTipo.SCATTER);
		Figura figura12 = new Figura(12, "figura12", FiguraTipo.NORMAL);
		Figura figuraBonus = new Figura(99, "bonus", FiguraTipo.BONUS);
        Figura figuraWild = new Figura(100, "Wild", FiguraTipo.WILD);

		figuras.add(figura4);
		figuras.add(figura7);
		figuras.add(figura9);
        figuras.add(figura12);
		figuras.add(figuraScatter);
        figuras.add(figuraBonus);
        figuras.add(figuraWild);

	}

	protected void inicializarJugadasGanadoras() {


		//jugada bonus
//		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99", figuras, 1));

		//jugada bonus
		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99", figuras, 2));

        //jugada scatter
        jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "11,11,11", figuras, JugadaTipo.SCATTER, 10));

        //jugada scatter
        jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "11,11,11,11", figuras, JugadaTipo.SCATTER, 20));

        //jugada scatter
        jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "11,11,11,11,11", figuras, JugadaTipo.SCATTER, 50));

        //jugada
//        jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4", figuras, JugadaTipo.NORMAL, 10));

        //jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4", figuras, JugadaTipo.NORMAL, 50));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4", figuras, JugadaTipo.NORMAL, 300));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4,4", figuras, JugadaTipo.NORMAL, 300));

//        jugada
//        jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7", figuras, JugadaTipo.NORMAL, 5));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7", figuras, JugadaTipo.NORMAL, 10));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7", figuras, JugadaTipo.NORMAL, 10));

		//jugada
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7,7", figuras, JugadaTipo.NORMAL, 100));

        //jugada
        jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "9,9,9", figuras, JugadaTipo.NORMAL, 10));

        //jugada
        jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "12,12,12", figuras, JugadaTipo.NORMAL, 30));

    }

	protected void inicializarRodillos() {

		//rodillo 1
		rodillos.add(Rodillo.configurarRodillo(1, "800,100,12,4,4,7,7,11,7,7,9,9,99", figuras));

		//rodillo 2
		rodillos.add(Rodillo.configurarRodillo(2, "800,99,12,4,4,7,7,11,7,7,9,9,99", figuras));

		//rodillo 3
		rodillos.add(Rodillo.configurarRodillo(3, "800,99,12,4,4,7,7,800,7,7,7,9,99", figuras));

		//rodillo 4
		rodillos.add(Rodillo.configurarRodillo(4, "11,99,12,4,4,7,800,7,11,7,9,100,99", figuras));

		//rodillo 5
		rodillos.add(Rodillo.configurarRodillo(5, "11,99,12,4,4,7,800,7,11,7,9,100,99", figuras));

	}
}