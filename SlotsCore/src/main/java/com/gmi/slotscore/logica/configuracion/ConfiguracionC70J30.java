package com.gmi.slotscore.logica.configuracion;

import com.gmi.slotscore.constante.FiguraTipo;
import com.gmi.slotscore.constante.JugadaTipo;
import com.gmi.slotscore.dominio.Figura;
import com.gmi.slotscore.dominio.JugadaGanadora;
import com.gmi.slotscore.dominio.JugadaGanadoraBonus;
import com.gmi.slotscore.dominio.Rodillo;

public class ConfiguracionC70J30 extends Configuracion {

	protected void inicializarFiguras() {

		Figura figuraScatter = new Figura(0, "Scatter", FiguraTipo.SCATTER);
		Figura figuraBonus = new Figura(99, "Bonus", FiguraTipo.BONUS);
		Figura figuraWild = new Figura(100, "Wild", FiguraTipo.WILD);
		Figura figuraKiller = new Figura(1000, "Killer", FiguraTipo.KILLER);
		Figura figura1 = new Figura(1, "figura1", FiguraTipo.NORMAL);
		Figura figura2 = new Figura(2, "figura2", FiguraTipo.NORMAL);
		Figura figura3 = new Figura(3, "figura3", FiguraTipo.NORMAL);
		Figura figura4 = new Figura(4, "figura4", FiguraTipo.NORMAL);
		Figura figura5 = new Figura(5, "figura5", FiguraTipo.NORMAL);
		Figura figura6 = new Figura(6, "figura6", FiguraTipo.NORMAL);
		Figura figura7 = new Figura(7, "figura7", FiguraTipo.NORMAL);
		Figura figura8 = new Figura(8, "figura8", FiguraTipo.NORMAL);
		Figura figura9 = new Figura(9, "figura9", FiguraTipo.NORMAL);
		Figura figura10 = new Figura(10, "figura10", FiguraTipo.NORMAL);
		
		figuras.add(figuraScatter);
		figuras.add(figuraBonus);
		figuras.add(figuraWild);
		figuras.add(figuraKiller);
		figuras.add(figura1);
		figuras.add(figura2);
		figuras.add(figura3);
		figuras.add(figura4);
		figuras.add(figura5);
		figuras.add(figura6);
		figuras.add(figura7);
		figuras.add(figura8);
		figuras.add(figura9);
		figuras.add(figura10);
		
	}

	protected void inicializarJugadasGanadoras() {

		
		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1", figuras, JugadaTipo.NORMAL, 50));
//		
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1,1", figuras, JugadaTipo.NORMAL, 100));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1,1,1", figuras, JugadaTipo.NORMAL, 300));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "2,2,2", figuras, JugadaTipo.NORMAL, 50));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "2,2,2,2", figuras, JugadaTipo.NORMAL, 100));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "2,2,2,2,2", figuras, JugadaTipo.NORMAL, 300));
//		
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3", figuras, JugadaTipo.NORMAL, 5));
//		
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3,3", figuras, JugadaTipo.NORMAL, 10));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3,3,3", figuras, JugadaTipo.NORMAL, 30));
//
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4", figuras, JugadaTipo.NORMAL, 5));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4", figuras, JugadaTipo.NORMAL, 10));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4,4", figuras, JugadaTipo.NORMAL, 30));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7", figuras, JugadaTipo.NORMAL, 2));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7", figuras, JugadaTipo.NORMAL, 4));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7,7", figuras, JugadaTipo.NORMAL, 20));
		
		/*

		//jugada bonus
		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99", figuras, 1));
		
		//jugada bonus
		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99", figuras, 2));
		
		//jugada bonus
		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99,99", figuras, 2));
		*/
		
		/*
		//jugada scatter
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0", figuras, JugadaTipo.SCATTER, 2));

		//jugada scatter
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0", figuras, JugadaTipo.SCATTER, 5));

		//jugada scatter
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0,0", figuras, JugadaTipo.SCATTER, 10));

		//jugada scatter
		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0,0,0", figuras, JugadaTipo.SCATTER, 50));
		*/
	}

	protected void inicializarRodillos() {

		//rodillo 1
		rodillos.add(Rodillo.configurarRodillo(1, "0,1,2,3,0,4,4,99,1000,7,7,7,7,7,99,7", figuras));

		//rodillo 2
		rodillos.add(Rodillo.configurarRodillo(2, "100,0,1,2,1000,3,99,99,0,4,4,7,1000,7,7,7", figuras));

		//rodillo 3
		rodillos.add(Rodillo.configurarRodillo(3, "100,0,99,2,2,3,3,4,1000,0,99,7,7,7,1000,7", figuras));

		//rodillo 4
		rodillos.add(Rodillo.configurarRodillo(4, "100,0,1,2,3,0,99,99,1000,4,4,7,7,7,7,1000", figuras));

		//rodillo 5
		rodillos.add(Rodillo.configurarRodillo(5, "100,0,1,1000,0,99,3,4,4,7,99,7,1000,7,7,7", figuras));

	}
}