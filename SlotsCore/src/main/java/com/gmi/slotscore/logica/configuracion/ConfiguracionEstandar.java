package com.gmi.slotscore.logica.configuracion;

import com.gmi.slotscore.constante.FiguraTipo;
import com.gmi.slotscore.dominio.Figura;
import com.gmi.slotscore.dominio.Rodillo;

/**
 * Configuracion por defecto de la maquina.
 * 
 * Las probabilidades no estan calculadas todavia, simplemente
 * se usa esta clase como prueba para la implementacion del
 * proyecto.
 * 
 * @author Pablo Caviglia
 *
 */
public class ConfiguracionEstandar extends Configuracion {

	protected void inicializarFiguras() {

		Figura figuraScatter = new Figura(0, "Scatter", FiguraTipo.SCATTER);
		Figura figuraBonus = new Figura(99, "Bonus", FiguraTipo.BONUS);
		Figura figuraWild = new Figura(100, "Wild", FiguraTipo.WILD);
		Figura figura1 = new Figura(1, "figura1", FiguraTipo.NORMAL);
		Figura figura2 = new Figura(2, "figura2", FiguraTipo.NORMAL);
		Figura figura3 = new Figura(3, "figura3", FiguraTipo.NORMAL);
		Figura figura4 = new Figura(4, "figura4", FiguraTipo.NORMAL);
		Figura figura7 = new Figura(7, "figura7", FiguraTipo.NORMAL);
		
		figuras.add(figuraScatter);
		figuras.add(figuraBonus);
		figuras.add(figuraWild);
		figuras.add(figura1);
		figuras.add(figura2);
		figuras.add(figura3);
		figuras.add(figura4);
		figuras.add(figura7);
		
	}

	protected void inicializarJugadasGanadoras() {

//		//jugada
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1", figuras, JugadaTipo.NORMAL, 100));
//		
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1,1", figuras, JugadaTipo.NORMAL, 400));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "1,1,1,1,1", figuras, JugadaTipo.NORMAL, 1000));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "2,2,2", figuras, JugadaTipo.NORMAL, 500));
//
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "2,2,2,2", figuras, JugadaTipo.NORMAL, 1000));
//
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "2,2,2,2,2", figuras, JugadaTipo.NORMAL, 5000));
//
//		
//		
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3", figuras, JugadaTipo.NORMAL, 20));
//
//		
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3,3", figuras, JugadaTipo.NORMAL, 50));
//
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "3,3,3,3,3", figuras, JugadaTipo.NORMAL, 50));
//
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4", figuras, JugadaTipo.NORMAL, 15));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4", figuras, JugadaTipo.NORMAL, 10));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "4,4,4,4,4", figuras, JugadaTipo.NORMAL, 30));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7", figuras, JugadaTipo.NORMAL, 2));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7", figuras, JugadaTipo.NORMAL, 4));
//
//		//jugada 
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "7,7,7,7,7", figuras, JugadaTipo.NORMAL, 20));
//
//		//jugada bonus
//		jugadasGanadoras.add(JugadaGanadoraBonus.configurarJugadaGanadoraBonus(maquina, "99,99,99,99", figuras, 10));
//		
//		//jugada scatter
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0", figuras, JugadaTipo.SCATTER, 5));
//
//		//jugada scatter
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0,0", figuras, JugadaTipo.SCATTER, 10));
//
//		//jugada scatter
//		jugadasGanadoras.add(JugadaGanadora.configurarJugadaGanadora(maquina, "0,0,0,0,0", figuras, JugadaTipo.SCATTER, 20));

	}

	protected void inicializarRodillos() {

		//rodillo 1
		rodillos.add(Rodillo.configurarRodillo(1, "0,1,2,3,4,4,4,4,7,7,7,7,7,7,7", figuras));

		//rodillo 2
		rodillos.add(Rodillo.configurarRodillo(2, "100,0,1,2,3,3,4,4,4,7,7,7,7,7,7", figuras));

		//rodillo 3
		rodillos.add(Rodillo.configurarRodillo(3, "100,0,1,2,3,3,4,4,4,4,7,7,7,7,7", figuras));

		//rodillo 4
		rodillos.add(Rodillo.configurarRodillo(4, "100,0,1,2,3,3,4,4,4,7,7,7,7,7,7", figuras));

		//rodillo 5
		rodillos.add(Rodillo.configurarRodillo(5, "100,0,1,2,3,3,4,4,4,4,7,7,7,7,7", figuras));

	}
}