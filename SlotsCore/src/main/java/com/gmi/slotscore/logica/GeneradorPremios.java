package com.gmi.slotscore.logica;

import com.gmi.slotscore.constante.FiguraTipo;
import com.gmi.slotscore.constante.Sistema;
import com.gmi.slotscore.dominio.Figura;
import com.gmi.slotscore.dominio.JugadaGanadora;
import com.gmi.slotscore.dominio.JugadaGanadoraJackpot;
import com.gmi.slotscore.dominio.JugadaLineaGanadora;
import com.gmi.slotscore.dominio.Linea;
import com.gmi.slotscore.dominio.Maquina;
import com.gmi.slotscore.util.SlotsRandomGenerator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Distribuye premios cancelados entre las
 * jugadas ganadoras definidas en la 
 * configuracion de la maquina. 
 * La distribucion la hace de la forma
 * mas optima en base a las probabilidades
 * de pago de cada jugada ganadora
 * 
 * @author Pablo Caviglia
 *
 */
public class GeneradorPremios {

	/**
	 * Cuando se genera un premio se guarda en
	 * esta variable toda la informacion del mismo
	 */
	public JugadaLineaGanadora premioGenerado;
	
	/**
	 * Tras generar un premio (o no), es necesario
	 * generar tambien una posicion valida en los
	 * rodillos para representar la jugada. Ya que
	 * puede llegar a ser bastante costoso (en cuanto
	 * a trabajo de procesamiento), buscar el premio
	 * exacto, lo que se hace es crear una tirada
	 * falsa, que no respeta las posiciones de las 
	 * figuras en los rodillos, de esta manera se
	 * logra entregar un premio, o cancelar otro, 
	 * con el fin de normalizar los pagos a los 
	 * valores indicados por el administrador.
	 */
	public int[][] tiradaFicticia;
	
	/**
	 * La maquina a la que se encuentra 
	 * ligado el distribuidor de premios
	 */
	private Maquina maquina;
	
	public GeneradorPremios(Maquina maquina) {
		this.maquina = maquina;
	}
	
	/**
	 * Teniendo en cuenta las lineas y el
	 * dinero apostado en la ultima apuesta,
	 * analiza si se puede pagar alguno de 
	 * los premios que tiene guardado el
	 * distribuidor de premios pronto para
	 * dar.
	 * En caso de que se haya generado algun
	 * premio, se guarda en la variable 
	 * 'premioGenerado'. Es importante que se
	 * utilice esa variable antes de llamarse 
	 * nuevamente a este metodo si no se quiere
	 * pisar el premio con uno nuevo.
	 * 
	 * @return
	 */
	public synchronized boolean generarPremio() {
		
		//jugada ganadora disponible y
		//pagable hasta el momento
		JugadaGanadora jugadaGanadoraDisponible = null;

		/**
		 * Chequeamos la creacion del premio JACKPOT
		 */
		JugadaGanadoraJackpot jugadaGanadoraJackpot = generarJackpot();

		/**
		 * Si la instancia 'jugadaGanadoraJackpot' no
		 * es nula significa que el jugador ha sacado
		 * el JACKPOT 	
		 */
		if(jugadaGanadoraJackpot != null) {
			jugadaGanadoraDisponible = jugadaGanadoraJackpot;
		}

		//generamos la tirada ficticia
		generarTiradaFicticia(jugadaGanadoraDisponible);

        boolean tienePremio;

        //seteamos el flag correspondiente para
		//que el metodo devuelva si fue creada
		//una jugada con o sin premio
		if(jugadaGanadoraDisponible != null) {
			tienePremio = true;
		}
		else {
			tienePremio = false;
		}
		
		return tienePremio;
	}
	
	/**
	 * Genera una jugada ganadora instantaneamente
	 * exclusivamente para el modo demo del juego
	 */
	public void generarJugadaGanadoraDemo() {
		//recorremos la lista de todas las
		//jugadas ganadoras del sistema y
		//elegimos una aleatoriamente
		List<JugadaGanadora> jugadasGanadorasNormal = maquina.configuracion.jugadasGanadorasNormal;
        JugadaGanadora jugadaGanadoraDisponible = jugadasGanadorasNormal.get(SlotsRandomGenerator.getInstance().nextInt(jugadasGanadorasNormal.size()));
		//generamos la tirada ficticia
		generarTiradaFicticia(jugadaGanadoraDisponible);
	}
	
	/**
	 * Evalua la posible entrega del
	 * JACKPOT en base a un analisis
	 * de la situacion estadistica de
	 * la maquina
	 * 
	 * @return
	 */
	private JugadaGanadoraJackpot generarJackpot() {
		
		JugadaGanadoraJackpot jackpot = null;
		
		//verificar si el monto minimo necesario que hay
		//en el pozo es el configurado en el sistema, menos
		//un 5% que se configura para tener un factor
		//de aleatoriedad mas interesante
		float montoMinimoJackpot = ((float)Sistema.JACKPOT_MONTO_MINIMO) - ((5f*(float)Sistema.JACKPOT_MONTO_MINIMO)/100f);
		
		//chequeamos que el monto actual
		//del jackpot sea mayor o igual al
		//monto minimo configurado
		if(maquina.logicaMaquina.montoDistribuidoJackpot >= montoMinimoJackpot) {
            jackpot = maquina.configuracion.jugadaGanadoraJackpot;
            jackpot.monto = maquina.logicaMaquina.montoDistribuidoJackpot;
		}
		
		return jackpot;
	}
	
	/**
	 * Dada una jugada ganadora por parametro genera una
	 * tirada ficticia preparada para entregar el premio
	 * de la jugada recibida por parametro. Si la jugada
	 * recibida es nula, el metodo crea una jugada ficticia
	 * sin premio.
	 * 
	 * @param jugadaGanadora
	 */
	private void generarTiradaFicticia(JugadaGanadora jugadaGanadora) {
		
		//obtenemos la cantidad de rodillos
		int cantRodillos = maquina.configuracion.getRodillos().size();
		
		//calculamos el alto (visible) de cada rodillo
		int figurasVisibleRodillo = maquina.configuracion.getLineas().get(0).getForma().length;
		
		//obtenemos la lista de todas las 
		//figuras que componen la maquina
		List<Figura> figurasMaquina = maquina.configuracion.getFiguras();

		//creamos una lista de figuras 'normales', 
		//sin figuras scatter, wild y bonus
		List<Figura> figuras = new ArrayList<Figura>();
		
		//coleccion temporal para mantener 
		//la lista de figuras no usables 
		//durante la creacion de un premio
		List<Integer> figurasNoUsables = new ArrayList<Integer>();

		//figuras que no contienen premio
		List<Figura> figurasSinPremio = new ArrayList<Figura>();

		//figuras que se pueden usar
		List<Integer> figurasUsables = new ArrayList<Integer>();

		//creamos una lista con figuras permitidas
		List<Figura> figurasPermitidas = new ArrayList<Figura>();
		
		//hacemos el filtro de figuras
		for(Figura figuraMaquinaActual : figurasMaquina) {
			if(figuraMaquinaActual.getTipo() == FiguraTipo.NORMAL || figuraMaquinaActual.getTipo() == FiguraTipo.BONUS_GAME) {
				figuras.add(figuraMaquinaActual);
			}
		}
		
		//variable util
		int cantidadFiguras = figuras.size();
		
		//constante que indica cuantas
		//figuras lindantes tiene la figura
		//de un rodillo con respecto a los
		//rodillos lindantes
		int FIGURAS_LINDANTES = 3;
		
		//contenedor de la tirada ficticia
		int[][] tirada = new int[cantRodillos][figurasVisibleRodillo];
		
		//configuramos el premio como vacio
		premioGenerado = null;
		
		//en caso de que la jugada ganadora
		//no sea nula (exista premio), guardamos
		//la forma de la linea elegida 
		int[][] formaLineaGanadora = null;
		
		/**
		 * Si existe premio, agregamos a la jugada
		 * sin premio generada lineas atras, la 
		 * jugada del premio y creamos una tirada
		 * ficticia con un premio
		 */
		if(jugadaGanadora != null) {
			
			//obtenemos cuantas lineas fueron
			//apostadas por el jugador
			int lineasApostadas = maquina.lineasApostadas;
			
			//elegimos una linea aleatoriamente
			int lineaElegida = (int)((SlotsRandomGenerator.getInstance().nextFloat() * (float)lineasApostadas) + 1f);
			
			//obtenemos la lista de lineas
			List<Linea> lineas = maquina.configuracion.getLineas();
			
			//referencia a la forma de
			//la linea elegida 
			Linea linea = null;
			
			//recorremos la lista buscando
			//el id correspondiente al valor
			//de la variable 'lineaElegida'
			for(Linea lineaActual : lineas) {
				if(lineaActual.getId() == lineaElegida) {
					formaLineaGanadora = lineaActual.getForma();
					linea = lineaActual;
					break;
				}
			}
			
			//recorremos cada rodillo
			for(int rodilloActual=0; rodilloActual<cantRodillos; rodilloActual++) {
				
				//recorremos cada figura visible del rodillo
				for(int posicionDentroRodilloActual=0; posicionDentroRodilloActual<figurasVisibleRodillo; posicionDentroRodilloActual++) {
					
					//identificamos si la posicion
					//actual posee premio en la 
					//forma de la linea
					boolean figuraPremio = formaLineaGanadora[posicionDentroRodilloActual][rodilloActual] == 1;
					
					//si el largo de las figuras
					//del premio no alcanza el largo
					//de la linea (5 generalmente) entonces
					//no es una figura con premio
					if(jugadaGanadora.figurasIds.length < rodilloActual+1) {
						figuraPremio = false;
					}
					
					//si es una posicion de premio
					//se guarda el id de la figura
					//en la posicion actual
					if(figuraPremio) {
						int idFiguraPremio = jugadaGanadora.figurasIds[rodilloActual];
						tirada[rodilloActual][posicionDentroRodilloActual] = idFiguraPremio;
					}
				}
			}
			
			//se genera el premio
			premioGenerado = new JugadaLineaGanadora();
			premioGenerado.setJugadaGanadora(jugadaGanadora);
			premioGenerado.setFiguras(jugadaGanadora.figuras);
			premioGenerado.setLinea(linea);
			premioGenerado.setTipo(jugadaGanadora.tipo);
			premioGenerado.setGenerado(true);
			
		}
		
		
		/**
		 * Generamos una jugada SIN premio.
		 * Mas adelante se chequea si se 
		 * debe de generar premio, en caso de
		 * que si haya que crear premio, a la
		 * jugada sin premio creada a continuacion
		 * se le agregan las figuras correspondientes
		 * al premio que se desee dar
		 */
		
		//indica cuantas figuras de tipo
		//bonus mayor menor fueron ya puestas
		//en la jugada sin premio generada
		byte figurasBonusGamePuestas = 0;
		
		//recorremos cada rodillo
		for(int rodilloActual=0; rodilloActual<cantRodillos; rodilloActual++) {
			
			//recorremos cada figura visible del rodillo
			for(int posicionDentroRodilloActual=0; posicionDentroRodilloActual<figurasVisibleRodillo; posicionDentroRodilloActual++) {
				
				//chequeamos (en caso de que existe premio), si
				//el mismo se encuentra en la posicion que estamos
				//recorriendo actualmente
				boolean figuraPremio = false;
				
				//si existe premio
				if(formaLineaGanadora != null) {
					figuraPremio = formaLineaGanadora[posicionDentroRodilloActual][rodilloActual] == 1;	
				}
				
				//el primer rodillo nos interesa siempre
				//y cuando haya un premio generado, de 
				//esta manera podemos detectar un caso
				//de generacion erroneo de premio
				if(rodilloActual == 0 && premioGenerado != null) {
					
					//solo si no es la posicion de 
					//la figura con premio
					if(!figuraPremio) {
						
						//rastreamos la figura con premio
						//en el primer rodillo
						int idFiguraPremio = premioGenerado.getFiguras().get(0).getId();
						
						//creamos un subconjunto de figuras
						//que no contenga el id de la figura
						//con premio
						figurasSinPremio.clear();
						
						//recorremos la lista de todas las
						//figuras que contiene la maquina
						for(Figura figuraActual : figurasMaquina) {

							//obtenemos la figura 
							//recorrida actual
							int idFiguraActual = figuraActual.getId();
							
							//si el id de figura actual es 
							//diferente al del premio, lo 
							//seleccionamos como figura para 
							//la posicion de tirada ficticia 
							//actual
							if(idFiguraPremio != idFiguraActual && 
									figuraActual.getTipo() != FiguraTipo.WILD && 
									figuraActual.getTipo() != FiguraTipo.BONUS &&
									figuraActual.getTipo() != FiguraTipo.JACKPOT &&
									figuraActual.getTipo() != FiguraTipo.SCATTER) {
								figurasSinPremio.add(figuraActual);
							}
						}
						
						//elegimos una figura aleatoria 
						//de la lista de figuras sin premio
						int posicionAleatoria = (int)(SlotsRandomGenerator.getInstance().nextFloat() * (float)figurasSinPremio.size());
						
						//guardamos el id de la figura en
						//el array de la tirada ficticia
						tirada[rodilloActual][posicionDentroRodilloActual] = figurasSinPremio.get(posicionAleatoria).getId();
						
					}
				}
				else if(rodilloActual == 1) {
					
					//El segundo rodillo nos
					//interesa debido a que los premios van
					//siempre de izquierda a derecha empezando
					//desde el primer rodillo. Si hacemos que no
					//coincida ninguna figura del primer rodillo
					//con el segundo, no va a generase ningun premio
				
					//obtenemos el primer rodillo para buscar
					//figuras que poner el segundo que sean
					//diferentes asi no se generan premios
					int[] primerRodillo = tirada[0];
					
					//lista con figuras que no deben ser usadas
					//en el segundo rodillo para no generar premios
					figurasNoUsables.clear();
					
					//obtenemos las figuras no admitidas 
					//como parte del segundo rodillo para
					//la posicion actual
					//La figura actual debe de ser diferente a las figuras del
					//primer rodillo en las posiciones figuraActual -1, 0 y +1
					for(int i=posicionDentroRodilloActual-1, j=0; j<FIGURAS_LINDANTES; i++, j++) {
						
						//si i es menor que cero significa
						//que estamos en una figura previa
						//a las visibles por ende la damos
						//por descontado porque no se evalua
						//para los premios
						if(i >= 0 && i < FIGURAS_LINDANTES) {
							figurasNoUsables.add(primerRodillo[i]);
						}
					}
					
					//una vez que tenemos una lista con
					//las figuras que no deben de ser 
					//usadas procedemos a elegir las figuras
					//que si se pueden usar
					figurasUsables.clear();
					
					for(Figura figuraActual : figuras) {
						
						//flag para indicar que la 
						//figura es usable
						boolean usable = true;
					
						//recorremos la lista de figuras no usables
						for(Integer idFiguraNoUsable : figurasNoUsables) {
							if(idFiguraNoUsable.intValue() == figuraActual.getId()) {
								
								//la figura no es usable
								usable = false;
								break;
							}
						}
						
						//si es usable la agregamos
						if(usable) {
							figurasUsables.add(figuraActual.getId());
						}
					}
					
					/**
					 * Ahora que tenemos la lista de figuras que
					 * si podemos usar, buscamos aleatoriamente en
					 * esa lista y generamos finalmente los valores
					 * del segundo rodillo 
					 */
					int cantidadFigurasUsables = figurasUsables.size();
					
					//buscamos figuras aleatorias
					int valorAleatorio = (int)(SlotsRandomGenerator.getInstance().nextFloat() * (float)cantidadFigurasUsables);
					
					//obtenemos el id de figura aleatorio 
					int figuraAleatoriaId = figurasUsables.get(valorAleatorio);
					
					/**
					 * Solo guardamos en la tirada la figura
					 * seleccionada aleatoriamente si sucede
					 * que no existe una figura con premio en
					 * la posicion actual
					 */
					if(!figuraPremio) {
						//guardamos el id de la figura en
						//el array de la tirada ficticia
						tirada[rodilloActual][posicionDentroRodilloActual] = figuraAleatoriaId;	
					}
				}
				else {
					
					/**
					 * Solo guardamos en la tirada la figura
					 * seleccionada aleatoriamente si sucede
					 * que no existe una figura con premio en
					 * la posicion actual
					 */
					if(!figuraPremio) {

						//si se genero un premio, creamos un 
						//subconjunto de figuras diferentes
						//a la del premio
						if(premioGenerado != null) {

							//creamos una lista con figuras permitidas
							figurasPermitidas.clear();
							
							//obtenemos la lista de figuras 
							//que componen el premio generado
							List<Figura> figurasPremio = premioGenerado.getFiguras();

							//recorremos todas las figuras
							for(Figura figuraActual : figuras) {
								
								boolean existe = false;
								
								//recorremos la lista de figuras 
								//del premio para removerlas de
								//la lista de jugadas permitidas
								for(Figura figuraPremioActual : figurasPremio) {
									
									//si tienen el mismo id, entonces
									//no es una figura permitida
									if(figuraActual.getId() == figuraPremioActual.getId()) {
										existe = true;
										break;
									}
								}
								
								//si no existe la figura en
								//el premio, la agregamos a
								//la lista de permitidas
								if(!existe) {
									figurasPermitidas.add(figuraActual);
								}
							}
							
							//buscamos figuras aleatorias
							int valorAleatorio = (int)(SlotsRandomGenerator.getInstance().nextFloat() * (float)figurasPermitidas.size());
							
							//obtenemos la figura aleatorio 
							Figura figuraAleatoria = figurasPermitidas.get(valorAleatorio);
							
							//guardamos el id de la figura en
							//el array de la tirada ficticia
							tirada[rodilloActual][posicionDentroRodilloActual] = figuraAleatoria.getId();

						}
						else {
							
							//buscamos figuras aleatorias
							int valorAleatorio = (int)(SlotsRandomGenerator.getInstance().nextFloat() * (float)cantidadFiguras);
							
							//obtenemos la figura aleatorio 
							Figura figuraAleatoria = figuras.get(valorAleatorio);
							
							//si la figura aleatoria es
							//de tipo bonus menor mayor,
							//debemos cerciorarnos que ya
							//no la hayamos usado demasiadas
							//veces
							if(figuraAleatoria.getTipo() == FiguraTipo.BONUS_GAME) {
								
								//increntamos las veces usada
								figurasBonusGamePuestas++;

								//no se puede usar mas de dos
								//veces en una jugada sin premio
								if(figurasBonusGamePuestas == 1) {
									Iterator<Figura> itFigurasUsables = figuras.iterator();
									while(itFigurasUsables.hasNext()) {
										Figura foo = itFigurasUsables.next();
										if(foo.getTipo() == FiguraTipo.BONUS_GAME) {
											
											//removemos la figura que
											//no queremos que aparezca 
											//mas
											itFigurasUsables.remove();
											
											//reconfiguramos el tamano
											//de la coleccion de figuras
											cantidadFiguras = figuras.size();
											
											//quitamos la figura que 
											//precisabamos y nos vamos
											break;
										}
									}
								}
							}
							
							//guardamos el id de la figura en
							//el array de la tirada ficticia
							tirada[rodilloActual][posicionDentroRodilloActual] = figuraAleatoria.getId();
						}
					}
				}
			}
		}
		
		/**
		 * Asignamos la tirada creada para
		 * que ya sea usable por otras capas
		 * del sistema
		 */
		tiradaFicticia = tirada;
	}
}