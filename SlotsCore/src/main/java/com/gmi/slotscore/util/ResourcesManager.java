package com.gmi.slotscore.util;

import com.gmi.slotscore.MainActivity;
import com.gmi.slotscore.dominio.Figura;
import com.gmi.slotscore.dominio.Linea;
import com.gmi.slotscore.dominio.util.Pair;

import org.andengine.engine.Engine;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.ITiledTextureRegion;
import org.andengine.opengl.texture.region.TextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 01/12/14.
 */
public class ResourcesManager {

    private static final ResourcesManager INSTANCE = new ResourcesManager();

    private boolean resourcesLoadedGame;
    private boolean resourcesLoadedMenu;

    public Engine engine;
    public MainActivity activity;
    public SmoothCamera camera;
    public VertexBufferObjectManager vbom;

    public Font menuFont;

    public ITextureRegion splashRegion;
    private BitmapTextureAtlas splashTextureAtlas;

    public ITextureRegion stadiumRegion;
    private BitmapTextureAtlas stadiumTextureAtlas;

    public ITextureRegion skyRegion;
    private BitmapTextureAtlas skyTextureAtlas;

    public ITextureRegion tribuna1Region;
    private BitmapTextureAtlas tribuna1TextureAtlas;

    public ITextureRegion tribuna2Region;
    private BitmapTextureAtlas tribuna2TextureAtlas;

    public ITextureRegion[] countRegions;

    public ITextureRegion[] menuPlayersRegion;
    private List<Pair<Integer, Integer>> menuPlayersDimensions;

    //net
    private BitmapTextureAtlas netTextureAtlas;
    public ITextureRegion netTextureRegion;

    //marco superior
    private BitmapTextureAtlas marcoSuperiorTextureAtlas;
    public ITextureRegion marcoSuperiorTextureRegion;

    //marco inferior
    private BitmapTextureAtlas marcoInferiorTextureAtlas;
    public ITextureRegion marcoInferiorTextureRegion;

    //bonus game
    private BitmapTextureAtlas bonusGameTextureAtlas;
    public ITextureRegion bonusGameTextureRegion;

    //figures
    private BitmapTextureAtlas[] figurasTextureAtlas;
    private ITextureRegion[] figurasTextureRegion;

    //shine
    private BitmapTextureAtlas shineTextureAtlas;
    public ITiledTextureRegion shineTextureRegion;

    //playing lines grid
    private BitmapTextureAtlas playingLinesGridTextureAtlas;
    public ITextureRegion playingLinesGridTextureRegion;

    public void loadMenuResources() {
        if(!resourcesLoadedMenu) {
            loadMenuFonts();
            loadMenuGraphics();
            loadMenuAudio();
            resourcesLoadedMenu = true;
        }
    }

    public void loadGameResources() {
        if(!resourcesLoadedGame) {
            loadGameGraphics();
            loadGameFonts();
            loadGameAudio();
            resourcesLoadedGame = true;
        }
    }

    private void loadMenuFonts() {
        ITexture bigFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        menuFont = FontFactory.createFromAsset(activity.getFontManager(), bigFontTexture, activity.getAssets(), "gfx/bigbimbo.ttf", 64, true, Color.WHITE_ARGB_PACKED_INT);
        menuFont.load();
    }

    private void loadMenuGraphics() {

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        stadiumTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 800, 584, TextureOptions.BILINEAR);
        stadiumRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(stadiumTextureAtlas, activity, "stadium.png", 0, 0);
        stadiumTextureAtlas.load();

        skyTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 800, 328, TextureOptions.BILINEAR);
        skyRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(skyTextureAtlas, activity, "sky.png", 0, 0);
        skyTextureAtlas.load();

        tribuna1TextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 800, 135, TextureOptions.BILINEAR);
        tribuna1Region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(tribuna1TextureAtlas, activity, "tribuna_1.png", 0, 0);
        tribuna1TextureAtlas.load();

        tribuna2TextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 800, 135, TextureOptions.BILINEAR);
        tribuna2Region = BitmapTextureAtlasTextureRegionFactory.createFromAsset(tribuna2TextureAtlas, activity, "tribuna_2.png", 0, 0);
        tribuna2TextureAtlas.load();

        menuPlayersDimensions = new ArrayList<Pair<Integer, Integer>>();
        menuPlayersDimensions.add(new Pair<Integer, Integer>(150, 245));
        menuPlayersDimensions.add(new Pair<Integer, Integer>(150, 245));
        menuPlayersDimensions.add(new Pair<Integer, Integer>(150, 245));
        menuPlayersDimensions.add(new Pair<Integer, Integer>(150, 225));
        menuPlayersDimensions.add(new Pair<Integer, Integer>(150, 225));
        menuPlayersDimensions.add(new Pair<Integer, Integer>(190, 260));
        menuPlayersDimensions.add(new Pair<Integer, Integer>(150, 185));

        menuPlayersRegion = new ITextureRegion[menuPlayersDimensions.size()];
        int menuPlayerCounter = 0;
        for(Pair<Integer,Integer> menuPlayerDimension : menuPlayersDimensions) {
            BitmapTextureAtlas menuPlayerTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), menuPlayerDimension.value1, menuPlayerDimension.value2, TextureOptions.BILINEAR);
            TextureRegion menuPlayerRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(menuPlayerTextureAtlas, activity, "INTROfig-" + (menuPlayerCounter+1) + ".png", 0, 0);
            menuPlayerTextureAtlas.load();
            menuPlayersRegion[menuPlayerCounter] = menuPlayerRegion;
            menuPlayerCounter++;
        }

        countRegions = new ITextureRegion[3];
        for(int i=0; i<3; i++) {
            BitmapTextureAtlas countTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 200, 276, TextureOptions.BILINEAR);
            TextureRegion countRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(countTextureAtlas, activity, "count" + (i+1) + ".png", 0, 0);
            countTextureAtlas.load();
            countRegions[i] = countRegion;
        }
    }

    private void loadMenuAudio() {

    }

    private BitmapTextureAtlas popupBackTextureAtlas;
    public ITextureRegion popupBackTextureRegion;

    private BitmapTextureAtlas freeRollBackTextureAtlas;
    public ITextureRegion freeRollBackTextureRegion;

    private BitmapTextureAtlas loadingIconTextureAtlas;
    public ITextureRegion loadingIconTextureRegion;

    //fonts
    public Font bigFont;
    public Font prizeFont;

    private void loadGameGraphics() {

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");

        //prize background
        popupBackTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 180, 270, TextureOptions.REPEATING_BILINEAR);
        popupBackTextureAtlas.load();
        popupBackTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(popupBackTextureAtlas, activity, "popup_back.jpg", 0, 0);
        popupBackTextureRegion.setTextureWidth(300);
        popupBackTextureRegion.setTextureHeight(140);

        //free roll
        freeRollBackTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 180, 270, TextureOptions.REPEATING_BILINEAR);
        freeRollBackTextureAtlas.load();
        freeRollBackTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(freeRollBackTextureAtlas, activity, "popup_back.jpg", 0, 0);
        freeRollBackTextureRegion.setTextureWidth(740);
        freeRollBackTextureRegion.setTextureHeight(140);

        //loading icon
        loadingIconTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 128, 128, TextureOptions.BILINEAR);
        loadingIconTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(loadingIconTextureAtlas, activity, "loading_icon.png", 0, 0);
        loadingIconTextureAtlas.load();

        //sky
        skyTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 640, 480, TextureOptions.BILINEAR);
        skyRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(skyTextureAtlas, activity, "game_BG_sky.jpg", 0, 0);
        skyTextureAtlas.load();

        //net
        netTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 800, 600, TextureOptions.BILINEAR);
        netTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(netTextureAtlas, activity, "game_BG_net.png", 0, 0);
        netTextureAtlas.load();

        //marco superior
        marcoSuperiorTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 800, 126, TextureOptions.BILINEAR);
        marcoSuperiorTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(marcoSuperiorTextureAtlas, activity, "marco-14.png", 0, 0);
        marcoSuperiorTextureAtlas.load();

        //marco inferior
        marcoInferiorTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 800, 82, TextureOptions.BILINEAR);
        marcoInferiorTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(marcoInferiorTextureAtlas, activity, "marco-15.png", 0, 0);
        marcoInferiorTextureAtlas.load();

        //bonus game
        bonusGameTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 364, 246, TextureOptions.BILINEAR);
        bonusGameTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(bonusGameTextureAtlas, activity, "bonus_game.png", 0, 0);
        bonusGameTextureAtlas.load();

        //shine
        shineTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 2015, 155, TextureOptions.BILINEAR);
        shineTextureRegion = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(shineTextureAtlas, activity, "shine_tile.png", 0, 0, 13, 1);
        shineTextureAtlas.load();

        //figures
        int FIGURES_QNTY = activity.maquina.configuracion.getFiguras().size();
        figurasTextureAtlas = new BitmapTextureAtlas[FIGURES_QNTY];
        figurasTextureRegion = new ITextureRegion[FIGURES_QNTY];
        List<Figura> figuras = activity.maquina.configuracion.getFiguras();
        int i = 0;
        for(Figura figura : figuras) {
            figurasTextureAtlas[i] = new BitmapTextureAtlas(activity.getTextureManager(), 155, 155, TextureOptions.BILINEAR);
            figurasTextureRegion[i] = BitmapTextureAtlasTextureRegionFactory.createFromAsset(figurasTextureAtlas[i], activity, "figuras_" + figura.getId() + ".png", 0, 0);
            figurasTextureAtlas[i].load();
            figura.setTextureRegion(figurasTextureRegion[i]);
            i++;
        }

        List<Pair<Integer,Integer>> playingLinesDimensions = new ArrayList<Pair<Integer,Integer>>();
        playingLinesDimensions.add(new Pair(1024, 42));
        playingLinesDimensions.add(new Pair(1024, 43));
        playingLinesDimensions.add(new Pair(1024, 42));
        playingLinesDimensions.add(new Pair(1024, 442));
        playingLinesDimensions.add(new Pair(1024, 441));
        playingLinesDimensions.add(new Pair(1024, 133));
        playingLinesDimensions.add(new Pair(1024, 129));
        playingLinesDimensions.add(new Pair(1024, 146));
        playingLinesDimensions.add(new Pair(1024, 148));
        playingLinesDimensions.add(new Pair(1024, 299));

        //playing lines
        int PLAYING_LINES_QNTY = activity.maquina.configuracion.getLineas().size();
        //lines
        BitmapTextureAtlas[] playingLinesTextureAtlas = new BitmapTextureAtlas[PLAYING_LINES_QNTY];
        ITextureRegion[] playingLinesTextureRegion = new ITextureRegion[PLAYING_LINES_QNTY];
        //circles
        BitmapTextureAtlas[] playingLinesCirclesTextureAtlas = new BitmapTextureAtlas[PLAYING_LINES_QNTY];
        ITextureRegion[] playingLinesCirclesTextureRegion = new ITextureRegion[PLAYING_LINES_QNTY];

        List<Linea> lineas = activity.maquina.configuracion.getLineas();
        int j = 0;
        for(Linea linea : lineas) {

            //lines
            Pair<Integer, Integer> playingLineDimension = playingLinesDimensions.get(j);
            playingLinesTextureAtlas[j] = new BitmapTextureAtlas(activity.getTextureManager(), playingLineDimension.value1, playingLineDimension.value2, TextureOptions.BILINEAR);
            playingLinesTextureRegion[j] = BitmapTextureAtlasTextureRegionFactory.createFromAsset(playingLinesTextureAtlas[j], activity, "line_" + linea.getId() + ".png", 0, 0);
            playingLinesTextureAtlas[j].load();

            //circles
            playingLinesCirclesTextureAtlas[j] = new BitmapTextureAtlas(activity.getTextureManager(), 42, 42, TextureOptions.BILINEAR);
            playingLinesCirclesTextureRegion[j] = BitmapTextureAtlasTextureRegionFactory.createFromAsset(playingLinesCirclesTextureAtlas[j], activity, "circle_" + linea.getId() + ".png", 0, 0);
            playingLinesCirclesTextureAtlas[j].load();

            //set textures
            linea.setLineTextureRegion(playingLinesTextureRegion[j]);
            linea.setCircleTextureRegion(playingLinesCirclesTextureRegion[j]);

            //increment counter
            j++;
        }

        //playing lines grid
        playingLinesGridTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 66, 474, TextureOptions.BILINEAR);
        playingLinesGridTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(playingLinesGridTextureAtlas, activity, "lines-num.png", 0, 0);
        playingLinesGridTextureAtlas.load();

    }

    private void loadGameFonts() {

        //prize font
        ITexture prizeFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 384, 384, TextureOptions.BILINEAR);
        prizeFont = FontFactory.createFromAsset(activity.getFontManager(), prizeFontTexture, activity.getAssets(), "gfx/bigbimbo.ttf", 100, true, Color.YELLOW_ARGB_PACKED_INT);
        prizeFont.load();

        //big font
        ITexture bigFontTexture = new BitmapTextureAtlas(activity.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        bigFont = FontFactory.createFromAsset(activity.getFontManager(), bigFontTexture, activity.getAssets(), "gfx/bigbimbo.ttf", 48, true, Color.WHITE_ARGB_PACKED_INT);
        bigFont.load();




    }

    private void loadGameAudio() {

    }

    public void loadSplashScreen() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        splashTextureAtlas = new BitmapTextureAtlas(activity.getTextureManager(), 715, 266, TextureOptions.BILINEAR);
        splashRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(splashTextureAtlas, activity, "splash.png", 0, 0);
        splashTextureAtlas.load();
    }

    /**
     * @param engine
     * @param activity
     * @param camera
     * @param vbom     <br><br>
     *                 We use this method at beginning of game loading, to prepare Resources Manager properly,
     *                 setting all needed parameters, so we can latter access them from different classes (eg. scenes)
     */
    public static void prepareManager(Engine engine, MainActivity activity, SmoothCamera camera, VertexBufferObjectManager vbom) {
        getInstance().engine = engine;
        getInstance().activity = activity;
        getInstance().camera = camera;
        getInstance().vbom = vbom;
    }

    public static ResourcesManager getInstance() {
        return INSTANCE;
    }
}