package com.gmi.slotscore.util;

import java.text.DecimalFormat;

public class MathUtil {

	private static DecimalFormat twoDForm = new DecimalFormat("#.##");
	
	public static double roundTwoDecimals(double d) {
		return Double.valueOf(twoDForm.format(d).replaceAll(",", "."));
	}
}