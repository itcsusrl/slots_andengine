package com.gmi.slotscore.util;

/**
 * Created by pablo on 18/11/14.
 */
public class ThreadUtil {

    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
