package com.gmi.slotscore.util;

import com.gmi.slotscore.dominio.Maquina;

public class MaquinaPoolManager {

	/**
	 * Instancia singleton de la maquina con
	 * la que se juego por dinero real
	 */
	private static Maquina maquinaJuego;
	
	/**
	 * Instancia singleton de la maquina
	 * usada con fines de testeos
	 */
	private static Maquina maquinaTest;
	
	public static Maquina obtenerInstanciaMaquinaJuego() {
		return maquinaJuego;
	}

	public static Maquina obtenerInstanciaMaquinaJuego(Class configuracionClass) {
		if(maquinaJuego == null) {
			maquinaJuego = new Maquina(configuracionClass);
		}
		return maquinaJuego;
	}
	
	public static Maquina obtenerInstanciaMaquinaTest(Class configuracionClass) {
		if(maquinaTest == null) {
			maquinaTest = new Maquina(configuracionClass);
		}
		return maquinaTest;
	}
}