package com.gmi.slotscore.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import com.gmi.slotscore.excepcion.SystemException;

public class SlotsRandomGenerator {

	private static SecureRandom secureRandom;

	public static SecureRandom getInstance() {
		
		if(secureRandom == null) {
			try {
				secureRandom = SecureRandom.getInstance("SHA1PRNG");
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				throw new SystemException("Problema instanciando generador aleatorio SHA1PRNG");
			}
		}
		
		return secureRandom;
	}
	
	/**
	 * En base a un porcentaje recibido de 1 a 100,
	 * se calcula aleatoriamente otro nuevo valor de
	 * 1 a 100. Si el numero generado aleatoriamente 
	 * es menor al recibido, entonces se devuelve
	 * verdadero, en caso contrario falso.
	 * 
	 * @param porcentaje
	 * @return
	 */
	public static boolean ejecutarPorcentajeEntrega(int porcentaje) {
		
		boolean entrega = false;
		
		//generamos un numero 
		//aleatoriamente
		int numeroAleatorio = (int)(SlotsRandomGenerator.getInstance().nextFloat() * 100);
		
		//si el numero generado es menor al
		//porcentaje de entrega de premios 
		//consideramos que es un caso exitoso
		if(numeroAleatorio < porcentaje) {
			entrega = true;
		}
		
		return entrega;
	}
	
}
