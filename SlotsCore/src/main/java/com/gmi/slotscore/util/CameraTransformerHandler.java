package com.gmi.slotscore.util;

import com.gmi.slotscore.MainActivity;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.handler.IUpdateHandler;


public class CameraTransformerHandler implements IUpdateHandler {
	
	private SmoothCamera camera;
	
	private boolean animating;
	private float animateDuration;
	private float animateOrigX;
	private float animateOrigY;
	private float animateDestX;
	private float animateDestY;
	private float animateOrigZoom;
	private float animateDestZoom;
	
	private float currentAnimateTime;
	private CameraTransformerListener listener;
    private Runnable runnablePre;
    private Runnable runnablePost;

    public CameraTransformerHandler(SmoothCamera camera) {
        this.camera = camera;
    }

    @Override
    public void onUpdate(float pSecondsElapsed) {
		
		if(animating) {
			
			//tiempo transcurrido de la animacion
			currentAnimateTime += pSecondsElapsed;
			
			//diferencia en px de cada eje
			float difX = animateDestX - animateOrigX;
			float difY = animateDestY - animateOrigY;
			float difZoom = animateDestZoom - animateOrigZoom;
			
			float currX = ((currentAnimateTime * difX) / animateDuration) + animateOrigX;
			float currY = ((currentAnimateTime * difY) / animateDuration) + animateOrigY;
			float currZoom = ((currentAnimateTime * difZoom) / animateDuration) + animateOrigZoom;
			
			camera.setCenterDirect(currX, currY);
			camera.setZoomFactorDirect(currZoom);
			
			if(currentAnimateTime >= animateDuration) {
				
				//reseteamos variables de animacion
				animating = false;
				currentAnimateTime = 0;
				
				camera.setCenterDirect(animateDestX, animateDestY);
				camera.setZoomFactorDirect(animateDestZoom);
				
				//llamamos al listener
				if(listener != null) {
					listener.cameraAnimationFinished(runnablePost);
				}
			}
		}
	}

    @Override
    public void reset() {

    }

    public void animateMoveAndZoom(float duration, float destX, float destY, float zoom) {
		animateDuration = duration;
		animateOrigX = camera.getCenterX();
		animateDestX = destX;
		animateOrigY = camera.getCenterY();
		animateDestY = destY;
		animateOrigZoom = camera.getZoomFactor();
		animateDestZoom = zoom;
		animating = true;
		if(listener != null) {
			listener.cameraAnimationStarted(runnablePre);
		}
	}

    public void animateGotoFrame(CameraTransformerListener listener, float duration, float frameX, float frameY, float frameWidth, float frameHeight) {
        animateGotoFrame(listener, duration, frameX, frameY, frameWidth, frameHeight, null, null);
    }

	public void animateGotoFrame(CameraTransformerListener listener, float duration, float frameX, float frameY, float frameWidth, float frameHeight, Runnable runnablePre, Runnable runnablePost) {
		
		this.listener = listener;
        this.runnablePre = runnablePre;
        this.runnablePost = runnablePost;

        float screenWidth = MainActivity.CAMERA_WIDTH;
        float screenHeight = MainActivity.CAMERA_HEIGHT;

        //nos quedamos con el zoom mas
		//chico entre el horizontal y el
		//vertical
		float zoomW = screenWidth / frameWidth;
		float zoomH = screenHeight / frameHeight;
		float zoom = zoomW >= zoomH ? zoomH : zoomW;
		
		//los destinos x,y corresponden a las coordenadas
		//del frame, mas la mitad del ancho/alto del mismo
		float destX = frameX + (frameWidth / 2f);
		float destY = frameY + (frameHeight / 2f);
		
		//ejecutamos la animacion
		animateMoveAndZoom(duration, destX, destY, zoom);
		
	}
	
	public void gotoFrame(float frameX, float frameY, float frameWidth, float frameHeight) {
		
		float screenWidth = MainActivity.CAMERA_WIDTH;
		float screenHeight = MainActivity.CAMERA_HEIGHT;
		
		//nos quedamos con el zoom mas
		//chico entre el horizontal y el
		//vertical
		float zoomW = screenWidth / frameWidth;
		float zoomH = screenHeight / frameHeight;
		float zoom = zoomW >= zoomH ? zoomH : zoomW;
		
		//los destinos x,y corresponden a las coordenadas
		//del frame, mas la mitad del ancho/alto del mismo
		float destX = frameX + (frameWidth / 2f);
		float destY = frameY + (frameHeight / 2f);
		
		camera.setCenterDirect(destX, destY);
		camera.setZoomFactorDirect(zoom);
		
	}

	public SmoothCamera getCamera() {
		return camera;
	}

	public void setCamera(SmoothCamera camera) {
		this.camera = camera;
	}

    public interface CameraTransformerListener {
        public void cameraAnimationStarted(Runnable runnable);
        public void cameraAnimationFinished(Runnable runnable);
    }
}