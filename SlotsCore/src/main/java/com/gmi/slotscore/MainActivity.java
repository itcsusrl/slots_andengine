package com.gmi.slotscore;

import android.view.KeyEvent;

import com.gmi.slotscore.constante.Sistema;
import com.gmi.slotscore.dominio.Figura;
import com.gmi.slotscore.dominio.Maquina;
import com.gmi.slotscore.logica.LogicaMaquina;
import com.gmi.slotscore.persistencia.DBManager;
import com.gmi.slotscore.ui.SceneManager;
import com.gmi.slotscore.util.MaquinaPoolManager;
import com.gmi.slotscore.util.ResourcesManager;

import org.andengine.engine.Engine;
import org.andengine.engine.LimitedFPSEngine;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.CroppedResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

import java.io.IOException;
import java.util.List;

/**
 * Created by pablo on 01/12/14.
 */
public class MainActivity extends BaseGameActivity {

    public static final float CAMERA_WIDTH = 1024;
    public static final float CAMERA_HEIGHT = 768;
    public static final float CAMERA_FULL_ZOOM = 1f;

    private SmoothCamera camera;
    private ResourcesManager resourcesManager;

    public static DBManager dbManager;
    public static Maquina maquina;
    public static LogicaMaquina logicaMaquina;


    @Override
    public Engine onCreateEngine(EngineOptions pEngineOptions) {
        return new LimitedFPSEngine(pEngineOptions, 60);
    }

    public EngineOptions onCreateEngineOptions() {
        camera = new SmoothCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT, 10, 10, 1.0f);
        return new EngineOptions(true, ScreenOrientation.LANDSCAPE_SENSOR, new CroppedResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.camera);
    }

    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws IOException {

        //init the database
        dbManager = new DBManager(getApplicationContext());

        //inicializamos la logica
        //de la maquina
        inicializarMaquina();

        //TODO remove me
        maquina.montoJugador = 100;

        //init the resource manager
        ResourcesManager.prepareManager(mEngine, this, camera, getVertexBufferObjectManager());
        resourcesManager = ResourcesManager.getInstance();
        pOnCreateResourcesCallback.onCreateResourcesFinished();

    }

    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws IOException {
        SceneManager.getInstance().createSplashScene(pOnCreateSceneCallback);
//        SceneManager.getInstance().loadGameScene(resourcesManager.engine);
    }

    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws IOException {
        mEngine.registerUpdateHandler(new TimerHandler(2f, new ITimerCallback() {
            public void onTimePassed(final TimerHandler pTimerHandler) {
                mEngine.unregisterUpdateHandler(pTimerHandler);
                SceneManager.getInstance().createMenuScene();
            }
        }));
        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
        }
        return false;
    }

    private void inicializarMaquina() {

        //create a machine
        maquina = MaquinaPoolManager.obtenerInstanciaMaquinaJuego(Sistema.configuracionMaquina);
        logicaMaquina = maquina.logicaMaquina;

        //inicia un nuevo resumen
//        resetStatistics();
//        resetPots();

        maquina.montoJugador = 1000;
        maquina.lineasApostadas = 10;
        maquina.dineroApostadoPorLinea = 1;

//        for(int i=0; i<55000; i++) {
//            maquina.logicaMaquina.apostarSlots();
//        }
    }

    public Figura getFigureById(int id) {
        List<Figura> figuras = maquina.configuracion.getFiguras();
        for(Figura figura : figuras) {
            if(figura.getId() == id) {
                return figura;
            }
        }
        return null;
    }

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        System.exit(0);
//    }
}