package com.gmi.slotscore.persistencia.dao;

import android.database.sqlite.SQLiteDatabase;


public abstract class GenericDAO {

	public String TABLA_NOMBRE;
	
	public abstract void inicializar();
	public abstract void crearTabla(SQLiteDatabase db);
	public abstract void crearData(SQLiteDatabase db);

	public GenericDAO() {
		inicializar();
	}
}