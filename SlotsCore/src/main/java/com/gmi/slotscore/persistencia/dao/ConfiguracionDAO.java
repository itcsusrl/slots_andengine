package com.gmi.slotscore.persistencia.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gmi.slotscore.constante.Sistema;
import com.gmi.slotscore.persistencia.DBManager;

import java.util.HashMap;
import java.util.Map;

public class ConfiguracionDAO extends GenericDAO {

	/**
	 * DEFINICION DE LAS CONSTANTES DE CONFIGURACION
	 * PARA SER USADAS LUEGO DURANTE LA ACTUALIZACION
	 * O LECTURA DE CAMPOS INDEPENDIENTES
	 */
	public static final String APUESTA_ADMITIDA = "APUESTA_ADMITIDA";
	public static final String INCREMENTO_APUESTA = "INCREMENTO_APUESTA";
	public static final String APUESTA_MAXIMA_POR_LINEA = "APUESTA_MAXIMA_POR_LINEA";
	public static final String FACTOR_CONVERSION_MONEDA = "FACTOR_CONVERSION_MONEDA";
	public static final String MONTO_APUESTA_GRATUITA = "MONTO_APUESTA_GRATUITA";
	public static final String CANTIDAD_LINEAS_APUESTA_GRATUITA = "CANTIDAD_LINEAS_APUESTA_GRATUITA";

	public static final String APUESTA_WILD = "APUESTA_WILD";

	public static final String AUDIO = "AUDIO";
	
	public static final String JACKPOT_MONTO_MINIMO = "JACKPOT_MONTO_MINIMO";
	public static final String JACKPOT_PROBABILIDADES_JUGADOR = "JACKPOT_PROBABILIDADES_JUGADOR";
	public static final String JACKPOT_MONTO_ACTUAL = "JACKPOT_MONTO_ACTUAL";
	public static final String JACKPOT_APERTURA_POZO = "JACKPOT_APERTURA_POZO";
	
	/**
	 * El monto del jugador se respalda 
	 * ante cualquier inconveniente, de
	 * esta manera cuando se reinicia la
	 * maquina mantenemos un registro del
	 * dinero que habia en la misma antes
	 * de resetearses
	 */
	public static final String MONTO_JUGADOR = "MONTO_JUGADOR";
	
	/**
	 * Monto de pago excedido. Este
	 * valor se actualiza cada vez
	 * que la maquina se ve excecida
	 * en su capacidad de pago debido
	 * a un premio de gran volumen, o
	 * porque la misma se estaba 
	 * quedando sin monedas
	 */
	public static final String CAPACIDAD_PAGO_EXCEDIDA = "CAPACIDAD_PAGO_EXCEDIDA";
	
	public ConfiguracionDAO() {
		
	}
	
	public void inicializar() {
		TABLA_NOMBRE = "configuracion";
	}

	public void crearTabla(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLA_NOMBRE + " " +
                "(codigo VARCHAR(100), valor VARCHAR(100), nombre VARCHAR(100), descripcion VARCHAR(1000)," +
                " PRIMARY KEY (codigo))");
	}

	/**
	 * Dado un codigo de configuracion, actualiza su
	 * valor por uno nuevo recibido tambien por 
	 * parametro
	 * @param codigo
	 */
	public void actualizarValor(String codigo, String nuevoValor) {
		String sql = "UPDATE " + TABLA_NOMBRE + " " +
		 			 "SET " +
		 			 "valor = '" + nuevoValor + "' " +
		 			 "WHERE codigo='" + codigo + "'";
        DBManager.db.execSQL(sql);
	}
	
	public String obtenerValor(String codigo) {
		String valor = null;
		String sql = "SELECT valor FROM " + TABLA_NOMBRE + " WHERE codigo='" + codigo + "'";
        Cursor cursor = DBManager.db.rawQuery(sql, null);
        while(cursor.moveToNext()) {
            valor = cursor.getString(cursor.getColumnIndex("valor"));
        }
		return valor;
	}
	
	public String obtenerDescripcion(String codigo) {
        String descripcion = null;
        String sql = "SELECT descripcion FROM " + TABLA_NOMBRE + " WHERE codigo='" + codigo + "'";
        Cursor cursor = DBManager.db.rawQuery(sql, null);
        while(cursor.moveToNext()) {
            descripcion = cursor.getString(cursor.getColumnIndex("descripcion"));
        }
        return descripcion;
	}
	
	public Map<String, String> obtenerValores() {
		
		Map<String, String> valores = new HashMap<String, String>();
		String sql = "SELECT codigo, valor FROM " + TABLA_NOMBRE;
        Cursor cursor = DBManager.db.rawQuery(sql, null);

        while(cursor.moveToNext()) {
            String codigo = cursor.getString(cursor.getColumnIndex("codigo"));
			String valor = cursor.getString(cursor.getColumnIndex("valor"));
			valores.put(codigo, valor);
        }

		return valores;
	}
	
	public void crearData(SQLiteDatabase db) {

		String sql = "INSERT INTO " + TABLA_NOMBRE + "('codigo', 'valor', 'nombre', 'descripcion') " +
					 "VALUES ";
		
		//CONFIGURACION GENERAL
        db.execSQL(sql + "('" + APUESTA_ADMITIDA + "', '" + Sistema.APUESTA_ADMITIDA + "', 'Apuesta Admitida', 'Monto real de incremento tras el ingreso de dinero a la maquina.')");

        db.execSQL(sql + "('" + INCREMENTO_APUESTA + "', '" + Sistema.INCREMENTO_APUESTA + "', 'Incremento Apuesta', 'Cada vez que el monto de la apuesta es incrementado, el valor de incremento es obtenido de aqui')");
        db.execSQL(sql + "('" + APUESTA_MAXIMA_POR_LINEA + "', '" + Sistema.APUESTA_MAXIMA_POR_LINEA + "', 'Apuesta maxima por linea', 'El monto maximo apostable por linea')");
        db.execSQL(sql + "('" + FACTOR_CONVERSION_MONEDA + "', '" + Sistema.FACTOR_CONVERSION_MONEDA + "', 'Factor conversion moneda', 'Factor multiplicador o divisor de moneda. Usado para modificar el tipo de moneda en juego y usar valores mas altos o mas bajos dependiendo de lo deseado')");
        db.execSQL(sql + "('" + APUESTA_WILD + "', '" + Sistema.APUESTA_WILD + "', 'Apuesta Wild', 'Si se encuentra habilitado indica que se evaluaran jugadas conformadas totalmente por figuras wild. El valor del premio para esta jugada es el mismo que el de la jugada de mayor pago del juego.')");
        db.execSQL(sql + "('" + MONTO_APUESTA_GRATUITA + "', '" + Sistema.MONTO_APUESTA_GRATUITA + "', 'Monto Apuesta Gratuita', 'El monto apostado durante la ejecucion de una tirada gratuita generada por el bonus')");
        db.execSQL(sql + "('" + CANTIDAD_LINEAS_APUESTA_GRATUITA + "', '" + Sistema.CANTIDAD_LINEAS_APUESTA_GRATUITA + "', 'Cantidad Lineas Apuesta Gratuita', 'Las lineas apostadas durante la ejecucion de una tirada gratuita generada por el bonus')");
        db.execSQL(sql + "('" + AUDIO + "', '" + Sistema.AUDIO_FX_ENABLED + "', 'Audio', 'Estado del sistema de audio de la maquina (Activado o desactivado)')");

		//JUGADOR
        db.execSQL(sql + "('" + MONTO_JUGADOR + "', '0', 'Monto del Jugador', 'Monto del jugador. Usado en caso de que se reinicie la maquina por alguna razon inesperada')");
        db.execSQL(sql + "('" + CAPACIDAD_PAGO_EXCEDIDA + "', '0', 'Capacidad de pago de la maquina excedida', 'Cuando se exige que la maquina pague y la misma no tiene mas credito, se guarda en este registro la informacion del monto adeudado hacia el jugador.')");
		
		//CONFIGURACION JACKPOT
        db.execSQL(sql + "('" + JACKPOT_MONTO_MINIMO + "', '" + Sistema.JACKPOT_MONTO_MINIMO + "', 'Monto Minimo', 'El monto al cual se debe llegar antes de que se entregue el pozo')");
        db.execSQL(sql + "('" + JACKPOT_MONTO_ACTUAL + "', '" + 0 + "', 'Monto Actual Jackpot','El monto actual del pozo acumulado')");

	}
}