package com.gmi.slotscore.persistencia.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.gmi.slotscore.persistencia.DBManager;

import java.util.HashMap;
import java.util.Map;

public class EstadisticaDAO extends GenericDAO {

	/**
	 * DEFINICION DE LAS CONSTANTES DE CONFIGURACION
	 * PARA SER USADAS LUEGO DURANTE LA ACTUALIZACION
	 * O LECTURA DE CAMPOS INDEPENDIENTES
	 */
	public static final String DINERO_APOSTADO = "DINERO_APOSTADO";
	public static final String DINERO_GANADO = "DINERO_GANADO";
	public static final String DINERO_GANADO_NORMAL = "DINERO_GANADO_NORMAL";
	public static final String DINERO_GANADO_SCATTER = "DINERO_GANADO_SCATTER";
	public static final String DINERO_GANADO_BONUS = "DINERO_GANADO_BONUS";
	public static final String DINERO_GANADO_JACKPOT = "DINERO_GANADO_JACKPOT";
	public static final String CANTIDAD_APUESTAS = "CANTIDAD_APUESTAS";
	public static final String CANTIDAD_APUESTAS_GANADAS = "CANTIDAD_APUESTAS_GANADAS";
	public static final String CANTIDAD_APUESTAS_GANADAS_BONUS = "CANTIDAD_APUESTAS_GANADAS_BONUS";
	
	public EstadisticaDAO() {
		
	}
	
	public void inicializar() {
		TABLA_NOMBRE = "estadistica";
	}

	public void crearTabla(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLA_NOMBRE + " " +
                "(DINERO_APOSTADO FLOAT NOT NULL, " +
                "DINERO_GANADO FLOAT NOT NULL, " +
                "DINERO_GANADO_NORMAL FLOAT NOT NULL, " +
                "DINERO_GANADO_SCATTER FLOAT NOT NULL, " +
                "DINERO_GANADO_BONUS FLOAT NOT NULL, " +
                "DINERO_GANADO_JACKPOT FLOAT NOT NULL, " +
                "CANTIDAD_APUESTAS FLOAT NOT NULL, " +
                "CANTIDAD_APUESTAS_GANADAS FLOAT NOT NULL, " +
                "CANTIDAD_APUESTAS_GANADAS_BONUS FLOAT NOT NULL)");
	}

	/**
	 * Dado un codigo de configuracion, actualiza su
	 * valor por uno nuevo recibido tambien por 
	 * parametro
	 * @throws java.sql.SQLException
	 */
	public void actualizarValor(String nombre, float nuevoValor) {
	
		String sql = "UPDATE " + TABLA_NOMBRE + " " +
		 			 "SET " +
		 			 nombre + " = " + nuevoValor;

        DBManager.db.execSQL(sql);
	}
	
	public void actualizarValoresEstadisticos(float dineroApostado, float dineroGanado, float dineroGanadoNormal, float dineroGanadoScatter, float dineroGanadoBonus, float dineroGanadoJackpot, long cantidadApuestas, long cantidadApuestasGanadas, long cantidadApuestasBonus) {

		String sql = "UPDATE " + TABLA_NOMBRE + " " +
		 "SET " +
		 "DINERO_APOSTADO = " + dineroApostado + ", " +
		 "DINERO_GANADO = " + dineroGanado + ", " +
		 "DINERO_GANADO_NORMAL = " + dineroGanadoNormal + ", " +
		 "DINERO_GANADO_SCATTER = " + dineroGanadoScatter + ", " +
		 "DINERO_GANADO_BONUS = " + dineroGanadoBonus + ", " +
		 "DINERO_GANADO_JACKPOT = " + dineroGanadoJackpot + ", " +
		 "CANTIDAD_APUESTAS = " + cantidadApuestas + ", " +
		 "CANTIDAD_APUESTAS_GANADAS = " + cantidadApuestasGanadas + ", " +
		 "CANTIDAD_APUESTAS_GANADAS_BONUS = " + cantidadApuestasBonus;

        DBManager.db.execSQL(sql);

	}
	
	
	public float obtenerValor(String nombre) {
		
		String sql = "SELECT " + nombre + " FROM " + TABLA_NOMBRE;

        Cursor cursor = DBManager.db.rawQuery(sql, null);
        cursor.moveToNext();

        float valor = cursor.getFloat(cursor.getColumnIndex("DINERO_APOSTADO"));
        return valor;
	}

	public Map<String, Float> obtenerValores() {
		
		Map<String, Float> valores = new HashMap<String, Float>();

        String sql = "SELECT * FROM " + TABLA_NOMBRE;
        Cursor cursor = DBManager.db.rawQuery(sql, null);
        cursor.moveToNext();

		//cargamos valores
		valores.put(DINERO_APOSTADO, cursor.getFloat(cursor.getColumnIndex("DINERO_APOSTADO")));
		valores.put(DINERO_GANADO, cursor.getFloat(cursor.getColumnIndex("DINERO_GANADO")));
		valores.put(DINERO_GANADO_NORMAL, cursor.getFloat(cursor.getColumnIndex("DINERO_GANADO_NORMAL")));
		valores.put(DINERO_GANADO_SCATTER, cursor.getFloat(cursor.getColumnIndex("DINERO_GANADO_SCATTER")));
		valores.put(DINERO_GANADO_BONUS, cursor.getFloat(cursor.getColumnIndex("DINERO_GANADO_BONUS")));
		valores.put(DINERO_GANADO_JACKPOT, cursor.getFloat(cursor.getColumnIndex("DINERO_GANADO_JACKPOT")));
		valores.put(CANTIDAD_APUESTAS, cursor.getFloat(cursor.getColumnIndex("CANTIDAD_APUESTAS")));
		valores.put(CANTIDAD_APUESTAS_GANADAS, cursor.getFloat(cursor.getColumnIndex("CANTIDAD_APUESTAS_GANADAS")));
		valores.put(CANTIDAD_APUESTAS_GANADAS_BONUS, cursor.getFloat(cursor.getColumnIndex("CANTIDAD_APUESTAS_GANADAS_BONUS")));

		return valores;
	}
	
	public void crearData(SQLiteDatabase db) {
		String sql = "INSERT INTO " + TABLA_NOMBRE +
					 "(DINERO_APOSTADO, DINERO_GANADO, DINERO_GANADO_NORMAL, DINERO_GANADO_SCATTER, DINERO_GANADO_BONUS, DINERO_GANADO_JACKPOT, CANTIDAD_APUESTAS, CANTIDAD_APUESTAS_GANADAS, CANTIDAD_APUESTAS_GANADAS_BONUS) " +
					 "VALUES (0,0,0,0,0,0,0,0,0)";
        db.execSQL(sql);
	}
}