package com.gmi.slotscore.persistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.gmi.slotscore.persistencia.dao.ConfiguracionDAO;
import com.gmi.slotscore.persistencia.dao.EstadisticaDAO;
import com.gmi.slotscore.persistencia.dao.GenericDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 10/09/14.
 */
public class DBManager {

    private static List<GenericDAO> daos = new ArrayList<GenericDAO>();
    private static final int DATABASE_VERSION = 10;
    public static SQLiteDatabase db = null;

    public DBManager(Context context) {
        //init daos
        daos.add(new ConfiguracionDAO());
        daos.add(new EstadisticaDAO());
        //init db
        SQLiteOpenHelper o = new PersistenceOpenHelper(context, "slots" + context.getPackageName() + ".db");
        DBManager.db = o.getWritableDatabase();
    }

    private class PersistenceOpenHelper extends SQLiteOpenHelper {
        PersistenceOpenHelper(Context context, String databaseName) {
            super(context, databaseName, null, DATABASE_VERSION);
        }
        @Override
        public void onCreate(SQLiteDatabase db) {
            for(GenericDAO genericDao : daos) {
                //drop old table
                db.execSQL("drop table if exists " + genericDao.TABLA_NOMBRE);
                //create news table and data
                genericDao.crearTabla(db);
                genericDao.crearData(db);
            }
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }
    }
}